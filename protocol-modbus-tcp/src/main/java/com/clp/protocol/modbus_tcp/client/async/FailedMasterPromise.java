package com.clp.protocol.modbus_tcp.client.async;

import com.clp.protocol.modbus_tcp.client.Master;

/**
 * 失败的 Promise
 */
public class FailedMasterPromise extends GenericMasterPromise<FailedMasterPromise, Void> {
    public FailedMasterPromise(Master master, Throwable cause) {
        super(master);
        super.setFailure(cause);
    }
}
