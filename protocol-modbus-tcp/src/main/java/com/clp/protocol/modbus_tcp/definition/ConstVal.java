package com.clp.protocol.modbus_tcp.definition;

public class ConstVal {
    /**
     * 事务处理标识符 长度
     */
    public static final int SEQ_LEN = 2;

    /**
     * 协议标识符 长度
     */
    public static final int TAG_LEN = 2;

    /**
     * 长度字段长度
     */
    public static final int LEN_LEN = 2;

    /**
     * 从站地址长度
     */
    public static final int ADDR_LEN = 1;

    /**
     * 功能码类型长度
     */
    public static final int FUNC_CODE_LEN = 1;

    /**
     * 协议头长度（7字节）
     */
    public static final int MB_HEAD_LEN = 6;

    /**
     * 字节计数 长度
     */
    public static final int BYTE_COUNT_LEN = 1;

    /**
     * 错误代码 长度
     */
    public static final int ERROR_CODE_LEN = 1;

    /**
     * 设置地址 长度
     */
    public static final int SET_ADDR_LEN = 2;

    /**
     * 起始地址 长度
     */
    public static final int START_ADDR_LEN = 2;

    /**
     * 数量 长度
     */
    public static final int NUM_LEN = 2;

    /**
     * 设置线圈内容 长度
     */
    public static final int SET_COIL_LEN = 2;

    /**
     * 设置寄存器内容 长度
     */
    public static final int SET_REG_LEN = 2;
    public static final int MAX_SEQ = 0xFFFF + 1;
}
