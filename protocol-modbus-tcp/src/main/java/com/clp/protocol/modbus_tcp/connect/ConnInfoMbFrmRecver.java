package com.clp.protocol.modbus_tcp.connect;

import com.clp.protocol.core.common.connect.FrameRecver;
import com.clp.protocol.modbus_tcp.mb_frame.MbFrm;

public interface ConnInfoMbFrmRecver extends FrameRecver<MbFrm> {

}
