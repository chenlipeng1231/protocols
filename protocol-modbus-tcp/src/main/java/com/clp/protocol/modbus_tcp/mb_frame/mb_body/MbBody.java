package com.clp.protocol.modbus_tcp.mb_frame.mb_body;

import com.clp.protocol.core.common.frame.InitializableFrameClip;
import com.clp.protocol.modbus_tcp.definition.FuncCode;

public interface MbBody extends InitializableFrameClip<MbBody> {
    int getAddr();

    FuncCode getFuncCode();
}
