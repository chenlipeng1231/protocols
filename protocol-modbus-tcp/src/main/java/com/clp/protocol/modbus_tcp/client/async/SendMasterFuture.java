package com.clp.protocol.modbus_tcp.client.async;

import com.clp.protocol.modbus_tcp.client.async.send_mb_frm.SendRes;
import com.clp.protocol.modbus_tcp.mb_frame.mb_body.mb_data.RespOkMbData;

public interface SendMasterFuture<D extends RespOkMbData> extends MasterFuture<SendRes<D>> {

    @Override
    SendMasterFuture<D> sync();

    @Override
    SendMasterFuture<D> sync(int timeoutMs);

    @Override
    SendMasterFuture<D> addListener(MasterFutureListener<SendRes<D>> listener);
}
