package com.clp.protocol.modbus_tcp.client;

import com.clp.protocol.modbus_tcp.connect.ConnInfo;

import java.util.concurrent.ExecutorService;

public interface Master extends ConnInfo, MasterMbFrmSender, MasterMbFrmRecver {

    int sendSeq();

    ExecutorService executorService();
}
