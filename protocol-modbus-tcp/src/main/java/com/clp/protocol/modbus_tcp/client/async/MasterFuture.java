package com.clp.protocol.modbus_tcp.client.async;

import com.clp.protocol.core.async.IFuture;
import com.clp.protocol.modbus_tcp.client.Master;

public interface MasterFuture<V> extends IFuture<V, MasterFutureListener<V>> {
    /**
     * 返回与此Future相关的主站
     * @return
     */
    Master master();

    @Override
    MasterFuture<V> sync();

    @Override
    MasterFuture<V> sync(int timeoutMs);

    @Override
    MasterFuture<V> addListener(MasterFutureListener<V> listener);
}
