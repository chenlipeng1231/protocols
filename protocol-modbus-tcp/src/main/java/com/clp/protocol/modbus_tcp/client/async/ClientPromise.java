package com.clp.protocol.modbus_tcp.client.async;

import com.clp.protocol.core.common.async.IFutureListener;
import com.clp.protocol.core.common.async.IPromise;

public interface ClientPromise<V> extends ClientFuture<V>, IPromise<V, IFutureListener<ClientFuture<V>>> {

    @Override
    ClientPromise<V> setRes(V val);

    @Override
    ClientPromise<V> setSuccess();

    @Override
    ClientPromise<V> setFailure(Throwable cause);

    @Override
    ClientPromise<V> sync();

    @Override
    ClientPromise<V> addListener(IFutureListener<ClientFuture<V>> listener);

}
