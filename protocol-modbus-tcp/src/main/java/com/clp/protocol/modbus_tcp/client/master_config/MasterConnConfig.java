package com.clp.protocol.modbus_tcp.client.master_config;

import com.clp.protocol.modbus_tcp.client.pipeline.AbstractModbusChannelInitializer;
import com.clp.protocol.modbus_tcp.client.pipeline.DefaultModbusChannelInitializer;
import com.clp.protocol.modbus_tcp.connect.config.ConnConfig;
import lombok.Getter;

@Getter
public class MasterConnConfig extends ConnConfig {
    /**
     * 默认初始化器
     */
    public static final AbstractModbusChannelInitializer DEFAULT_INITIALIZER = new DefaultModbusChannelInitializer();

    /******************************************************************************************************************/

    /**
     * 初始化器
     */
    private final AbstractModbusChannelInitializer initializer;

    private MasterConnConfig(Configurer configurer) {
        super(configurer);
        this.initializer = configurer.initializer;
    }

    public static Configurer configurer() {
        return new Configurer();
    }

    public static class Configurer extends ConnConfig.Configurer<Configurer> {
        private AbstractModbusChannelInitializer initializer = DEFAULT_INITIALIZER;

        public Configurer initializer(AbstractModbusChannelInitializer initializer) {
            this.initializer = initializer;
            return this;
        }

        public MasterConnConfig configOk() {
            return new MasterConnConfig(this);
        }
    }

    @Override
    public void check() throws Throwable {
        super.check();
        if (initializer == null) throw new RuntimeException("初始化器配置错误！");
    }
}
