package com.clp.protocol.modbus_tcp.mb_frame.mb_body.mb_data;

import io.netty.buffer.ByteBuf;

public interface RespOkMbData extends RespMbData {
    @Override
    RespOkMbData initBy(ByteBuf buf);

    @SuppressWarnings("unchecked")
    default <D extends RespMbData> D castTo(Class<D> dClass) {
        return ((D) this);
    }
}
