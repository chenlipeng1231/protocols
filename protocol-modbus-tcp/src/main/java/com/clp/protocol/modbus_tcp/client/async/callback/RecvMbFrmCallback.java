package com.clp.protocol.modbus_tcp.client.async.callback;

import com.clp.protocol.modbus_tcp.mb_frame.RespMbFrm;

public interface RecvMbFrmCallback {

    /**
     * 返回 true 则只执行一次，返回 false，会一直执行
     * @param respMbFrm
     * @return
     */
    boolean whenRecvMbFrm(RespMbFrm respMbFrm);

}
