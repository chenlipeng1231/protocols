package com.clp.protocol.modbus_tcp.definition;

import com.clp.protocol.core.exception.EnumElemDoesNotExistException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * 输入状态
 */
@Getter
@AllArgsConstructor
public enum InputStatus {
    /**
     * 通电
     */
    ELECTRIFY(1),
    /**
     * 未通电
     */
    NOT_ELECTRIFY(0);

    private final int val;

    public static InputStatus gain(int val) {
        for (InputStatus inputStatus : InputStatus.values()) {
            if (inputStatus.val == val) {
                return inputStatus;
            }
        }
        throw new EnumElemDoesNotExistException(InputStatus.class);
    }

    public static List<InputStatus> gainList(byte[] bytes) {
        List<InputStatus> list = new ArrayList<>();
        for (int i = 0; i < bytes.length; i++) {
            for (int j = 0; j < 8; j++) {
                list.add(gain((bytes[i] >> j) & 0x01));
            }
        }
        return list;
    }
}
