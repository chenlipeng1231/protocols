package com.clp.protocol.modbus_tcp.mb_frame.mb_body.mb_data.req;

import com.clp.protocol.core.utils.ByteUtil;
import com.clp.protocol.modbus_tcp.definition.MbByteOrder;
import com.clp.protocol.modbus_tcp.mb_frame.mb_body.mb_data.ReqMbData;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.nio.ByteOrder;
import java.util.Objects;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ReadInputRegsReqMbData implements ReqMbData {
    /**
     * 起始地址
     */
    private int startAddr;
    /**
     * 读取数量
     */
    private int num;

    @Override
    public ReqMbData initBy(ByteBuf buf) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isValid() {
        if (startAddr < 0 || startAddr > 0xFFFF) return false;
        return num >= 0 && num <= 0xFFFF;
    }

    @Override
    public void writeTo(ByteBuf buf) {
        buf.writeBytes(MbByteOrder.startAddrOrder == ByteOrder.BIG_ENDIAN ?
                ByteUtil.intToBytes2BE(startAddr) : ByteUtil.intToBytes2LE(startAddr));
        buf.writeBytes(MbByteOrder.numOrder == ByteOrder.BIG_ENDIAN ?
                ByteUtil.intToBytes2BE(num) : ByteUtil.intToBytes2LE(num));
    }

    @Override
    public String toString() {
        return "起始地址：" + startAddr + ", 读取数量：" + num;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReadInputRegsReqMbData that = (ReadInputRegsReqMbData) o;
        return startAddr == that.startAddr && num == that.num;
    }

    @Override
    public int hashCode() {
        return Objects.hash(startAddr, num);
    }
}
