package com.clp.protocol.modbus_tcp.connect.async.res;

public abstract class BaseRes<R extends BaseRes<R>> {
    private volatile boolean isSendSuccess; // 是否成功发送

    @SuppressWarnings("unchecked")
    protected R self() {
        return ((R) this);
    }

    public boolean getSendSuccess() {
        return isSendSuccess;
    }

    public R setSendSuccess(boolean isSendSuccess) {
        this.isSendSuccess = isSendSuccess;
        return self();
    }
}
