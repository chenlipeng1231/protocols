package com.clp.protocol.modbus_tcp.mb_frame.mb_body.mb_data;

import io.netty.buffer.ByteBuf;

public interface ReqMbData extends MbData {
    @Override
    ReqMbData initBy(ByteBuf buf);
}
