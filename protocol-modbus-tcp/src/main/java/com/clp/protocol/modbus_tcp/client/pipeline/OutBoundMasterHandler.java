package com.clp.protocol.modbus_tcp.client.pipeline;

import com.clp.protocol.modbus_tcp.mb_frame.RespMbFrm;
import io.netty.channel.ChannelHandlerContext;

public abstract class OutBoundMasterHandler extends DuplexMasterHandler {

    @Override
    public void whenRecvingMbFrm(ChannelHandlerContext ctx, RespMbFrm respMbFrm) {
        passRecvingMbFrm(ctx, respMbFrm);
    }

}
