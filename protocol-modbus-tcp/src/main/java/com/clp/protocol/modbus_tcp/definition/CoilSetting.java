package com.clp.protocol.modbus_tcp.definition;

import com.clp.protocol.core.exception.EnumElemDoesNotExistException;
import com.clp.protocol.core.utils.ByteUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 线圈设置
 */
@Getter
@AllArgsConstructor
public enum CoilSetting {
    /**
     * 吸合继电器
     */
    ATTRACT(new byte[]{(byte) 0xFF, 0x00}),
    /**
     * 释放继电器
     */
    BREAK(new byte[]{0x00, 0x00});

    private final byte[] bytes;

    public static CoilSetting gain(byte[] bytes) {
        for (CoilSetting coilSetting : CoilSetting.values()) {
            if (ByteUtil.isSameBytes(coilSetting.bytes, bytes)) {
                return coilSetting;
            }
        }
        throw new EnumElemDoesNotExistException(CoilSetting.class);
    }
}
