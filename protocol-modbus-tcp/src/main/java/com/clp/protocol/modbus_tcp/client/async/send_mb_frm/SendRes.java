package com.clp.protocol.modbus_tcp.client.async.send_mb_frm;

import com.clp.protocol.modbus_tcp.connect.async.res.BaseRes;
import com.clp.protocol.modbus_tcp.definition.ErrCode;
import com.clp.protocol.modbus_tcp.mb_frame.mb_body.mb_data.RespOkMbData;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class SendRes<D extends RespOkMbData> extends BaseRes<SendRes<D>> {
    private volatile boolean isRecvedResp;

    private volatile boolean isRecvedRespOk;
    private volatile D respData;

    private volatile boolean isRecvedRespErr;
    private volatile ErrCode respErrCode;

//    public R setRecvedResp(boolean isRecvedResp) {
//        this.isRecvedResp = isRecvedResp;
//        return self();
//    }
//
//    public R setRecvedRespOk(boolean isRecvedRespOk) {
//        this.isRecvedRespOk = isRecvedRespOk;
//        return self();
//    }
//
//    public R setRespData(D respData) {
//        this.respData = respData;
//        return self();
//    }
//
//    public R setRecvedRespErr(boolean isRecvedRespErr) {
//        this.isRecvedRespErr = isRecvedRespErr;
//        return self();
//    }
//
//    public R setRespErrCode(ErrCode errCode) {
//        this.respErrCode = errCode;
//        return self();
//    }
}
