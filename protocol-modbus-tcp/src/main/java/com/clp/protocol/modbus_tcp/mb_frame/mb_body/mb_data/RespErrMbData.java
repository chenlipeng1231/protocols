package com.clp.protocol.modbus_tcp.mb_frame.mb_body.mb_data;

import com.clp.protocol.modbus_tcp.definition.ErrCode;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RespErrMbData implements RespMbData {
    /**
     * 错误代码
     */
    private ErrCode errCode;

    @Override
    public RespErrMbData initBy(ByteBuf buf) {
        this.errCode = ErrCode.gain(buf.readByte());
        return this;
    }

    @Override
    public boolean isValid() {
        return errCode != null;
    }

    @Override
    public void writeTo(ByteBuf buf) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        return "错误码：" + errCode + "(" + errCode.getVal() + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RespErrMbData that = (RespErrMbData) o;
        return errCode == that.errCode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(errCode);
    }
}
