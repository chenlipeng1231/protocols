package com.clp.protocol.modbus_tcp.connect;

public interface ConnInfo extends ConnInfoMbFrmSender, ConnInfoMbFrmRecver {
    /**
     * 是否已连接
     * @return
     */
    boolean isConnected();

    String remoteHost();

    int remotePort();

    int addr();
}
