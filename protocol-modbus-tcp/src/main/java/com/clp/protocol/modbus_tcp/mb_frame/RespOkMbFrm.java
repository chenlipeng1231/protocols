package com.clp.protocol.modbus_tcp.mb_frame;

import com.clp.protocol.modbus_tcp.definition.MbFrmType;
import com.clp.protocol.modbus_tcp.mb_frame.mb_body.RespOkMbBody;
import com.clp.protocol.modbus_tcp.mb_frame.mb_head.MbHead;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RespOkMbFrm implements RespMbFrm {
    @Override
    public MbFrmType mbFrmType() {
        return MbFrmType.RESP_OK;
    }

    private MbHead head;
    private RespOkMbBody body;

    @Override
    public RespOkMbFrm initBy(ByteBuf buf) {
        this.head = new MbHead().initBy(buf);
        this.body = new RespOkMbBody().initBy(buf);
        return this;
    }

    @Override
    public boolean isValid() {
        if (head == null || !head.isValid()) return false;
        return body != null && body.isValid();
    }

    @Override
    public void writeTo(ByteBuf buf) {
        head.writeTo(buf);
        body.writeTo(buf);
    }

    @Override
    public String toString() {
        switch (PrintOption.option) {
            case PrintOption.DESCRIPTION:
                return "请求帧：" + head + ", " + body;
            case PrintOption.HEX_STRING:
                return toHexString();
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RespOkMbFrm that = (RespOkMbFrm) o;
        return Objects.equals(head, that.head) && Objects.equals(body, that.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(head, body);
    }
}
