package com.clp.protocol.modbus_tcp.definition;

import java.nio.ByteOrder;

public interface MbByteOrder {
    /**
     * 序号字节序
     */
    ByteOrder seqOrder = ByteOrder.LITTLE_ENDIAN;
    /**
     * 长度字节序
     */
    ByteOrder lenOrder = ByteOrder.BIG_ENDIAN;
    /**
     * 数量字节序
     */
    ByteOrder numOrder = ByteOrder.BIG_ENDIAN;
    /**
     * 起始地址字节序
     */
    ByteOrder startAddrOrder = ByteOrder.BIG_ENDIAN;
    /**
     * 设置地址字节序
     */
    ByteOrder setAddrOrder = ByteOrder.BIG_ENDIAN;

    /**
     * 设置线圈内容字节序
     */
    ByteOrder setCoilOrder = ByteOrder.BIG_ENDIAN;

    /**
     * 设置寄存器内容字节序
     */
    ByteOrder setRegOrder = ByteOrder.BIG_ENDIAN;
}
