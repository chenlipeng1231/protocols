package com.clp.protocol.modbus_tcp.mb_frame.mb_body.mb_data;

import com.clp.protocol.core.common.frame.InitializableFrameClip;

public interface MbData extends InitializableFrameClip<MbData> {
}
