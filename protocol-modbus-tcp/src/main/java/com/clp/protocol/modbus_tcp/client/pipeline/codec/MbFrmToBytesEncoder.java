package com.clp.protocol.modbus_tcp.client.pipeline.codec;

import com.clp.protocol.modbus_tcp.mb_frame.ReqMbFrm;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class MbFrmToBytesEncoder extends MessageToByteEncoder<ReqMbFrm> {
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, ReqMbFrm reqMbFrm, ByteBuf byteBuf) throws Exception {
        byteBuf.writeBytes(reqMbFrm.toByteArray());
    }
}
