package com.clp.protocol.modbus_tcp.client;

import com.clp.protocol.modbus_tcp.client.async.SendMasterFuture;
import com.clp.protocol.modbus_tcp.connect.ConnInfoMbFrmSender;
import com.clp.protocol.modbus_tcp.definition.CoilSetting;
import com.clp.protocol.modbus_tcp.definition.CoilStatus;
import com.clp.protocol.modbus_tcp.mb_frame.mb_body.mb_data.resp_ok.*;

import java.util.List;

public interface MasterMbFrmSender extends ConnInfoMbFrmSender {

    SendMasterFuture<ReadCoilStatusRespOkMbData> sendReqMbFrmOfReadCoilStatus(int startAddr, int num);

    SendMasterFuture<ReadInputStatusRespOkMbData> sendReqMbFrmOfReadInputStatus(int startAddr, int num);

    SendMasterFuture<ReadHoldRegsRespOkMbData> sendReqMbFrmOfReadHoldRegs(int startAddr, int num);

    SendMasterFuture<ReadInputRegsRespOkMbData> sendReqMbFrmOfReadInputRegs(int startAddr, int num);

    SendMasterFuture<ForceSingleCoilRespOkMbData> sendReqMbFrmOfForceSingleCoil(int setAddr, CoilSetting coilSetting);

    SendMasterFuture<PresetSingleRegRespOkMbData> sendReqMbFrmOfPresetSingleReg(int setAddr, int setVal);

    SendMasterFuture<ForceMultiCoilsRespOkMbData> sendReqMbFrmOfForceMultiCoils(int startAddr, List<CoilStatus> coilStatuses);

    SendMasterFuture<ForceMultiRegsRespOkMbData> sendReqMbFrmOfForceMultiRegs(int startAddr, List<Integer> setVals);
}
