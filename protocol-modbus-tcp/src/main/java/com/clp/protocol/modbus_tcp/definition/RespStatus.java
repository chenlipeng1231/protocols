package com.clp.protocol.modbus_tcp.definition;

public enum RespStatus {
    OK,
    ERROR
}
