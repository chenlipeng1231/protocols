package com.clp.protocol.modbus_tcp.definition;

public enum MbFrmType {
    REQ,
    RESP_OK,
    RESP_ERR;
}
