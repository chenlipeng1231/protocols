package com.clp.protocol.modbus_tcp.client.pipeline.codec;

import com.clp.protocol.modbus_tcp.mb_frame.MbFrmFactory;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class BytesToMbFrmDecoder extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        // 将消息解码成ModbusFrame对象
        list.add(MbFrmFactory.getRespMbFrm(byteBuf));
    }
}
