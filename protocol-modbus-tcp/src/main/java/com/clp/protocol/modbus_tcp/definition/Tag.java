package com.clp.protocol.modbus_tcp.definition;

import com.clp.protocol.core.exception.EnumElemDoesNotExistException;
import com.clp.protocol.core.utils.ByteUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 协议标识符
 */
@Getter
@AllArgsConstructor
public enum Tag {
    /**
     * Modbus-Tcp 类型
     */
    MODBUS_TCP(new byte[]{0x00, 0x00});

    private final byte[] bytes;

    public static Tag gain(byte[] bytes) {
        for (Tag tag : Tag.values()) {
            if (ByteUtil.isSameBytes(tag.bytes, bytes)) {
                return tag;
            }
        }
        throw new EnumElemDoesNotExistException(Tag.class);
    }
}
