package com.clp.protocol.modbus_tcp.client.async;

import com.clp.protocol.core.async.IFutureListener;

public interface MasterFutureListener<V> extends IFutureListener<MasterFuture<V>> {
}
