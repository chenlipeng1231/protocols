package com.clp.protocol.modbus_tcp.mb_frame.mb_body.mb_data.req;

import com.clp.protocol.core.utils.ByteUtil;
import com.clp.protocol.modbus_tcp.definition.CoilSetting;
import com.clp.protocol.modbus_tcp.definition.MbByteOrder;
import com.clp.protocol.modbus_tcp.mb_frame.mb_body.mb_data.ReqMbData;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.nio.ByteOrder;
import java.util.Objects;

/**
 * 设置单个继电器
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ForceSingleCoilReqMbData implements ReqMbData {
    /**
     * 设置地址
     */
    private int setAddr;
    /**
     * 设置内容
     */
    private CoilSetting coilSetting;

    @Override
    public ReqMbData initBy(ByteBuf buf) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isValid() {
        if (setAddr < 0 || setAddr > 0xFFFF) return false;
        return coilSetting != null;
    }

    @Override
    public void writeTo(ByteBuf buf) {
        // 设置地址
        buf.writeBytes(MbByteOrder.setAddrOrder == ByteOrder.BIG_ENDIAN ?
                ByteUtil.intToBytes2BE(setAddr) : ByteUtil.intToBytes2LE(setAddr));
        // 设置内容
        buf.writeBytes(coilSetting.getBytes());
    }

    @Override
    public String toString() {
        return "设置地址：" + setAddr + ", 设置内容：" + coilSetting;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ForceSingleCoilReqMbData that = (ForceSingleCoilReqMbData) o;
        return setAddr == that.setAddr && coilSetting == that.coilSetting;
    }

    @Override
    public int hashCode() {
        return Objects.hash(setAddr, coilSetting);
    }
}
