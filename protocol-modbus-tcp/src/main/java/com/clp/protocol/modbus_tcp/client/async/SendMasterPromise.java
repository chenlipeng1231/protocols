package com.clp.protocol.modbus_tcp.client.async;

import com.clp.protocol.modbus_tcp.client.Master;
import com.clp.protocol.modbus_tcp.client.async.send_mb_frm.SendRes;
import com.clp.protocol.modbus_tcp.mb_frame.mb_body.mb_data.RespOkMbData;
import lombok.Getter;

@Getter
public class SendMasterPromise<D extends RespOkMbData> extends GenericMasterPromise<SendMasterPromise<D>, SendRes<D>>
        implements SendMasterFuture<D> {
    private final int sendSeq;
    private final Class<D> dClass;

    public SendMasterPromise(Master master, Class<D> dClass, int sendSeq) {
        super(master);
        this.res = new SendRes<>();
        this.dClass = dClass;
        this.sendSeq = sendSeq;
    }
}
