package com.clp.protocol.modbus_tcp.client.async;

import com.clp.protocol.modbus_tcp.client.Master;

public class SucceedMasterPromise<V> extends GenericMasterPromise<SucceedMasterPromise<V>, V>{

    public SucceedMasterPromise(Master master, V val) {
        super(master);
        this.setRes(val);
        setSuccess();
    }

    public SucceedMasterPromise(Master master) {
        this(master, null);
    }
}
