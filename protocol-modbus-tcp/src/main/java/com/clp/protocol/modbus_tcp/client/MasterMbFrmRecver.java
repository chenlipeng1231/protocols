package com.clp.protocol.modbus_tcp.client;

import com.clp.protocol.modbus_tcp.client.async.callback.RecvMbFrmCallback;
import com.clp.protocol.modbus_tcp.connect.ConnInfoMbFrmRecver;

public interface MasterMbFrmRecver extends ConnInfoMbFrmRecver {

    void addRecvMbFrmCallback(RecvMbFrmCallback callback);

}
