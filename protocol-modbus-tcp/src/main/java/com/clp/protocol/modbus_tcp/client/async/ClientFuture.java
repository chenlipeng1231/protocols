package com.clp.protocol.modbus_tcp.client.async;

import com.clp.protocol.core.common.async.IFuture;
import com.clp.protocol.core.common.async.IFutureListener;
import com.clp.protocol.modbus_tcp.client.ModbusClient;

public interface ClientFuture<V> extends IFuture<V, IFutureListener<ClientFuture<V>>> {
    /**
     * 返回与此Future相关的主站
     * @return
     */
    ModbusClient client();

    @Override
    ClientFuture<V> sync();

    @Override
    ClientFuture<V> addListener(IFutureListener<ClientFuture<V>> listener);

}
