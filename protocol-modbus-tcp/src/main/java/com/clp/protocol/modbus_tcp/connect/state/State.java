package com.clp.protocol.modbus_tcp.connect.state;

public interface State {
    /**
     * 重置状态，但保留上次连接相关信息
     */
    void reset();

    /**
     * 监视器要执行作业
     */
    void startMonitorTasks();

    void tryStopMonitorTasks();
}
