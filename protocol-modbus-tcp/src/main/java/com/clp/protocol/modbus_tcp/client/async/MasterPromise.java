package com.clp.protocol.modbus_tcp.client.async;

import com.clp.protocol.core.common.async.IPromise;

public interface MasterPromise<V> extends MasterFuture<V>, IPromise<V, MasterFutureListener<V>> {

    @Override
    MasterPromise<V> setRes(V val);

    @Override
    MasterPromise<V> setSuccess();

    @Override
    MasterPromise<V> setFailure(Throwable cause);

    @Override
    MasterPromise<V> sync();

    @Override
    MasterPromise<V> sync(int timeoutMs);

    @Override
    MasterPromise<V> addListener(MasterFutureListener<V> listener);

}
