package com.clp.protocol.modbus_tcp.client.pipeline;

import com.clp.protocol.modbus_tcp.mb_frame.ReqMbFrm;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

public abstract class InBoundMasterHandler extends DuplexMasterHandler {

    @Override
    public void whenSendingMbFrm(ChannelHandlerContext ctx, ReqMbFrm reqMbFrm, ChannelPromise promise) {
        passSendingMbFrm(ctx, reqMbFrm, promise);
    }
}
