package com.clp.protocol.modbus_tcp.definition;

import com.clp.protocol.core.exception.EnumElemDoesNotExistException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * 线圈状态
 */
@Getter
@AllArgsConstructor
public enum CoilStatus {
    /**
     * 吸合
     */
    ATTRACT(1),
    /**
     * 断开
     */
    BREAK(0);

    private final int val;

    public static CoilStatus gain(int val) {
        for (CoilStatus coilStatus : CoilStatus.values()) {
            if (coilStatus.val == val) {
                return coilStatus;
            }
        }
        throw new EnumElemDoesNotExistException(CoilStatus.class);
    }

    public static List<CoilStatus> gainList(byte[] bytes) {
        List<CoilStatus> list = new ArrayList<>();
        for (int i = 0; i < bytes.length; i++) {
            for (int j = 0; j < 8; j++) {
                list.add(gain((bytes[i] >> j) & 0x01));
            }
        }
        return list;
    }
}
