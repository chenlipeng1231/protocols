package com.clp.protocol.modbus_tcp.connect.state;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractState implements State {
    protected ScheduledFuture<?> scheduledFuture;

    @Override
    public void tryStopMonitorTasks() {
        if (scheduledFuture != null && !scheduledFuture.isCancelled()) scheduledFuture.cancel(true);
    }

    protected abstract ScheduledFuture<?> scheduleAtFixedRate(Runnable task, long initialDelay, long period, TimeUnit unit);
}
