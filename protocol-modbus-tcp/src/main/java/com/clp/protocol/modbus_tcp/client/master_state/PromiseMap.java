package com.clp.protocol.modbus_tcp.client.master_state;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

class PromiseMap<P> {
    private Map<Integer, List<P>> map;

    public PromiseMap() {
        this.map = new HashMap<>();
    }

    public synchronized void put(int sendSeq, P promise) {
        List<P> promises = map.get(sendSeq);
        if (promises == null) {
            promises = new ArrayList<>();
            promises.add(promise);
            map.put(sendSeq, promises);
        } else {
            promises.add(promise);
        }
    }

    public synchronized void removeIf(int sendSeq, Predicate<P> filter) {
        List<P> promises = map.get(sendSeq);
        if (promises != null) {
            promises.removeIf(filter);
        }
    }
}
