package com.clp.protocol.modbus_tcp.client.pipeline.codec;

import com.clp.protocol.modbus_tcp.definition.ConstVal;
import com.clp.protocol.modbus_tcp.definition.MbByteOrder;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

import java.nio.ByteOrder;

public class CompleteMbFrmDecoder extends LengthFieldBasedFrameDecoder {
    private static final ByteOrder byteOrder = MbByteOrder.lenOrder;
    private static final int maxFrameLength = ConstVal.MB_HEAD_LEN + 255;
    private static final int lengthFieldOffset = 4;
    private static final int lengthFieldLength = 2;
    private static final int lengthAdjustment = 0;
    private static final int initialBytesToStrip = 0;
    private static final boolean failFast = true;

    public CompleteMbFrmDecoder() {
        super(byteOrder, maxFrameLength, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip, failFast);
    }
    public CompleteMbFrmDecoder(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength) {
        super(maxFrameLength, lengthFieldOffset, lengthFieldLength);
    }
}
