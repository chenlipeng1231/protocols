package com.clp.protocol.modbus_tcp.client.async;

import com.clp.protocol.modbus_tcp.client.Master;

public class DefaultMasterPromise<V> extends GenericMasterPromise<DefaultMasterPromise<V>, V>{
    public DefaultMasterPromise(Master master, V val) {
        super(master);
        this.res = val;
    }

    public DefaultMasterPromise(Master master) {
        this(master, null);
    }
}
