package com.clp.protocol.iec104.definition.quatype;

import com.clp.protocol.core.utils.EnumElemDoesNotExistException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 电度量召唤限定词类型（总召唤101）
 */
@Getter
@AllArgsConstructor
public enum TpQuaType {
    QCC((byte)0x45, "电度量召唤");

    private final byte val;
    private final String desc;

    public static TpQuaType gain(byte val) {
        for (TpQuaType type : TpQuaType.values()) {
            if (type.val == val) {
                return type;
            }
        }
        throw new EnumElemDoesNotExistException(TpQuaType.class);
    }
}
