package com.clp.protocol.iec104.server.pipeline.filter;

import com.clp.protocol.iec104.server.pipeline.PipelineManager;
import io.netty.handler.ipfilter.IpFilterRule;
import io.netty.handler.ipfilter.RuleBasedIpFilter;

public class RemoteIpFilterSlaveChannelHandler extends RuleBasedIpFilter {
    private final PipelineManager manager;

    public RemoteIpFilterSlaveChannelHandler(PipelineManager manager, IpFilterRule ipFilterRule) {
        super(false, ipFilterRule);
        this.manager = manager;
    }
}
