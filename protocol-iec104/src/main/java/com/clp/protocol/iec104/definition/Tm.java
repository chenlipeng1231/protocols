package com.clp.protocol.iec104.definition;

import com.clp.protocol.core.utils.EnumElemDoesNotExistException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 遥测的相关定义
 */
public interface Tm {
    /**
     * 值类型
     */
    @Getter
    @AllArgsConstructor
    enum Type {
        /**
         * 归一化值
         */
        NORMALIZE("归一化值"),
        /**
         * 标度化值
         */
        SCALED("标度化值"),
        /**
         * 短浮点数
         */
        FLOAT("短浮点数");

        private final String desc;
    }

    /**
     * 变化类型
     */
    @Getter
    @AllArgsConstructor
    enum VaryType {
        FLOAT(1, "短浮点数", TypeTag.M_ME_NC_1);

        private final int val;
        private final String name;
        private final TypeTag typeTag;

        public static VaryType gain(String name) {
            for (VaryType type : VaryType.values()) {
                if (type.name.equals(name)) {
                    return type;
                }
            }
            throw new EnumElemDoesNotExistException(VaryType.class);
        }
    }

    /**
     * 遥测 有效位 的定义
     */
    @Getter
    @AllArgsConstructor
    enum Valid {
        /**
         * 0为有效
         */
        VALID(0, "有效"),
        /**
         * 1为无效
         */
        NOT_VALID(1, "无效");

        private final int val;
        private final String desc;

        /**
         * 根据值返回对应的状态
         */
        public static Valid gain(int value) {
            for (Valid valid : Valid.values()) {
                if (valid.getVal() == value) {
                    return valid;
                }
            }
            throw new EnumElemDoesNotExistException(Valid.class);
        }

        public static Valid gain(boolean flag) {
            if (flag) return VALID;
            return NOT_VALID;
        }
    }
}
