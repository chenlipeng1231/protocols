package com.clp.protocol.iec104.server.pipeline.state.data;

/**
 * 测点数据
 */
public interface PointData {

    /**
     * 地址
     * @return
     */
    int getAddress();

}
