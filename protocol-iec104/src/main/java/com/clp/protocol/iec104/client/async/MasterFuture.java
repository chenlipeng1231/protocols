package com.clp.protocol.iec104.client.async;

import com.clp.protocol.core.async.IFuture;
import com.clp.protocol.iec104.client.Master;

/**
 * 主站Future，跟ChannelFuture一样，ChannelFuture会绑定对应的Channel，MasterFuture会绑定对应的Master
 * @param <V>
 */
public interface MasterFuture<V> extends IFuture<V, MasterFutureListener<V>> {
    /**
     * 返回与此Future相关的主站
     * @return
     */
    Master master();

    @Override
    MasterFuture<V> sync();

    @Override
    MasterFuture<V> sync(int timeoutMs);

    @Override
    MasterFuture<V> addListener(MasterFutureListener<V> listener);
}
