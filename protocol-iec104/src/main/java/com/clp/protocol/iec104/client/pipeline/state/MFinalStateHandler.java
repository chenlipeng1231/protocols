package com.clp.protocol.iec104.client.pipeline.state;

import com.clp.protocol.iec104.client.pipeline.MPipelineManager;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nullable;
import java.util.concurrent.TimeUnit;

@Slf4j
public class MFinalStateHandler extends AbstractMStateHandler {
    public MFinalStateHandler(MPipelineManager pipelineManager) {
        super(pipelineManager);
    }

    @Override
    protected void resetState() {

    }

    @Override
    protected void afterResetState() {

    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    }

    @Nullable
    @Override
    protected ScheduledTask getScheduledTask() {
        return null;
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        // 以一定的延迟时间进行重连—— 1分钟
        eventLoop.schedule(() -> {
            inMaster().getConnector().tryAutoReconnect();
        }, 30, TimeUnit.SECONDS);

        ctx.fireChannelInactive();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        // 捕获异常
        cause.printStackTrace();
    }
}
