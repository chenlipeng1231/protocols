package com.clp.protocol.iec104.definition.cot;

import com.clp.protocol.core.utils.EnumElemDoesNotExistException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 传输原因的 试验/未试验 定义
 */
@Getter
@AllArgsConstructor
public enum Test {
    /**
     * 1为试验
     */
    TEST(1, "试验"),
    /**
     * 0为未试验
     */
    NOT_TEST(0, "未试验");

    private final int val;
    private final String desc;

    public static Test gain(int value) {
        for (Test test : Test.values()) {
            if (test.getVal() == value) {
                return test;
            }
        }
        throw new EnumElemDoesNotExistException(Test.class);
    }
}
