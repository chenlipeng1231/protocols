package com.clp.protocol.iec104.apdu.asdu;

import com.clp.protocol.iec104.definition.Sq;
import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePduClip;
import com.clp.protocol.core.pdu.ByteToStringFormat;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * 可变结构限定词
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Vsq extends BaseNBytePduClip<Vsq> {

    /**
     * 1个字节。第1位：sq=0为连续；sq=1为不连续。
     */
    private Sq sq;
    /**
     * 1个字节。后7位：信息元素的数目
     */
    private int num;

    @Override
    public Vsq refreshFrom(ByteBuf buf) {
        byte by = buf.readByte();
        this.sq = Sq.gain((by & 0x80) == 0x80 ? 1 : 0);
        this.num = by & (byte) 0x7F;
        return this;
    }

    @Override
    public boolean isValid() {
        return sq != null && num >= 0 && num <= 0x7F;
    }

    @Override
    public void writeBytesTo(ByteBuf buf) {
        buf.writeByte(generateByte());
    }

    private byte generateByte() {
        byte by = 0x00;
        if (sq.getVal() == 1) by |= 0x80;
        by |= (byte) (num & 0x7F);
        return by;
    }

    @Override
    public void writeFormattedByteStringsTo(StringBuilder sb, String frameClipBytesSeparator, String byteSeparator, ByteToStringFormat byteFormat) {
        sb.append(byteFormat.format(generateByte()));
    }

    @Override
    public void writeSimpleDescriptionTo(StringBuilder sb) {
        sb.append("<").append(sq.getDesc()).append("(").append(sq.getVal()).append("), ").append(num).append(">");
    }

    @Override
    public void writeDetailDescriptionTo(StringBuilder sb) {
        sb.append("<类型：").append(sq.getDesc()).append("(").append(sq.getVal()).append("), 个数：").append(num).append(">");
    }

    @Override
    protected void forEachOneLevelChild(Consumer<BaseNBytePduClip<?>> consumer) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vsq vsq = (Vsq) o;
        return num == vsq.num && sq == vsq.sq;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sq, num);
    }
}
