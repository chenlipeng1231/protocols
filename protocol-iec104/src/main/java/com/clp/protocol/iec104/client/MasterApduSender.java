package com.clp.protocol.iec104.client;

import com.clp.protocol.iec104.client.async.MasterFuture;
import com.clp.protocol.iec104.common.res.DefaultRes;
import com.clp.protocol.iec104.common.res.SendTestFrVRes;
import com.clp.protocol.iec104.client.async.sendapdu.*;
import com.clp.protocol.core.pdu.PduSender;
import com.clp.protocol.iec104.apdu.Apdu;

/**
 * 主站Apdu发送器：作为主站可以发送一些对应的Apdu
 */
public interface MasterApduSender {

    /**
     * 修改父接口的返回值，保证返回Future的一致性
     * @return
     */
    MasterFuture<DefaultRes> sendSApduOfAck();

    /**
     * 修改父接口的返回值，保证返回Future的一致性
     * @return
     */
    MasterFuture<SendTestFrVRes> sendUApduOfTestFrV();

    /**
     * 修改父接口的返回值，保证返回Future的一致性
     * @return
     */
    MasterFuture<DefaultRes> sendUApduOfTestFrC();

    /**
     * 发送启动传输激活
     * @return
     */
    MasterFuture<SendStartDtVRes> sendUApduOfStartDtV();

    /**
     * 手动发送总召唤100
     */
    MasterFuture<SendTotalCall100Res> sendIApduOfTotalCall100();

    /**
     * 手动发送总召唤101
     * @return
     */
    MasterFuture<SendTotalCall101Res> sendIApduOfTotalCall101();

    /**
     * 发送单点遥控选择
     * @param infoObjAddr
     * @param isSwitchOn
     * @return
     */
    MasterFuture<SendTcSelectRes> sendIApduOfOnePointTcSelect(int infoObjAddr, boolean isSwitchOn);

    /**
     * 发送双点遥控选择
     * @param infoObjAddr
     * @param isSwitchOn
     * @return
     */
    MasterFuture<SendTcSelectRes> sendIApduOfTwoPointTcSelect(int infoObjAddr, boolean isSwitchOn);

    /**
     * 发送单点遥控执行
     * @param infoObjAddr
     * @param isSwitchOn
     * @return
     */
    MasterFuture<SendTcExecuteRes> sendIApduOfOnePointTcExecute(int infoObjAddr, boolean isSwitchOn);

    /**
     * 发送单点遥控执行
     * @param infoObjAddr
     * @param isSwitchOn
     * @return
     */
    MasterFuture<SendTcExecuteRes> sendIApduOfTwoPointTcExecute(int infoObjAddr, boolean isSwitchOn);

    /**
     * 发送遥调选择归一化值
     * @param infoObjAddr
     * @param setVal
     * @return
     */
    MasterFuture<SendTaSelectRes> sendIApduOfTaSelectNormalized(int infoObjAddr, int setVal);

    /**
     * 发送遥调执行归一化值
     * @param infoObjAddr
     * @param setVal
     * @return
     */
    MasterFuture<SendTaExecuteRes> sendIApduOfTaExecuteNormalized(int infoObjAddr, int setVal);

    /**
     * 发送遥调选择短浮点数
     * @param infoObjAddr
     * @param setVal
     * @return
     */
    MasterFuture<SendTaSelectRes> sendIApduOfTaSelectFloat(int infoObjAddr, float setVal);

    /**
     * 发送遥调执行短浮点数
     * @param infoObjAddr
     * @param setVal
     * @return
     */
    MasterFuture<SendTaExecuteRes> sendIApduOfTaExecuteFloat(int infoObjAddr, float setVal);
}
