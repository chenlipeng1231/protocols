package com.clp.protocol.iec104.client.async.sendapdu;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 发送遥调执行 的结果
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class SendTaExecuteRes {
    private volatile boolean isSendExecuteSuccess;

    private volatile boolean isRecvExecuteAckYes;
    private volatile boolean isRecvExecuteAckNo;
    private volatile boolean isRecvEnd;

    private String failDesc;
}
