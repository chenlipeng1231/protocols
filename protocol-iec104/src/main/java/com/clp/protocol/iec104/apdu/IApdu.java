package com.clp.protocol.iec104.apdu;

import com.clp.protocol.iec104.apdu.apci.IApci;
import com.clp.protocol.iec104.apdu.asdu.IAsdu;
import com.clp.protocol.iec104.definition.ApduType;
import com.clp.protocol.iec104.definition.ConstVal;
import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePduClip;
import com.clp.protocol.core.pdu.ByteToStringFormat;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * I帧
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class IApdu extends Apdu {
    private IApci iApci;
    private IAsdu iAsdu;

    @Override
    public ApduType apduType() {
        return ApduType.IType;
    }

    @Override
    public IApdu refreshFrom(ByteBuf buf) {
        this.iApci = new IApci().refreshFrom(buf);
        this.iAsdu = new IAsdu().refreshFrom(buf);
        return this;
    }

    @Override
    public boolean isValid() {
        return iApci != null && iApci.isValid() && iAsdu != null && iAsdu.isValid();
    }

    @Override
    public void writeBytesTo(ByteBuf buf) {
        iApci.writeBytesTo(buf);
        iAsdu.writeBytesTo(buf);
        // 注意：最后要设置长度
        buf.setByte(buf.readerIndex() + ConstVal.HEAD_LEN, buf.readableBytes() - ConstVal.HEAD_LEN - ConstVal.LENGTH_LEN);
    }

    @Override
    public void writeFormattedByteStringsTo(StringBuilder sb, String frameClipBytesSeparator, String byteSeparator, ByteToStringFormat byteFormat) {
        iApci.writeFormattedByteStringsTo(sb, frameClipBytesSeparator, byteSeparator, byteFormat);
        sb.append(frameClipBytesSeparator);
        iAsdu.writeFormattedByteStringsTo(sb, frameClipBytesSeparator, byteSeparator, byteFormat);
    }

    @Override
    public void writeSimpleDescriptionTo(StringBuilder sb) {
        sb.append("     <").append(apduType().getDesc()).append("> ");
        iApci.writeSimpleDescriptionTo(sb);
        iAsdu.writeSimpleDescriptionTo(sb);
    }

    @Override
    public void writeDetailDescriptionTo(StringBuilder sb) {
        sb.append("     <帧类型：").append(apduType().getDesc()).append("> ");
        iApci.writeDetailDescriptionTo(sb);
        iAsdu.writeDetailDescriptionTo(sb);
    }

    @Override
    protected void forEachOneLevelChild(Consumer<BaseNBytePduClip<?>> consumer) {
        consumer.accept(iApci);
        consumer.accept(iAsdu);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IApdu iApdu = (IApdu) o;
        return Objects.equals(iApci, iApdu.iApci) && Objects.equals(iAsdu, iApdu.iAsdu);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iApci, iAsdu);
    }
}

