package com.clp.protocol.iec104.apdu;

import com.clp.protocol.iec104.apdu.apci.*;
import com.clp.protocol.iec104.definition.ConstVal;
import com.clp.protocol.iec104.definition.UCtrlType;

public class ApciFactory {


    public static IApci getIApci(int sendSeq, int recvSeq) {
        return new IApci(ConstVal.HEAD_VAL, 0, new ICtrlArea(sendSeq, recvSeq));
    }

    public static SApci getSApci(int recvSeq) {
        return new SApci(ConstVal.HEAD_VAL, ConstVal.APCI_LEN, new SCtrlArea(recvSeq));
    }

    public static UApci getUApci(UCtrlType uCtrlType) {
        return new UApci(ConstVal.HEAD_VAL, ConstVal.APCI_LEN, new UCtrlArea(uCtrlType));
    }

    public static UApci getUApciOfStartDtV() {
        return getUApci(UCtrlType.U_START_DT_V);
    }

    public static UApci getUApciOfStartDtC() {
        return getUApci(UCtrlType.U_START_DT_C);
    }

    public static UApci getUApduOfTestFrV() {
        return getUApci(UCtrlType.U_TEST_FR_V);
    }

    public static UApci getUApciOfTestFrC() {
        return getUApci(UCtrlType.U_TEST_FR_C);
    }

    public static UApci getUApciOfStopDtV() {
        return getUApci(UCtrlType.U_STOP_DT_V);
    }

    public static UApci getUApciOfStopDtC() {
        return getUApci(UCtrlType.U_STOP_DT_C);
    }
}
