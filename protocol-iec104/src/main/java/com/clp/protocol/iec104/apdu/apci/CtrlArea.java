package com.clp.protocol.iec104.apdu.apci;

import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePduClip;;

/**
 * 控制域
 */
public abstract class CtrlArea<C extends CtrlArea<C>> extends BaseNBytePduClip<C> {
}
