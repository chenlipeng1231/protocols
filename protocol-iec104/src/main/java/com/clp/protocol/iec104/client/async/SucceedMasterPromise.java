package com.clp.protocol.iec104.client.async;

import com.clp.protocol.iec104.client.Master;

/**
 * 成功的主站Promise
 * @param <V> 值
 */
public class SucceedMasterPromise<V> extends GenericMasterPromise<SucceedMasterPromise<V>, V>{

    public SucceedMasterPromise(Master master, V val) {
        super(master);
        this.setRes(val);
        setSuccess();
    }

    public SucceedMasterPromise(Master master) {
        this(master, null);
    }
}
