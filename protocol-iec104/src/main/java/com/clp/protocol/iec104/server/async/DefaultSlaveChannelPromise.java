package com.clp.protocol.iec104.server.async;

import com.clp.protocol.iec104.server.SlaveChannel;

public class DefaultSlaveChannelPromise<V> extends GenericSlaveChannelPromise<DefaultSlaveChannelPromise<V>, V> {
    public DefaultSlaveChannelPromise(SlaveChannel slaveChannel, V res) {
        super(slaveChannel);
        this.res = res;
    }

    public DefaultSlaveChannelPromise(SlaveChannel slaveChannel) {
        this(slaveChannel, null);
    }
}
