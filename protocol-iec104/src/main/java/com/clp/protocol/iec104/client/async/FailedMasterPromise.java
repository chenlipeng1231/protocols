package com.clp.protocol.iec104.client.async;

import com.clp.protocol.iec104.client.Master;

/**
 * 失败的 主站Promise
 */
public class FailedMasterPromise extends GenericMasterPromise<FailedMasterPromise, Void> {
    public FailedMasterPromise(Master master, Throwable cause) {
        super(master);
        super.setFailure(cause);
    }
}
