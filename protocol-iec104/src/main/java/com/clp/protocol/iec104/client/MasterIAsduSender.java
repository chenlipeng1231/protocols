package com.clp.protocol.iec104.client;

import com.clp.protocol.iec104.client.async.MasterFuture;
import com.clp.protocol.iec104.client.async.sendapdu.*;

public interface MasterIAsduSender {

    /**
     * 该发送器所属的主站
     * @return
     */
    Master master();

    /**
     * 发送总召唤100
     * @return
     */
    MasterFuture<SendTotalCall100Res> sendTotalCall100();

    /**
     * 发送电度量召唤
     * @return
     */
    MasterFuture<SendTotalCall101Res> sendTotalCall101();

    /**
     * 发送单点遥控选择
     * @param infoObjAddr
     * @param isSwitchOn
     * @return
     */
    MasterFuture<SendTcSelectRes> sendOnePointTcSelect(int infoObjAddr, boolean isSwitchOn);

    /**
     * 发送双点遥控选择
     * @param infoObjAddr
     * @param isSwitchOn
     * @return
     */
    MasterFuture<SendTcSelectRes> sendTwoPointTcSelect(int infoObjAddr, boolean isSwitchOn);

    /**
     * 发送单点遥控执行
     * @param infoObjAddr
     * @param isSwitchOn
     * @return
     */
    MasterFuture<SendTcExecuteRes> sendOnePointTcExecute(int infoObjAddr, boolean isSwitchOn);

    /**
     * 发送双点遥控执行
     * @param infoObjAddr
     * @param isSwitchOn
     * @return
     */
    MasterFuture<SendTcExecuteRes> sendTwoPointTcExecute(int infoObjAddr, boolean isSwitchOn);

    /**
     * 发送遥调选择归一化值
     * @param infoObjAddr
     * @param setVal
     * @return
     */
    MasterFuture<SendTaSelectRes> sendTaSelectNormalized(int infoObjAddr, int setVal);

    /**
     * 发送遥调执行归一化值
     * @param infoObjAddr
     * @param setVal
     * @return
     */
    MasterFuture<SendTaExecuteRes> sendTaExecuteNormalized(int infoObjAddr, int setVal);

    /**
     * 发送遥调选择短浮点数
     * @param infoObjAddr
     * @param setVal
     * @return
     */
    MasterFuture<SendTaSelectRes> sendTaSelectFloat(int infoObjAddr, float setVal);

    /**
     * 发送遥调执行短浮点数
     * @param infoObjAddr
     * @param setVal
     * @return
     */
    MasterFuture<SendTaExecuteRes> sendTaExecuteFloat(int infoObjAddr, float setVal);

}
