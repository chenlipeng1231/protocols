package com.clp.protocol.iec104.client.async;

import com.clp.protocol.iec104.client.Iec104MasterManager;

/**
 * 可能弃用
 * @param <V>
 */
public class DefaultClientPromise<V> extends AbstarctClientPromise<DefaultClientPromise<V>, V> {
    public DefaultClientPromise(Iec104MasterManager iec104Client) {
        super(iec104Client);
    }
}
