package com.clp.protocol.iec104.apdu.asdu.infoobj.infoelem;

import com.clp.protocol.iec104.definition.TypeTag;

/**
 * 遥调信息元素类型
 */
public abstract class TaInfoElem extends InfoElem implements HasTmInfo {
    protected TaInfoElem(TypeTag typeTag) {
        super(typeTag);
    }
}
