package com.clp.protocol.iec104.client.pipeline.codec;

import com.clp.protocol.iec104.client.pipeline.MPipelineManager;
import com.clp.protocol.iec104.common.pipeline.AbstractCompleteApduDecoder;

public class MCompleteApduDecoder extends AbstractCompleteApduDecoder {

    private final MPipelineManager pipelineManager;

    public MCompleteApduDecoder(MPipelineManager pipelineManager) {
        this.pipelineManager = pipelineManager;
    }
}
