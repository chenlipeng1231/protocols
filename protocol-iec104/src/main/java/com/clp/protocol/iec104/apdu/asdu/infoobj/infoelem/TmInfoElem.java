package com.clp.protocol.iec104.apdu.asdu.infoobj.infoelem;

import com.clp.protocol.iec104.definition.TypeTag;

/**
 * 遥测信息元素类型
 */
public abstract class TmInfoElem extends InfoElem implements HasTmInfo {
    protected TmInfoElem(TypeTag typeTag) {
        super(typeTag);
    }
}
