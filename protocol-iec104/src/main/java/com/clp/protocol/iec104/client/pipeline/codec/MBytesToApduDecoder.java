package com.clp.protocol.iec104.client.pipeline.codec;

import com.clp.protocol.iec104.client.pipeline.MPipelineManager;
import com.clp.protocol.iec104.common.pipeline.AbstractBytesToApduDecoder;
import lombok.extern.slf4j.Slf4j;

/**
 * 不需释放 bytebuf
 */
@Slf4j
public class MBytesToApduDecoder extends AbstractBytesToApduDecoder {
    private final MPipelineManager pipelineManager;

    public MBytesToApduDecoder(MPipelineManager pipelineManager) {
        this.pipelineManager = pipelineManager;
    }
}
