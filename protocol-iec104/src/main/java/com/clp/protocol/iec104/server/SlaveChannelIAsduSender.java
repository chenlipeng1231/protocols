package com.clp.protocol.iec104.server;

import com.clp.protocol.iec104.server.async.SlaveChannelFuture;

import java.math.BigDecimal;

public interface SlaveChannelIAsduSender {

    SlaveChannel slaveChannel();

    /**
     * 发送变化遥测
     * @param infoObjAddr
     * @param val
     * @return
     */
    SlaveChannelFuture<Void> sendVaryTm(int infoObjAddr, BigDecimal val, boolean isValid);

    /**
     * 发送变位遥信
     * @param infoObjAddr
     * @param isSwitchOn
     * @return
     */
    SlaveChannelFuture<Void> sendVaryTs(int infoObjAddr, boolean isSwitchOn,
                                               boolean isValid, boolean isCurrVal, boolean isReplaced, boolean isLocked);

}
