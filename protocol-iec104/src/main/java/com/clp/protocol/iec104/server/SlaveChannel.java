package com.clp.protocol.iec104.server;

import com.clp.protocol.iec104.server.async.DefaultSlaveChannelPromise;
import com.clp.protocol.iec104.server.async.SlaveChannelPromise;
import com.clp.protocol.iec104.server.pipeline.state.control.ControlInfo;

import java.util.concurrent.Executor;

public interface SlaveChannel {

    Executor executor();

    /**
     * 所属子站
     * @return
     */
    Slave slave();

    /**
     * 远程ip
     * @return
     */
    String remoteHost();

    /**
     * 远程端口号
     * @return
     */
    int remotePort();

    /**
     * 控制信息
     * @return
     */
    ControlInfo controlInfo();

    SlaveChannelIAsduSender getIAsduSender();

    default <V> SlaveChannelPromise<V> newPromise(V res) {
        return new DefaultSlaveChannelPromise<V>(this, res);
    }

    default SlaveChannelPromise<Void> newVoidPromise() {
        return new DefaultSlaveChannelPromise<>(this, null);
    }

}
