package com.clp.protocol.iec104.client.pipeline.state.data;

import com.clp.protocol.core.async.IPromise;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

/**
 * Promise的Map
 * @param <P>
 */
public class PromiseMap<P extends IPromise<?, ?>> {
    /**
     * 键：genKey()方法生成
     * 值：对应的Promise列表
     */
    private Map<String, List<P>> map;

    public PromiseMap() {
        this.map = new HashMap<>();
    }

    /**
     * 根据公共地址和信息体地址生成键
     * @param rtuAddr
     * @param addr
     * @return
     */
    private String genKey(int rtuAddr, int addr) {
        return "" + rtuAddr + "-" + addr;
    }

    /**
     * 添加新的promise
     * @param rtuAddr
     * @param addr
     * @param promise
     */
    public synchronized void put(int rtuAddr, int addr, P promise) {
        List<P> promises = map.get(genKey(rtuAddr, addr));
        if (promises == null) {
            promises = new ArrayList<>();
            promises.add(promise);
            map.put(genKey(rtuAddr, addr), promises);
        } else {
            promises.add(promise);
        }
    }

    /**
     * 判断promise列表中是否有满足filter条件的promise，如果有，则移除
     * @param rtuAddr 公共地址
     * @param addr 信息体地址
     * @param filter 条件
     */
    public synchronized void removeIf(int rtuAddr, int addr, Predicate<P> filter) {
        List<P> promises = map.get(genKey(rtuAddr, addr));
        if (promises != null) {
            promises.removeIf(filter);
        }
    }
}
