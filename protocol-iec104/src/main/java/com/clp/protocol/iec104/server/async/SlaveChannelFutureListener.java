package com.clp.protocol.iec104.server.async;

import com.clp.protocol.core.async.IFutureListener;

public interface SlaveChannelFutureListener<V> extends IFutureListener<SlaveChannelFuture<V>> {
}
