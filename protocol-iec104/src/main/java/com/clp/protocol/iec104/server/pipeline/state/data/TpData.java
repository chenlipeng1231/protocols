package com.clp.protocol.iec104.server.pipeline.state.data;

public interface TpData extends PointData {

    long getValue();

    boolean isValid();

    boolean isAdjusted();

    boolean isOverflowed();

    int getSeq();

    class Comparator implements java.util.Comparator<TpData> {
        public static final Comparator INSTANCE = new Comparator();

        private Comparator() {}

        @Override
        public int compare(TpData o1, TpData o2) {
            return o1.getAddress() - o2.getAddress();
        }
    }

}
