package com.clp.protocol.iec104.server.async;

import com.clp.protocol.core.async.IPromise;

/**
 * 主站Promise
 * @param <V> 值
 */
public interface SlavePromise<V> extends SlaveFuture<V>, IPromise<V, SlaveFutureListener<V>> {

    @Override
    SlavePromise<V> setRes(V val);

    @Override
    SlavePromise<V> setSuccess();

    @Override
    SlavePromise<V> setFailure(Throwable cause);

    @Override
    SlavePromise<V> sync();

    @Override
    SlavePromise<V> sync(int timeoutMs);

    @Override
    SlavePromise<V> addListener(SlaveFutureListener<V> listener);

}
