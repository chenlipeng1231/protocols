package com.clp.protocol.iec104.definition;

import com.clp.protocol.core.utils.EnumElemDoesNotExistException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 遥控的相关定义
 */
public interface Tc {
    @Getter
    enum Type {
        ONE_POINT(TypeTag.C_SC_NA_1),
        TWO_POINT(TypeTag.C_DC_NA_1);

        private final TypeTag typeTag;

        Type(TypeTag typeTag) {
            this.typeTag = typeTag;
        }
    }

    /**
     * 命令类型
     */
    @Getter
    @AllArgsConstructor
    enum CmdType {
        /**
         * 1为遥控选择命令
         */
        SELECT(1, (byte) 0x80, "遥控选择"),
        /**
         * 0为遥控执行命令
         */
        EXECUTE(0, (byte) 0x00, "遥控执行");

        private final int val;
        /**
         * 掩码
         */
        private final byte byteMask;
        private final String desc;

        public static CmdType gain(int val) {
            for (CmdType cmdType : CmdType.values()) {
                if (cmdType.getVal() == val) {
                    return cmdType;
                }
            }
            throw new EnumElemDoesNotExistException(CmdType.class);
        }
    }

    /**
     * 单点控制开关状态
     */
    @Getter
    @AllArgsConstructor
    enum OnePointSwitch {
        /**
         * 1为开关合
         */
        ON(1, (byte) 0x01, "开关合"),
        /**
         * 0为开关分
         */
        OFF(0, (byte) 0x00, "开关分");

        private final int val;
        private final byte byteMask;
        private final String desc;

        public static OnePointSwitch gain(int val) {
            for (OnePointSwitch onePointSwitch : OnePointSwitch.values()) {
                if (onePointSwitch.getVal() == val) {
                    return onePointSwitch;
                }
            }
            throw new EnumElemDoesNotExistException(OnePointSwitch.class);
        }
    }

    /**
     * 双点控制开关状态
     */
    @Getter
    @AllArgsConstructor
    enum TwoPointSwitch {
        /**
         * 2为开关合
         */
        ON(2, (byte) 0x02, "开关合"),
        /**
         * 1为开关分
         */
        OFF(1, (byte) 0x01, "开关分");

        private final int val;
        private final byte byteMask;
        private final String desc;

        public static TwoPointSwitch gain(int val) {
            for (TwoPointSwitch twoPointSwitch : TwoPointSwitch.values()) {
                if (twoPointSwitch.getVal() == val) {
                    return twoPointSwitch;
                }
            }
            throw new EnumElemDoesNotExistException(TwoPointSwitch.class);
        }
    }
}
