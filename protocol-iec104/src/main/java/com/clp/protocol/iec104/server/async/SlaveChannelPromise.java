package com.clp.protocol.iec104.server.async;

import com.clp.protocol.core.async.IPromise;

public interface SlaveChannelPromise<V> extends SlaveChannelFuture<V>, IPromise<V, SlaveChannelFutureListener<V>> {

    @Override
    SlaveChannelPromise<V> setRes(V val);

    @Override
    SlaveChannelPromise<V> setSuccess();

    @Override
    SlaveChannelPromise<V> setFailure(Throwable cause);

    @Override
    SlaveChannelPromise<V> sync();

    @Override
    SlaveChannelPromise<V> sync(int timeoutMs);

    @Override
    SlaveChannelPromise<V> addListener(SlaveChannelFutureListener<V> listener);

}
