package com.clp.protocol.iec104.server.pipeline.state.data;

import com.clp.protocol.iec104.definition.Tc;
import com.clp.protocol.iec104.server.SlaveChannel;
import lombok.Getter;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

/**
 * 遥控数据反应器
 */
@Getter
public abstract class TcDataReactor extends DataReactor {
    private final Tc.Type tcType;

    public TcDataReactor(Executor executor, Tc.Type tcType) {
        super(executor);
        this.tcType = tcType;
    }

    /**
     * 是否接受遥控选择
     * @param slaveChannel
     * @return
     */
    public abstract CompletableFuture<Boolean> acceptSelect(SlaveChannel slaveChannel, int infoObjAddr);

    /**
     * 是否接受遥控执行
     * @param slaveChannel
     * @return
     */
    public abstract CompletableFuture<Boolean> acceptExecute(SlaveChannel slaveChannel, int infoObjAddr);

}
