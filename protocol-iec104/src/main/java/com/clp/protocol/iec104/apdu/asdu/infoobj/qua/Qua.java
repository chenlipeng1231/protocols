package com.clp.protocol.iec104.apdu.asdu.infoobj.qua;

import com.clp.protocol.iec104.definition.TypeTag;
import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePduClip;
import com.clp.protocol.iec104.apdu.asdu.infoobj.TypeTagRequired;

public abstract class Qua extends BaseNBytePduClip<Qua> implements TypeTagRequired {
    private final TypeTag typeTag;

    protected Qua(TypeTag typeTag) {
        this.typeTag = typeTag;
    }

    @Override
    public TypeTag typeTag() {
        return typeTag;
    }

    public boolean isTmQua() {
        return this instanceof TmQua;
    }

    public TmQua castToTmQua() {
        return castTo(TmQua.class);
    }

    @SuppressWarnings("unchecked")
    public  <T extends Qua> T castTo(Class<T> clazz) {
        return ((T) this);
    }
}
