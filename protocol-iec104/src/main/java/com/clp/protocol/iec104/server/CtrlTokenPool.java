package com.clp.protocol.iec104.server;

import javax.annotation.Nullable;
import java.util.Collection;

public interface CtrlTokenPool<T extends CtrlToken> {

    /**
     * 申请令牌
     * @param slaveChannel
     * @param address
     * @return
     */
    @Nullable
    T applyFor(int address, SlaveChannel slaveChannel);

    boolean returnBack(T token);

    boolean returnBack(Collection<T> tokens);

}
