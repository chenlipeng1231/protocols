package com.clp.protocol.iec104.client.pipeline.state;

import com.clp.protocol.iec104.apdu.ApduFactory;
import com.clp.protocol.iec104.client.InMaster;
import com.clp.protocol.iec104.client.pipeline.MPipelineManager;
import io.netty.channel.ChannelFuture;

public class MInternalSApduSender {
    private final MPipelineManager pipelineManager;

    public MInternalSApduSender(MPipelineManager pipelineManager) {
        this.pipelineManager = pipelineManager;
    }

    private InMaster master() {
        return pipelineManager.getInMaster();
    }

    /**
     * @param recvSeq
     */
    public ChannelFuture chSendAck(int recvSeq) {
        return master().channel().writeAndFlush(ApduFactory.getSApdu(recvSeq));
    }

}
