package com.clp.protocol.iec104.client;

import com.clp.protocol.core.event.ListenableContainer;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nullable;
import java.util.concurrent.ExecutorService;

/**
 * 主站集合容器（线程安全）
 */
@Slf4j
public class MasterContainer extends ListenableContainer<Master> {
    public MasterContainer(ExecutorService executor) {
        super(executor);
    }

    /**
     * 如果没有这个主站，就添加
     * @param master
     * @return
     */
    @Override
    public boolean add(Master master) {
        boolean isAdded = super.add(master);
        if (isAdded) {
            log.info("向Iec104主站容器中添加新的主站：{}。\n当前容器状态：\n{}", master, this);
        } else {
            log.warn("向Iec104主站容器中添加新的主站失败！可能已经存在：{}。\n当前容器状态：\n{}", master, this);
        }
        return isAdded;
    }

    /**
     * 根据远程ip地址和端口号获取主站
     * @param remoteHost 远程ip地址
     * @param remotePort 远程端口号
     * @return
     */
    @Nullable
    public Master getOne(String remoteHost, int remotePort) {
        return getOne(master -> master.remoteHost().equals(remoteHost) && master.remotePort() == remotePort);
    }

    /**
     * 判断容器中是否有指定远程ip和端口号的主站
     * @param remoteHost 远程ip
     * @param remotePort 远程端口号
     * @return 对应的主站
     */
    public boolean contains(String remoteHost, int remotePort) {
        return contains(master -> master.remoteHost().equals(remoteHost) && master.remotePort() == remotePort);
    }

    /**
     * 如果有这个主站，就移除
     *
     * @param element
     * @return
     */
    @Override
    public boolean remove(Object element) {
        boolean isRemoved = super.remove(element);
        if (isRemoved) {
            log.info("从Iec104主站容器中移除主站：{}。\n当前容器状态：{}", element, this);
        } else {
            log.warn("从Iec104主站容器中移除主站失败！可能已经不存在：{}。\n当前容器状态：{}", element, this);
        }
        return isRemoved;
    }

    /**
     * 移除所有主站
     * @return
     */
    @Override
    public void clear() {
        super.clear();
        log.info("从Iec104主站容器中移除所有主站。当前个数为：{}", 0);
    }

    @Override
    public String toString() {
        return "主站列表：\n" + super.toString();
    }
}
