package com.clp.protocol.iec104.server;

import javax.annotation.Nullable;

/**
 * 控制令牌
 */
public interface CtrlToken {

    /**
     * 当前的申请者，为null代表当前无申请者
     * @return
     */
    @Nullable
    SlaveChannel getApplier();

    /**
     * 信息体地址
     * @return
     */
    int getAddress();

}
