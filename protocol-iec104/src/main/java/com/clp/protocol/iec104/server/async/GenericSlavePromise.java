package com.clp.protocol.iec104.server.async;

import com.clp.protocol.iec104.server.Slave;

import java.util.LinkedList;
import java.util.List;

/**
 * 通用的 SlavePromise
 * @param <P> 子promise
 * @param <V> 值
 */
public abstract class GenericSlavePromise<P extends GenericSlavePromise<P, V>, V> implements SlavePromise<V> {
    protected volatile boolean isDone = false; // 是否结束
    protected volatile boolean isSuccess = false; // 是否成功
    protected volatile V res = null; // 结果
    protected volatile Throwable cause = null; // 异常、错误原因
    protected final List<SlaveFutureListener<V>> listeners = new LinkedList<>(); // 监听器列表，当执行结束后会通知该列表的监听器并执行对应的方法

    private final Slave slave; // 对应的主站

    protected GenericSlavePromise(Slave slave) {
        this.slave = slave;
    }

    @Override
    public Slave slave() {
        return slave;
    }

    @SuppressWarnings("unchecked")
    protected P self() {
        return ((P) this);
    }

    @Override
    public boolean isDone() {
        return isDone;
    }

    @Override
    public boolean isSuccess() {
        return isDone() && isSuccess;
    }

    @Override
    public V getRes() {
        return res;
    }

    @Override
    public P setRes(V val) {
        synchronized (this) {
            this.res = val;
            return self();
        }
    }

    /**
     * 只会设置一次，重复设置第2次无效
     * @return
     */
    @Override
    public P setSuccess() {
        synchronized (this) {
            if (this.isDone()) return self();
            this.isSuccess = true;
            this.isDone = true;

            notifyAllListeners();
            this.notifyAll();
            return self();
        }
    }

    /**
     * 只会设置一次，重复设置第2次无效
     * @param cause
     * @return
     */
    @Override
    public P setFailure(Throwable cause) {
        synchronized (this) {
            if (this.isDone()) return self();
            this.isSuccess = false;
            this.cause = cause;
            this.isDone = true;

            notifyAllListeners();
            this.notifyAll();
            return self();
        }
    }

    @Override
    public P addListener(SlaveFutureListener<V> listener) {
        synchronized (this) {
            listeners.add(listener);
            if (this.isDone()) {
                notifyAllListeners();
            }
            return self();
        }
    }

    /**
     * 通知所有监听器
     */
    protected void notifyAllListeners() {
        GenericSlavePromise<P, V> thisPromise = this;
        if (slave() != null) {
            slave().executor().execute(() -> {
                synchronized (thisPromise) {
                    for (SlaveFutureListener<V> listener : listeners) {
                        listener.operationComplete(self());
                    }
                    listeners.clear();
                }
            });
        } else {
            synchronized (thisPromise) {
                for (SlaveFutureListener<V> listener : listeners) {
                    listener.operationComplete(self());
                }
                listeners.clear();
            }
        }
    }

    @Override
    public P sync() {
        synchronized (this) {
            while (!isDone()) {
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return self();
        }
    }

    @Override
    public P sync(int timeoutMs) {
        synchronized (this) {
            long beginMs = System.currentTimeMillis();
            long passedMs = 0;
            while (!isDone()) {
                long waitMs = timeoutMs - passedMs;
                if (waitMs <= 0) break;
                try {
                    this.wait(waitMs);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                passedMs = System.currentTimeMillis() - beginMs;
            }
        }
        return self();
    }

    @Override
    public Throwable cause() {
        return cause;
    }
}
