package com.clp.protocol.iec104.server;

import com.clp.protocol.iec104.definition.Ta;
import com.clp.protocol.core.utils.AssertUtil;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

public class TaToken implements CtrlToken {

    private enum State {
        R_SELECT,
        S_SELECT_ACK_YES,
        S_SELECT_ACK_NO,
        R_EXECUTE,
        S_EXECUTE_ACK_YES,
        S_EXECUTE_ACK_NO,
        S_FINISHED
    }

    @Getter
    private final int address;

    @Getter
    private volatile SlaveChannel applier; // 当前的申请者
    private volatile State state;
    @Getter
    private volatile Ta.Type type;
    private volatile float floatVal; // 短浮点数值

    public TaToken(int addr) {
        this.address = addr;
    }

    void setApplier(SlaveChannel newApplier) {
        // 要求当前没有申请者
        if (applier != null) {
            throw new IllegalStateException("当前已经有了一个申请者：" + applier);
        }
        AssertUtil.notNull(newApplier, "slaveChannel");
        applier = newApplier;
        resetState();
    }

    private void resetState() {
        state = State.S_FINISHED;
    }

    void removeApplier() {
        applier = null;
    }

    public void setRSelectFloat(float floatVal) {
        checkStateIn(State.S_FINISHED);
        this.state = State.R_SELECT;
        this.type = Ta.Type.FLOAT;
        this.floatVal = floatVal;
    }

    private void checkStateIn(State... states) {
        for (State stateToCheck : states) {
            if (state == stateToCheck) return;
        }
        throw new IllegalStateException("当前状态：" + state + "，需要状态：" + Arrays.toString(states));
    }

    public boolean isRSelect() {
        return state == State.R_SELECT;
    }

    public void setSSelectAck(Ta.Type type, boolean isAckYes) {
        checkType(type);
        checkStateIn(State.R_SELECT);
        if (isAckYes) {
            this.state = State.S_SELECT_ACK_YES;
        } else {
            this.state = State.S_SELECT_ACK_NO;
        }
    }

    public void setSSelectFloatAck(boolean isAckYes) {
        checkType(Ta.Type.FLOAT);
        checkStateIn(State.R_SELECT);
        if (isAckYes) {
            this.state = State.S_SELECT_ACK_YES;
        } else {
            this.state = State.S_SELECT_ACK_NO;
        }
    }

    private void checkType(Ta.Type typeToCheck) {
        if (type != typeToCheck) {
            throw new IllegalStateException("当前遥控点类型：" + type + "，需要点类型：" + typeToCheck);
        }
    }

    public boolean isSSelectAck(boolean isAckYes) {
        if (isAckYes) return state == State.S_SELECT_ACK_YES;
        return state == State.S_SELECT_ACK_NO;
    }

    public void setRExecuteFloat(float floatVal) {
        checkStateIn(State.S_SELECT_ACK_YES);
        checkType(Ta.Type.FLOAT);
        checkFloatVal(floatVal);
        this.state = State.R_EXECUTE;
    }

    public boolean isRExecute() {
        return state == State.R_EXECUTE;
    }

    private void checkFloatVal(float floatVal) {
        if (Float.compare(this.floatVal, floatVal) != 0) {
            throw new IllegalStateException("当前短浮点数值：" + this.floatVal + "，需要短浮点数值：" + floatVal);
        }
    }

    public void setSExecuteFloatAck(boolean isAckYes) {
        checkType(Ta.Type.FLOAT);
        checkStateIn(State.R_EXECUTE);
        if (isAckYes) {
            this.state = State.S_EXECUTE_ACK_YES;
        } else {
            this.state = State.S_EXECUTE_ACK_NO;
        }
    }

    public void setSFloatFinished() {
        checkType(Ta.Type.FLOAT);
        checkStateIn(State.S_EXECUTE_ACK_YES, State.S_EXECUTE_ACK_NO);
        this.state = State.S_FINISHED;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaToken taToken = (TaToken) o;
        return address == taToken.address;
    }

    @Override
    public int hashCode() {
        return Objects.hash(address);
    }
}
