package com.clp.protocol.iec104.client.async.sendapdu;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 发送遥控选择 的结果
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class SendTcSelectRes {
    private volatile boolean isSendSelectSuccess; // 是否成功发送遥控选择

    private volatile boolean isRecvSelectAckYes; // 是否接收到遥控选择肯定确认
    private volatile boolean isRecvSelectAckNo; // 是否接收到遥控选择否定确认

    private volatile String failDesc; // 失败描述
}
