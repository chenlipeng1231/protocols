package com.clp.protocol.iec104.apdu.apci;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class SApci extends Apci<SApci, SCtrlArea> {
    public SApci(byte head, int length, SCtrlArea sControlArea) {
        super(head, length, sControlArea);
    }
}
