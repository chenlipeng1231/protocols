package com.clp.protocol.iec104.common;

public class IncorrectSendSeqException extends IncorrectSeqException {

    public IncorrectSendSeqException(int sendSeq) {
        super("Received apdu has an incorrect send sequence: " + sendSeq);
    }
}
