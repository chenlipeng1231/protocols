package com.clp.protocol.iec104.common.pipeline;

import com.clp.protocol.iec104.apdu.Apdu;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public abstract class AbstractApduToBytesEncoder extends MessageToByteEncoder<Apdu> {

    protected AbstractApduToBytesEncoder() {}

    @Override
    protected void encode(ChannelHandlerContext ctx, Apdu apdu, ByteBuf out) throws Exception {
        apdu.writeBytesTo(out);
    }
}
