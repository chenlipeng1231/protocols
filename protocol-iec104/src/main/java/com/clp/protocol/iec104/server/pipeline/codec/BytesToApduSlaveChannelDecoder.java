package com.clp.protocol.iec104.server.pipeline.codec;

import com.clp.protocol.iec104.common.pipeline.AbstractBytesToApduDecoder;
import com.clp.protocol.iec104.server.pipeline.PipelineManager;

public class BytesToApduSlaveChannelDecoder extends AbstractBytesToApduDecoder {
    private final PipelineManager manager;

    public BytesToApduSlaveChannelDecoder(PipelineManager manager) {
        this.manager = manager;
    }
}
