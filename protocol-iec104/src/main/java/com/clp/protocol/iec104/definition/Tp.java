package com.clp.protocol.iec104.definition;

import com.clp.protocol.core.utils.EnumElemDoesNotExistException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 遥脉的相关定义
 */
public interface Tp {
    /**
     * 有效标志位
     */
    @Getter
    @AllArgsConstructor
    enum Iv {
        VALID(0, "有效"),
        NOT_VALID(1, "无效");

        private final int val;
        private final String desc;

        public static Iv gain(int val) {
            for (Iv iv : Iv.values()) {
                if (iv.val == val) {
                    return iv;
                }
            }
            throw new EnumElemDoesNotExistException(Iv.class);
        }

        public static Iv gain(boolean isValid) {
            if (isValid) return VALID;
            return NOT_VALID;
        }
    }

    /**
     * 调整标志位
     */
    @Getter
    @AllArgsConstructor
    enum Ca {
        CA_NO(0, "上次读数后未调整"),
        CA_YES(1, "上次读数后被调整");

        private final int val;
        private final String desc;

        public static Ca gain(int val) {
            for (Ca ca : Ca.values()) {
                if (ca.val == val) {
                    return ca;
                }
            }
            throw new EnumElemDoesNotExistException(Ca.class);
        }

        public static Ca gain(boolean isAdjusted) {
            if (isAdjusted) return CA_YES;
            return CA_NO;
        }
    }

    /**
     * 溢出标志位
     */
    @Getter
    @AllArgsConstructor
    enum Cy {
        CY_NO(0, "无溢出"),
        CY_YES(1, "溢出");

        private final int val;
        private final String desc;

        public static Cy gain(int val) {
            for (Cy cy : Cy.values()) {
                if (cy.val == val) {
                    return cy;
                }
            }
            throw new EnumElemDoesNotExistException(Cy.class);
        }

        public static Cy gain(boolean isOverFlowed) {
            if (isOverFlowed) return CY_YES;
            return CY_NO;
        }
    }
}
