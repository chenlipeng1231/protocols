package com.clp.protocol.iec104.common;

public class IncorrectRecvSeqException extends IncorrectSeqException {
    public IncorrectRecvSeqException(int recvSeq) {
        super("Received apdu has an incorrect receive sequence: " + recvSeq);
    }
}
