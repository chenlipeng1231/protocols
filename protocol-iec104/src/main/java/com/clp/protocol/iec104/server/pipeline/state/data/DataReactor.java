package com.clp.protocol.iec104.server.pipeline.state.data;

import com.clp.protocol.core.utils.AssertUtil;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.Supplier;

/**
 * 数据反应器：用于对数据的传输过程进行反应
 *  一般来说，每个SlaveChannel都有一个唯一的netty线程去处理其IO操作和任务等，子站接收了多个SlaveChannel之后，便存在多个线程同时访问该类的情况
 *  该类的方法可能有些比较耗时，比如查询数据库实时数据等，为了避免netty线程在此处长时间运行、等待以及同步影响IO效率，
 *  可以通过 CompletableFuture 委托执行器去异步执行，等到执行结束后再监听结果，进行后续的处理（需要注意后续处理的线程是谁，避免线程安全问题）
 */
public abstract class DataReactor {
    private final Executor executor;

    protected DataReactor(Executor executor) {
        AssertUtil.notNull(executor, "executor");
        this.executor = executor;
    }

    protected <V> CompletableFuture<V> supplyAsync(Supplier<V> supplier) {
        return CompletableFuture.supplyAsync(supplier, executor);
    }
}
