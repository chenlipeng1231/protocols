package com.clp.protocol.iec104.apdu.asdu;

import com.clp.protocol.iec104.apdu.asdu.infoobj.CInfoObj;
import com.clp.protocol.iec104.apdu.asdu.infoobj.InfoObj;
import com.clp.protocol.iec104.apdu.asdu.infoobj.NcInfoObj;
import com.clp.protocol.iec104.definition.ConstVal;
import com.clp.protocol.iec104.definition.TypeTag;
import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePdu;
import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePduClip;
import com.clp.protocol.core.pdu.ByteToStringFormat;
import com.clp.protocol.core.utils.ByteUtil;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class IAsdu extends BaseNBytePdu<IAsdu> {
    /**
     * 类型标识，1字节
     */
    private TypeTag typeTag;

    /**
     * 可变结构限定词，
     */
    private Vsq vsq;

    /**
     * 传输原因，
     */
    private Cot cot;

    /**
     * 终端地址，2个字节，也就是应用服务数据单元公共地址
     */
    private int rtuAddr;

    /**
     * 信息体列表
     */
    private List<InfoObj> infoObjs;

    @Override
    public IAsdu refreshFrom(ByteBuf buf) {
        // 1、获取类型标识
        this.typeTag = TypeTag.gain(buf.readByte());
        // 2、获取可变结构限定词
        this.vsq = new Vsq().refreshFrom(buf);
        // 3、获取传输原因
        this.cot = new Cot().refreshFrom(buf);
        // 4、获取应用服务单元公共地址（RTU地址）
        this.rtuAddr = buf.readShortLE() & 0xFFFF;
        // 5、获取信息对象列表
        this.infoObjs = new ArrayList<>();
        if (!typeTag.hasVsq()) {
            // 可变结构限定词无效的情况：文件传输
        } else {
            switch (vsq.getSq()) {
                case CONTINUE:
                    // 连续的，前三个字节是基地址
                    byte[] infoElemBytes = new byte[ConstVal.INFO_ELEM_ADDRESS_LEN];
                    buf.readBytes(infoElemBytes);
                    int addr = ByteUtil.bytes3ToIntLE(infoElemBytes);

                    // 设置信息元素列表（至少有循环一次）
                    int num = 0;
                    while (num < vsq.getNum()) {
                        infoObjs.add(new CInfoObj(typeTag, addr).refreshFrom(buf));
                        addr += 1;
                        num += 1;
                    }
                    break;
                case NOT_CONTINUE:
                    // 设置信息元素列表（至少有循环一次）
                    num = 0;
                    while (num < vsq.getNum()) {
                        // 设置信息元素列表
                        infoObjs.add(new NcInfoObj(typeTag).refreshFrom(buf));
                        num += 1;
                    }
                    break;
            }
        }
        return this;
    }

    @Override
    public boolean isValid() {
        if (typeTag == null) return false;
        if (vsq == null || !vsq.isValid()) return false;
        if (cot == null || !cot.isValid()) return false;
        if (rtuAddr < 0 || rtuAddr > 0xFFFF) return false;
        if (infoObjs == null || infoObjs.isEmpty()) return false;
        for (InfoObj infoObj : infoObjs) {
            if (!infoObj.isValid()) return false;
        }
        return true;
    }

    @Override
    public void writeBytesTo(ByteBuf buf) {
        buf.writeByte(typeTag.getVal());
        vsq.writeBytesTo(buf);
        cot.writeBytesTo(buf);
        buf.writeShortLE(rtuAddr);
        if (!typeTag.hasVsq()) {
            // 可变结构限定词无效的情况：文件传输
            infoObjs.get(0).writeBytesTo(buf);
        } else {
            switch (vsq.getSq()) {
                case CONTINUE:
                    // 连续的，取第1个信息体的地址
                    buf.writeBytes(ByteUtil.intToBytes3LE(infoObjs.get(0).getAddr()));
                    int num = 0;
                    while (num < vsq.getNum()) {
                        infoObjs.get(num).writeBytesTo(buf);
                        num += 1;
                    }
                    break;
                case NOT_CONTINUE:
                    // 设置信息元素列表（至少有循环一次）
                    num = 0;
                    while (num < vsq.getNum()) {
                        infoObjs.get(num).writeBytesTo(buf);
                        num += 1;
                    }
                    break;
            }
        }
    }

    @Override
    public void writeFormattedByteStringsTo(StringBuilder sb, String frameClipBytesSeparator, String byteSeparator, ByteToStringFormat byteFormat) {
        sb.append(byteFormat.format(typeTag.getVal()));
        sb.append(frameClipBytesSeparator);
        vsq.writeFormattedByteStringsTo(sb, frameClipBytesSeparator, byteSeparator, byteFormat);
        sb.append(frameClipBytesSeparator);
        cot.writeFormattedByteStringsTo(sb, frameClipBytesSeparator, byteSeparator, byteFormat);
        sb.append(frameClipBytesSeparator);
        byte[] rtuAddrBytes = ByteUtil.intToBytes2LE(rtuAddr);
        sb.append(byteFormat.format(rtuAddrBytes[0]));
        sb.append(byteSeparator).append(byteFormat.format(rtuAddrBytes[1]));
        if (!typeTag.hasVsq()) {
            sb.append(frameClipBytesSeparator).append(infoObjs.get(0));
        } else {
            switch (vsq.getSq()) {
                case CONTINUE:
                    // 连续的，取第1个信息体的地址
                    byte[] addrBytes = ByteUtil.intToBytes3LE(infoObjs.get(0).getAddr());
                    sb.append(frameClipBytesSeparator).append(byteFormat.format(addrBytes[0]));
                    for (int i = 1; i < addrBytes.length; i++) {
                        sb.append(byteSeparator).append(byteFormat.format(addrBytes[i]));
                    }

                    int num = 0;
                    while (num < vsq.getNum()) {
                        sb.append(frameClipBytesSeparator);
                        infoObjs.get(num).writeFormattedByteStringsTo(sb, frameClipBytesSeparator, byteSeparator, byteFormat);
                        num += 1;
                    }
                    break;
                case NOT_CONTINUE:
                    // 设置信息元素列表（至少有循环一次）
                    num = 0;
                    while (num < vsq.getNum()) {
                        sb.append(frameClipBytesSeparator);
                        infoObjs.get(num).writeFormattedByteStringsTo(sb, frameClipBytesSeparator, byteSeparator, byteFormat);
                        num += 1;
                    }
                    break;
            }
        }
    }

    @Override
    public void writeSimpleDescriptionTo(StringBuilder sb) {
        sb.append("     [Typ：").append(typeTag.getDesc()).append("(").append(typeTag.getVal() & 0xFF).append(")");
        sb.append(", Vsq："); vsq.writeSimpleDescriptionTo(sb);
        sb.append(", Cot："); cot.writeSimpleDescriptionTo(sb);
        sb.append(", RtuAddr：").append(rtuAddr);
        sb.append("]\n");
        if (!infoObjs.isEmpty()) {
            sb.append("       InfoObjs：");
            for (int i = 0; i < infoObjs.size(); i++) {
                sb.append("\n     [").append(i + 1).append("：");
                infoObjs.get(i).writeSimpleDescriptionTo(sb);
                sb.append("]");
            }
        }
        sb.append("\n");
    }

    @Override
    public void writeDetailDescriptionTo(StringBuilder sb) {
        sb.append("     [ 类型标识：").append(typeTag.getDesc()).append("(").append(typeTag.getVal() & 0xFF).append(")");
        sb.append(", 可变结构限定词："); vsq.writeDetailDescriptionTo(sb);
        sb.append(", 传输原因："); cot.writeDetailDescriptionTo(sb);
        sb.append(", 公共地址：").append(rtuAddr).append(" ]\n");
        if (!infoObjs.isEmpty()) {
            sb.append("       信息元素集：");
            for (int i = 0; i < infoObjs.size(); i++) {
                sb.append("\n     [ 信息体").append(i + 1).append("：");
                infoObjs.get(i).writeDetailDescriptionTo(sb);
                sb.append(" ]");
            }
        }
        sb.append("\n");
    }

    @Override
    protected void forEachOneLevelChild(Consumer<BaseNBytePduClip<?>> consumer) {
        consumer.accept(vsq);
        consumer.accept(cot);
        infoObjs.forEach(consumer);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IAsdu iAsdu = (IAsdu) o;
        return rtuAddr == iAsdu.rtuAddr && typeTag == iAsdu.typeTag && Objects.equals(vsq, iAsdu.vsq) && Objects.equals(cot, iAsdu.cot) && Objects.equals(infoObjs, iAsdu.infoObjs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeTag, vsq, cot, rtuAddr, infoObjs);
    }
}
