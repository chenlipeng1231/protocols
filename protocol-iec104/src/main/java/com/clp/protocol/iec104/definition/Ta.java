package com.clp.protocol.iec104.definition;

import lombok.Getter;

/**
 * 遥调的相关定义
 */
public interface Ta {
    /**
     * 值类型
     */
    @Getter
    enum Type {
        /**
         * 归一化值
         */
        NORMALIZE(TypeTag.C_SE_NA_1),
        /**
         * 标度化值
         */
        SCALED(TypeTag.C_SE_NB_1),
        /**
         * 短浮点数
         */
        FLOAT(TypeTag.C_SE_NC_1);

        private final TypeTag typeTag;

        Type(TypeTag typeTag) {
            this.typeTag = typeTag;
        }
    }
}
