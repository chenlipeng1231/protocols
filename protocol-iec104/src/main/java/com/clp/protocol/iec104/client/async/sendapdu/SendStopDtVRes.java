package com.clp.protocol.iec104.client.async.sendapdu;

import com.clp.protocol.iec104.common.res.BaseRes;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 发送停止传输激活 的结果
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class SendStopDtVRes extends BaseRes<SendStopDtVRes> {
    private volatile boolean isRecvAck; // 是否成功接收停止传输激活确认
}
