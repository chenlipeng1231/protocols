package com.clp.protocol.iec104.server.async;

import com.clp.protocol.iec104.server.SlaveChannel;
import com.clp.protocol.core.async.IFuture;

public interface SlaveChannelFuture<V> extends IFuture<V, SlaveChannelFutureListener<V>> {
    /**
     * 返回与此Future相关的主站
     * @return
     */
    SlaveChannel slaveChannel();

    @Override
    SlaveChannelFuture<V> sync();

    @Override
    SlaveChannelFuture<V> sync(int timeoutMs);

    @Override
    SlaveChannelFuture<V> addListener(SlaveChannelFutureListener<V> listener);
}
