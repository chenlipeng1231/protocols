package com.clp.protocol.iec104.server.pipeline.state.control;

/**
 * 控制信息
 */
public interface ControlInfo {

    /**
     * 是否初始化结束
     * @return
     */
    boolean isInitCompleted();

    /**
     * 是否激活了启动传输
     * @return
     */
    boolean isStartedDt();

    int k();

    int w();

    int t0();

    int t1();

    int t2();

    int t3();

    int sendSeq();

    int recvSeq();

}
