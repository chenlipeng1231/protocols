package com.clp.protocol.iec104.server.pipeline.state.control;

import com.clp.protocol.iec104.apdu.IApdu;
import com.clp.protocol.iec104.apdu.SApdu;
import com.clp.protocol.iec104.apdu.UApdu;
import com.clp.protocol.iec104.apdu.asdu.IAsdu;
import com.clp.protocol.iec104.apdu.asdu.infoobj.InfoObj;
import com.clp.protocol.iec104.definition.TypeTag;
import com.clp.protocol.iec104.server.pipeline.PipelineManager;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nullable;

@Slf4j
public class ApduLogControlStateHandler extends AbstractControlStateHandler {
    public ApduLogControlStateHandler(PipelineManager manager) {
        super(manager);
    }

    @Override
    protected void resetState() {

    }

    @Override
    protected void afterResetState() {

    }

    @Override
    protected UApdu updateStateByRecv(UApdu uApdu) {
        log.info("[Control] 接收U帧\n{}", uApdu);
        return uApdu;
    }

    @Override
    protected SApdu updateStateByRecv(SApdu sApdu) {
        log.info("[Control] 接收S帧\n{}", sApdu);
        return sApdu;
    }

    @Override
    protected IApdu updateStateByRecv(IApdu iApdu) {log.info("[Control] 接收I帧\n{}", iApdu);
        return iApdu;
    }

    @Override
    protected UApdu updateStateBySend(UApdu uApdu) {
        log.info("[Control] 发送U帧\n{}", uApdu);
        return uApdu;
    }

    @Override
    protected SApdu updateStateBySend(SApdu sApdu) {
        log.info("[Control] 发送S帧\n{}", sApdu);
        return sApdu;
    }

    @Override
    protected IApdu updateStateBySend(IApdu iApdu) {
        log.info("[Control] 发送I帧\n{}", iApdu);
        return iApdu;
    }

    @Nullable
    @Override
    protected ScheduledTask getScheduledTask() {
        return null;
    }
}
