package com.clp.protocol.iec104.client;

import com.clp.protocol.iec104.client.async.DefaultMasterPromise;
import com.clp.protocol.iec104.client.async.MasterPromise;
import com.clp.protocol.iec104.client.pipeline.state.control.MControlInfo;
import com.clp.protocol.iec104.client.pipeline.state.data.MDataInfo;
import com.clp.protocol.core.event.EventListenerRegister;

import java.util.concurrent.Executor;

/**
 * 主站，继承连接信息，同时可以发送和接收Apdu
 */
public interface Master {

    /**
     * 远程IP
     * @return
     */
    String remoteHost();

    /**
     * 远程端口号
     * @return
     */
    int remotePort();

    int remoteRtuAddr();

    /**
     * 公共地址
     * @return
     */
    int localRtuAddr();

    Executor executor();

    /**
     * 是否已连接
     * @return
     */
    boolean isConnected();

    /**
     * 该主站的控制信息
     * @return
     */
    MControlInfo controlInfo();

    MDataInfo dataInfo();

    EventListenerRegister<Master> getEventListenerRegister();

    MasterIAsduSender getIAsduSender();

    MasterIAsduRecver getIAsduRecver();

    default <V> MasterPromise<V> newPromise(V val) {
        return new DefaultMasterPromise<>(this, val);
    }
}
