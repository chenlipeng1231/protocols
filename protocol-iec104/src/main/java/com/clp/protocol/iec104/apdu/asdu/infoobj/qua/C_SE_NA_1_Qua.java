package com.clp.protocol.iec104.apdu.asdu.infoobj.qua;

import com.clp.protocol.iec104.definition.TypeTag;
import com.clp.protocol.iec104.definition.quatype.TaQuaType;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 设定值命令，归一化值
 */
@Getter
public class C_SE_NA_1_Qua extends TaQua {

    public C_SE_NA_1_Qua() {
        this(null);
    }

    public C_SE_NA_1_Qua(TaQuaType type) {
        super(TypeTag.C_SE_NA_1, type);
    }
}
