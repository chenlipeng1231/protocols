package com.clp.protocol.iec104.server.pipeline.state.data;

import java.math.BigDecimal;

/**
 * 遥测数据
 */
public interface TmData extends PointData {

    /**
     * 实际值
     * @return
     */
    BigDecimal getValue();

    /**
     * 是否有效
     * @return
     */
    boolean isValid();

    class Comparator implements java.util.Comparator<TmData> {
        public static final Comparator INSTANCE = new Comparator();

        private Comparator() {}

        @Override
        public int compare(TmData o1, TmData o2) {
            return o1.getAddress() - o2.getAddress();
        }
    }

}
