package com.clp.protocol.iec104.server.async;

import com.clp.protocol.core.async.IFutureListener;

public interface SlaveFutureListener<V> extends IFutureListener<SlaveFuture<V>> {
}
