package com.clp.protocol.iec104.apdu.asdu.infoobj.infoelem;

/**
 * 有遥信信息
 */
public interface HasTsInfo {

    /**
     * 是否有效
     * @return
     */
    boolean isTsValid();

    /**
     * 是否为当前值
     * @return
     */
    boolean isTsCurrVal();

    /**
     * 是否被取代
     */
    boolean isTsReplaced();

    /**
     * 是否被闭锁
     */
    boolean isTsLocked();

    /**
     * 是否闭合
     */
    boolean isTsSwitchOn();

}
