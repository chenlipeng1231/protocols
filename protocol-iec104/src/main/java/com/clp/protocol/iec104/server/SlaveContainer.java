package com.clp.protocol.iec104.server;

import com.clp.protocol.core.event.ListenableContainer;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nullable;
import java.util.concurrent.ExecutorService;

@Slf4j
public class SlaveContainer extends ListenableContainer<Slave> {
    public SlaveContainer(ExecutorService executor) {
        super(executor);
    }

    /**
     * 如果没有这个子站，就添加
     * @param slave
     * @return
     */
    @Override
    public boolean add(Slave slave) {
        boolean isAdded = super.add(slave);
        if (isAdded) {
            log.info("向Iec104子站容器中添加新的子站：{}。\n当前容器状态：\n{}", slave, this);
        } else {
            log.warn("向Iec104子站容器中添加新的子站失败！可能已经存在：{}。\n当前容器状态：\n{}", slave, this);
        }
        return isAdded;
    }

    /**
     * 根据远程ip地址和端口号获取子站
     * @param localHost 远程ip地址
     * @param localPort 远程端口号
     * @return
     */
    @Nullable
    public Slave getOne(String localHost, int localPort) {
        return getOne(slave -> slave.localHost().equals(localHost) && slave.localPort() == localPort);
    }

    /**
     * 判断容器中是否有指定远程ip和端口号的子站
     * @param localHost 远程ip
     * @param localPort 远程端口号
     * @return 对应的子站
     */
    public boolean contains(String localHost, int localPort) {
        return contains(slave -> slave.localHost().equals(localHost) && slave.localPort() == localPort);
    }

    /**
     * 如果有这个子站，就移除
     *
     * @param element
     * @return
     */
    @Override
    public boolean remove(Object element) {
        boolean isRemoved = super.remove(element);
        if (isRemoved) {
            log.info("从Iec104子站容器中移除子站：{}。\n当前容器状态：{}", element, this);
        } else {
            log.warn("从Iec104子站容器中移除子站失败！可能已经不存在：{}。\n当前容器状态：{}", element, this);
        }
        return isRemoved;
    }

    /**
     * 移除所有主站
     * @return
     */
    @Override
    public void clear() {
        super.clear();
        log.info("从Iec104子站容器中移除所有子站。当前个数为：{}", 0);
    }

    @Override
    public String toString() {
        return "子站列表：\n" + super.toString();
    }
}
