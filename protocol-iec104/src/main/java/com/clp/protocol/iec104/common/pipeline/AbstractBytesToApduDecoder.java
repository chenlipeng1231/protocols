package com.clp.protocol.iec104.common.pipeline;

import com.clp.protocol.iec104.apdu.Apdu;
import com.clp.protocol.iec104.apdu.ApduFactory;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public abstract class AbstractBytesToApduDecoder extends ByteToMessageDecoder {

    protected AbstractBytesToApduDecoder() {}

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        try {
            Apdu apdu = ApduFactory.getApdu(byteBuf);
            list.add(apdu);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
