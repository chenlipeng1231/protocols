package com.clp.protocol.iec104.server.pipeline.state.data;

import com.clp.protocol.iec104.apdu.asdu.Cot;
import com.clp.protocol.iec104.apdu.asdu.IAsdu;
import com.clp.protocol.iec104.apdu.asdu.infoobj.InfoObj;
import com.clp.protocol.iec104.apdu.asdu.infoobj.infoelem.C_SE_NC_1_InfoElem;
import com.clp.protocol.iec104.apdu.asdu.infoobj.qua.C_SE_NC_1_Qua;
import com.clp.protocol.iec104.definition.Ta;
import com.clp.protocol.iec104.definition.TypeTag;
import com.clp.protocol.iec104.definition.cot.Cause;
import com.clp.protocol.iec104.definition.cot.Pn;
import com.clp.protocol.iec104.definition.quatype.TaQuaType;
import com.clp.protocol.iec104.server.*;
import com.clp.protocol.iec104.server.pipeline.PipelineManager;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

@Slf4j
public class TaDataStateHandler extends AbstractDataStateHandler {
    private final TaDataReactor dataReactor;
    private final Map<Integer, TaToken> tokenMap = new HashMap<>();

    public TaDataStateHandler(PipelineManager manager, SlaveDataConfig dataConfig) {
        super(manager);
        this.dataReactor = dataConfig.getTaDataReactor();
    }

    @Override
    protected void resetState() {
        taTokenPool().returnBack(tokenMap.values());
        tokenMap.clear();
    }

    @Override
    protected void afterResetState() {

    }

    private TaTokenPool taTokenPool() {
        return inSlaveChannel().getTaTokenPool();
    }

    @Override
    protected IAsdu updateStateByRecv(IAsdu iAsdu) throws Exception {
        Ta.Type taType = dataReactor.getTaType();
        TypeTag taTypeTag = taType.getTypeTag();

        TypeTag typeTag = iAsdu.getTypeTag();
        Cot cot = iAsdu.getCot();
        Cause cause = cot.getCause();

        if (typeTag == TypeTag.C_SE_NC_1) { // 遥调短浮点数
            if (cause != Cause.COT_ACT) return iAsdu;

            // 激活
            InfoObj infoObj = iAsdu.getInfoObjs().get(0);
            C_SE_NC_1_InfoElem infoElem = infoObj.getInfoElem().castTo(C_SE_NC_1_InfoElem.class);
            C_SE_NC_1_Qua qua = infoObj.getQua().castTo(C_SE_NC_1_Qua.class);
            int infoObjAddr = infoObj.getAddr(); // 信息体地址
            float floatVal = infoElem.getFloatVal(); // 开关状态
            TaQuaType quaType = qua.getType(); // 遥调类型

            // 不支持支持处理该遥控类型返回遥控否定
            if (taTypeTag != typeTag) {
                // 发送否定确认
                iAsduSender().chSendTaFloatAck(infoObjAddr, floatVal, quaType, Pn.PN_NO);
                return null; // 丢弃该报文
            }

            if (quaType == TaQuaType.SELECT) { // 选择
                // 是否接受遥调选择
                dataReactor.acceptSelect(inSlaveChannel(), infoObjAddr).whenComplete(new BiConsumer<Boolean, Throwable>() {
                    @Override
                    public void accept(Boolean isAccepted, Throwable throwable) {
                        if (throwable != null) {
                            throwable.printStackTrace();
                            return;
                        }
                        safeExecute(new Runnable() {
                            @Override
                            public void run() {
                                // 检查是否已存在
                                TaToken token = tokenMap.get(infoObjAddr);
                                if (token != null) {
                                    // 已经存在该信息体地址的遥调过程，该过程丢弃
                                    log.warn("[Ta] 遥调点{}的遥调过程已存在，放弃本次遥调选择请求", infoObjAddr);
                                    return;
                                }
                                if (isAccepted) { // 接受选择
                                    // 申请token
                                    token = taTokenPool().applyFor(infoObjAddr, inSlaveChannel());
                                    if (token == null) {
                                        // 已经存在该信息体地址的别的遥调过程，返回否定确认
                                        iAsduSender().chSendTaFloatAck(infoObjAddr, floatVal, quaType, Pn.PN_NO);
                                        return;
                                    }

                                    // 申请成功
                                    tokenMap.put(infoObjAddr, token);
                                    token.setRSelectFloat(floatVal); // 设置为接收到遥调选择
                                    // 返回肯定确认
                                    iAsduSender().chSendTaFloatAck(infoObjAddr, floatVal, quaType, Pn.PN_YES);
                                } else { // 不接受选择
                                    // 否定确认
                                    iAsduSender().chSendTaFloatAck(infoObjAddr, floatVal, quaType, Pn.PN_NO);
                                }
                            }
                        });
                    }
                });
            } else { // 执行
                // 是否接受遥调执行
                dataReactor.acceptExecute(inSlaveChannel(), infoObjAddr).whenComplete(new BiConsumer<Boolean, Throwable>() {
                    @Override
                    public void accept(Boolean isAccepted, Throwable throwable) {
                        if (throwable != null) {
                            throwable.printStackTrace();
                            return;
                        }
                        safeExecute(new Runnable() {
                            @Override
                            public void run() {
                                // 检查是否存在
                                TaToken token = tokenMap.get(infoObjAddr);
                                if (token == null) {
                                    // 不存在该信息体地址的遥调过程，该过程丢弃
                                    log.warn("[Ta] 遥调点{}的遥调过程不存在，放弃本次遥调执行请求", infoObjAddr);
                                    return;
                                }
                                if (isAccepted) { // 接受执行
                                    token.setRExecuteFloat(floatVal); // 设置为接收到遥调执行
                                    // 返回肯定确认
                                    iAsduSender().chSendTaFloatAck(infoObjAddr, floatVal, quaType, Pn.PN_YES).addListener(new ChannelFutureListener() {
                                        @Override
                                        public void operationComplete(ChannelFuture future) throws Exception {
                                            if (!future.isSuccess()) {
                                                future.cause().printStackTrace();
                                                return;
                                            }
                                            // 肯定确认之后，发送激活终止
                                            iAsduSender().chSendTaFloatFinished(infoObjAddr, floatVal);
                                        }
                                    });
                                } else { // 不接受执行
                                    // 否定确认
                                    iAsduSender().chSendTaFloatAck(infoObjAddr, floatVal, quaType, Pn.PN_NO);
                                }
                            }
                        });
                    }
                });
            }
        } else if (typeTag == TypeTag.C_SE_NB_1) { // 标度化值
            // TODO 实现同上
        }

        return iAsdu;
    }

    @Override
    protected IAsdu updateStateBySend(IAsdu iAsdu) throws Exception {
        Ta.Type tcType = dataReactor.getTaType();
        TypeTag taTypeTag = tcType.getTypeTag();

        TypeTag typeTag = iAsdu.getTypeTag();
        if (taTypeTag != typeTag) return iAsdu; // 不用处理不支持的
        Cot cot = iAsdu.getCot();
        Pn pn = cot.getPn();
        Cause cause = cot.getCause();

        if (typeTag == TypeTag.C_SE_NC_1) { // 短浮点数
            InfoObj infoObj = iAsdu.getInfoObjs().get(0);
            C_SE_NC_1_InfoElem infoElem = infoObj.getInfoElem().castTo(C_SE_NC_1_InfoElem.class);
            C_SE_NC_1_Qua qua = infoObj.getQua().castTo(C_SE_NC_1_Qua.class);
            int infoObjAddr = infoObj.getAddr(); // 信息体地址
            float floatVal = infoElem.getFloatVal(); // 值
            TaQuaType quaType = qua.getType(); // 命令类型

            if (cause == Cause.COT_ACTCON) { // 激活确认
                if (quaType == TaQuaType.SELECT) { // 选择
                    if (pn == Pn.PN_NO) return iAsdu; // 选择否定确认没有token
                    TaToken token = tokenMap.get(infoObjAddr);
                    if (token == null) {
                        log.warn("[Ta] 发送了无效的遥调短浮点数选择肯定确认");
                        return iAsdu;
                    }
                    token.setSSelectFloatAck(pn == Pn.PN_YES); // 设置为发送了遥调短浮点数确认
                    log.info("[Ta] 发送遥调短浮点数选择肯定确认（{}）", infoObjAddr);
                    return iAsdu;
                } else { // 执行
                    TaToken token = tokenMap.get(infoObjAddr);
                    if (token == null) {
                        log.warn("[Ta] 发送了无效的遥调短浮点数执行确认");
                        return iAsdu;
                    }
                    token.setSExecuteFloatAck(pn == Pn.PN_YES);
                    log.info("[Ta] 发送遥调短浮点数执行{}确认（{}）", pn == Pn.PN_YES ? "肯定" : "否定", infoObjAddr);
                    return iAsdu;
                }
            } else if (cause == Cause.COT_ACTTERM) { // 激活终止
                TaToken token = tokenMap.get(infoObjAddr);
                if (token == null) {
                    log.warn("[Ta] 发送了无效的遥调短浮点数激活终止");
                    return iAsdu;
                }
                token.setSFloatFinished();
                // 移除这个token
                tokenMap.remove(token.getAddress());
                taTokenPool().returnBack(token);
                log.info("[Ta] 发送遥调短浮点数激活终止（{}）", infoObjAddr);
                return iAsdu;
            }
        } else if (typeTag == TypeTag.C_SE_NA_1) { // 归一化值
            // TODO 实现同上
        }

        return iAsdu;
    }

    @Nullable
    @Override
    protected ScheduledTask getScheduledTask() {
        return null;
    }
}
