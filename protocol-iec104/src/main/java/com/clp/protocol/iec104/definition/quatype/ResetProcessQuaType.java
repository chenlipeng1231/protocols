package com.clp.protocol.iec104.definition.quatype;

import com.clp.protocol.core.utils.EnumElemDoesNotExistException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 复位进程限定词类型
 */
@Getter
@AllArgsConstructor
public enum ResetProcessQuaType {
    RESET_PROCESS((byte)0x01, "复位进程限定词"); // 复位进程限定词

    private final byte val;
    private final String desc;

    public static ResetProcessQuaType gain(byte val) {
        for (ResetProcessQuaType quaType : ResetProcessQuaType.values()) {
            if (quaType.val == val) {
                return quaType;
            }
        }
        throw new EnumElemDoesNotExistException(ResetProcessQuaType.class);
    }
}
