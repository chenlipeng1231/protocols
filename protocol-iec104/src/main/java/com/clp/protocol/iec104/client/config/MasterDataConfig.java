package com.clp.protocol.iec104.client.config;

import com.clp.protocol.core.common.Checkable;
import lombok.Getter;

@Getter
public class MasterDataConfig implements Checkable {
    /**
     * 默认总召唤周期（秒）
     */
    public static final int DEFAULT_TOTAL_CALL_100_PERIOD_SECONDS = 10 * 60;
    /**
     * 默认电度量召唤周期（秒）
     */
    public static final int DEFAULT_TOTAL_CALL_101_PERIOD_SECONDS = 10 * 60;

    private final int totalCall100PeriodSeconds;
    private final int totalCall101PeriodSeconds;

    public static Configurer configurer() {
        return new Configurer();
    }

    private MasterDataConfig(Configurer configurer) {
        this.totalCall100PeriodSeconds = configurer.totalCall100PeriodSeconds;
        this.totalCall101PeriodSeconds = configurer.totalCall101PeriodSeconds;
    }

    @Override
    public void check() throws Throwable {

    }

    public static class Configurer {
        private int totalCall100PeriodSeconds = DEFAULT_TOTAL_CALL_100_PERIOD_SECONDS;
        private int totalCall101PeriodSeconds = DEFAULT_TOTAL_CALL_101_PERIOD_SECONDS;

        public Configurer totalCall100PeriodSeconds(int totalCallPeriodSeconds) {
            this.totalCall100PeriodSeconds = totalCallPeriodSeconds;
            return this;
        }

        public Configurer totalCall101PeriodSeconds(int totalCall101PeriodSeconds) {
            this.totalCall101PeriodSeconds = totalCall101PeriodSeconds;
            return this;
        }

        public MasterDataConfig configOk() {
            return new MasterDataConfig(this);
        }
    }
}
