package com.clp.protocol.iec104.apdu.asdu.infoobj.infoelem;

import com.clp.protocol.iec104.definition.Tm;
import com.clp.protocol.iec104.definition.TypeTag;
import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePduClip;
import com.clp.protocol.core.pdu.ByteToStringFormat;
import com.clp.protocol.core.utils.ByteUtil;
import io.netty.buffer.ByteBuf;
import lombok.Getter;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * 设定值命令，归一化值
 */
@Getter
public class C_SE_NA_1_InfoElem extends TaInfoElem {
    /**
     * 归一化值用整型表示
     */
    private int intVal;

    public C_SE_NA_1_InfoElem() {
        this(-1);
    }

    public C_SE_NA_1_InfoElem(int intVal) {
        super(TypeTag.C_SE_NA_1);
        this.intVal = intVal;
    }

    @Override
    public InfoElem refreshFrom(ByteBuf buf) {
        byte[] bytes = new byte[typeTag().getInfoElemBytesLen()];
        buf.readBytes(bytes);
        this.intVal = ByteUtil.bytes2ToIntLE(bytes);
        return this;
    }

    @Override
    public boolean isValid() {
        return intVal >= 0 && intVal <= 0xFFFF;
    }

    @Override
    public void writeBytesTo(ByteBuf buf) {
        buf.writeShortLE(intVal);
    }

    @Override
    public void writeFormattedByteStringsTo(StringBuilder sb, String frameClipBytesSeparator, String byteSeparator, ByteToStringFormat byteFormat) {
        byte[] bytes = ByteUtil.intToBytes2LE(intVal);
        sb.append(byteFormat.format(bytes[0]));
        for (int i = 1; i < bytes.length; i++) {
            sb.append(byteSeparator).append(byteFormat.format(bytes[i]));
        }
    }

    @Override
    public void writeSimpleDescriptionTo(StringBuilder sb) {
        sb.append(intVal);
    }

    @Override
    public void writeDetailDescriptionTo(StringBuilder sb) {
        sb.append("设定值，归一化值（整型表示）：").append(intVal);
    }

    @Override
    public Tm.Type getTmType() {
        return Tm.Type.NORMALIZE;
    }

    @Override
    public Number getNumberVal() {
        return intVal;
    }

    @Override
    protected void forEachOneLevelChild(Consumer<BaseNBytePduClip<?>> consumer) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        C_SE_NA_1_InfoElem that = (C_SE_NA_1_InfoElem) o;
        return intVal == that.intVal;
    }

    @Override
    public int hashCode() {
        return Objects.hash(intVal);
    }
}
