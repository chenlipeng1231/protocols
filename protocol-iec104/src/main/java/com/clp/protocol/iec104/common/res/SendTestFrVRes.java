package com.clp.protocol.iec104.common.res;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class SendTestFrVRes extends BaseRes<SendTestFrVRes> {
    private volatile boolean isRecvAckSuccess; // 是否成功接收确认
}
