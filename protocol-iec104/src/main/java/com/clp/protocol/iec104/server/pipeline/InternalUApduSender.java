package com.clp.protocol.iec104.server.pipeline;

import com.clp.protocol.iec104.apdu.ApduFactory;
import com.clp.protocol.iec104.apdu.UApdu;
import com.clp.protocol.iec104.server.InSlaveChannel;

public class InternalUApduSender {
    private final PipelineManager manager;

    public InternalUApduSender(PipelineManager manager) {
        this.manager = manager;
    }

    private void chSend(UApdu uApdu) {
        InSlaveChannel inSlaveChannel = manager.getInSlaveChannel();
        inSlaveChannel.executor().execute(() -> inSlaveChannel.channel().writeAndFlush(uApdu));
    }

    /**
     * 发送启动传输确认
     */
    public void chSendStartDtC() {
        chSend(ApduFactory.getUApduOfStartDtC());
    }

    public void chSendStopDtC() {
        chSend(ApduFactory.getUApduOfStopDtC());
    }

    public void chSendTestFrC() {
        chSend(ApduFactory.getUApduOfTestFrC());
    }

    public void chSendTestFrV() {
        chSend(ApduFactory.getUApduOfTestFrV());
    }
}
