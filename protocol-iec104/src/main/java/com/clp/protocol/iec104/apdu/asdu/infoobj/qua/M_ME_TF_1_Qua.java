package com.clp.protocol.iec104.apdu.asdu.infoobj.qua;

import com.clp.protocol.iec104.definition.Tm;
import com.clp.protocol.iec104.definition.TypeTag;

public class M_ME_TF_1_Qua extends TmQua {

    public M_ME_TF_1_Qua() {
        this(null);
    }

    public M_ME_TF_1_Qua(Tm.Valid valid) {
        super(TypeTag.M_ME_TF_1, valid);
    }
}
