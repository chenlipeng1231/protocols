package com.clp.protocol.iec104.server;

import com.clp.protocol.core.common.Checkable;
import io.netty.handler.ipfilter.IpFilterRule;
import io.netty.handler.ipfilter.IpFilterRuleType;
import lombok.Getter;

import java.net.InetSocketAddress;

/**
 * 子站控制配置
 */
@Getter
public class SlaveControlConfig implements Checkable {
    /**
     * 默认T0时间，单位为秒
     */
    public static final int DEFAULT_T0 = 6; // 30
    /**
     * 默认T1时间，单位为秒
     */
    public static final int DEFAULT_T1 = 3; // 15
    /**
     * 默认T2时间，单位为秒
     */
    public static final int DEFAULT_T2 = 2; // 10
    /**
     * 默认T3时间，单位为秒
     */
    public static final int DEFAULT_T3 = 4; // 20
    /**
     * 默认k值
     */
    public static final int DEFAULT_K = 12;
    /**
     * 默认w值
     */
    public static final int DEFAULT_W = 8;
    /**
     * 默认的IP过滤规则：通过全部
     */
    public static final IpFilterRule DEFAULT_IP_FILTER_RULE = new IpFilterRule() {
        @Override
        public boolean matches(InetSocketAddress remoteAddress) {
            return true;
        }

        @Override
        public IpFilterRuleType ruleType() {
            return IpFilterRuleType.ACCEPT;
        }
    };

    private final int k;
    private final int w;

    private final int t0;
    private final int t1;
    private final int t2;
    private final int t3;

    // IP过滤规则
    private final IpFilterRule ipFilterRule;

    private SlaveControlConfig(Configurer cfgr) {
        this.k = cfgr.k;
        this.w = cfgr.w;
        this.t0 = cfgr.t0;
        this.t1 = cfgr.t1;
        this.t2 = cfgr.t2;
        this.t3 = cfgr.t3;
        this.ipFilterRule = cfgr.ipFilterRule;
    }

    @Override
    public void check() throws Throwable {
        Checker.checkT0T1T2T3(t0, t1, t2, t3);
        Checker.checkKw(k, w);
    }

    public static Configurer configurer() {
        return new Configurer();
    }

    public static class Configurer {
        private int k = DEFAULT_K;
        private int w = DEFAULT_W;
        private int t0 = DEFAULT_T0;
        private int t1 = DEFAULT_T1;
        private int t2 = DEFAULT_T2;
        private int t3 = DEFAULT_T3;
        private IpFilterRule ipFilterRule = DEFAULT_IP_FILTER_RULE;

        public Configurer t0t1t2t3(int t0, int t1, int t2, int t3) {
            this.t0 = t0;
            this.t1 = t1;
            this.t2 = t2;
            this.t3 = t3;
            return this;
        }

        public Configurer kw(int k, int w) {
            this.k = k;
            this.w = w;
            return this;
        }

        public Configurer ipFilterRule(IpFilterRule ipFilterRule) {
            this.ipFilterRule = ipFilterRule;
            return this;
        }

        public SlaveControlConfig configOk() {
            return new SlaveControlConfig(this);
        }
    }

    public interface Checker {

        static void checkKw(int k, int w) {
            if (!(k * 2 - w * 3 >= 0)) throw new IllegalArgumentException("k=" + k + ", w=" + w + "，w的值不能超过k的三分之二");
        }

        static void checkT0T1T2T3(int t0, int t1, int t2, int t3) {
            if (!(t2 < t1 && t1 < t3)) throw new IllegalArgumentException("t1=" + t1 + ", t2=" + t2 + ", t3=" + t3 + ", t1, t2, t3 的值必须满足：t2 < t1 < t3");
        }

    }
}
