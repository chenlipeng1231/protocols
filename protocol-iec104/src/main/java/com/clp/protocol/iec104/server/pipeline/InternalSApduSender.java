package com.clp.protocol.iec104.server.pipeline;

import com.clp.protocol.iec104.apdu.ApduFactory;
import com.clp.protocol.iec104.server.InSlaveChannel;

public class InternalSApduSender {
    private final PipelineManager manager;

    public InternalSApduSender(PipelineManager manager) {
        this.manager = manager;
    }

    /**
     * @param recvSeq
     */
    public void chSend(int recvSeq) {
        InSlaveChannel inSlaveChannel = manager.getInSlaveChannel();
        inSlaveChannel.executor().execute(() -> inSlaveChannel.channel().writeAndFlush(ApduFactory.getSApdu(recvSeq)));
    }

}
