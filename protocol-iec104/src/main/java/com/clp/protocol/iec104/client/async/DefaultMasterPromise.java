package com.clp.protocol.iec104.client.async;

import com.clp.protocol.iec104.client.Master;

/**
 * 默认的主站promise
 * @param <V>
 */
public class DefaultMasterPromise<V> extends GenericMasterPromise<DefaultMasterPromise<V>, V>{
    public DefaultMasterPromise(Master master, V res) {
        super(master);
        this.res = res;
    }

    public DefaultMasterPromise(Master master) {
        this(master, null);
    }
}
