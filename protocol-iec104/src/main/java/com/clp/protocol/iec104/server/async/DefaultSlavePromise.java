package com.clp.protocol.iec104.server.async;

import com.clp.protocol.iec104.server.Slave;

/**
 * 默认的子站promise
 * @param <V>
 */
public class DefaultSlavePromise<V> extends GenericSlavePromise<DefaultSlavePromise<V>, V> {
    public DefaultSlavePromise(Slave slave, V res) {
        super(slave);
        this.res = res;
    }

    public DefaultSlavePromise(Slave slave) {
        this(slave, null);
    }
}
