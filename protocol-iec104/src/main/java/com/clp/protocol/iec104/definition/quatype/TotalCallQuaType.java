package com.clp.protocol.iec104.definition.quatype;

import com.clp.protocol.core.utils.EnumElemDoesNotExistException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 总召唤100限定词类型
 */
@Getter
@AllArgsConstructor
public enum TotalCallQuaType {
    TOTAL_CALL((byte) 0x20, "总召唤限定词"),
    GROUP_TOTAL_CALL((byte) 0x14, "总召唤限定词，支持老版的分组");

    private final byte val;
    private final String desc;

    public static TotalCallQuaType gain(byte val) {
        for (TotalCallQuaType quaType : TotalCallQuaType.values()) {
            if (quaType.val == val) {
                return quaType;
            }
        }
        throw new EnumElemDoesNotExistException(TotalCallQuaType.class);
    }
}
