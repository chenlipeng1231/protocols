package com.clp.protocol.iec104.client.async.sendapdu;

import com.clp.protocol.iec104.common.res.BaseRes;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 发送总召唤100 的结果
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class SendTotalCall100Res extends BaseRes<SendTotalCall100Res> {
    private volatile boolean isRecvTotalCall100Ack; // 是否接收到总召唤100确认
    private volatile boolean isRecvTotalCall100End; // 是否接收到总召唤100终止
}
