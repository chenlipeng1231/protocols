package com.clp.protocol.iec104.apdu.asdu.infoobj.infoelem.filetransfer;

import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePduClip;
import com.clp.protocol.core.pdu.ByteToStringFormat;

import java.util.function.Consumer;

/**
 * 附加数据包定义
 */
public abstract class AttachPack extends BaseNBytePduClip<AttachPack> implements OperationTagRequired {

    /**
     * 转化为子类对象（需根据操作标识）
     *
     * @param clazz
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public  <T extends AttachPack> T castTo(Class<T> clazz) {
        return (T) this;
    }

    @Override
    public void writeFormattedByteStringsTo(StringBuilder sb, String frameClipBytesSeparator, String byteSeparator, ByteToStringFormat byteFormat) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void writeSimpleDescriptionTo(StringBuilder sb) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void writeDetailDescriptionTo(StringBuilder sb) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void forEachOneLevelChild(Consumer<BaseNBytePduClip<?>> consumer) {
        throw new UnsupportedOperationException();
    }
}
