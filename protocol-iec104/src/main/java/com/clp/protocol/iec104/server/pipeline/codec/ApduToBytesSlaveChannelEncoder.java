package com.clp.protocol.iec104.server.pipeline.codec;

import com.clp.protocol.iec104.common.pipeline.AbstractApduToBytesEncoder;
import com.clp.protocol.iec104.server.pipeline.PipelineManager;

public class ApduToBytesSlaveChannelEncoder extends AbstractApduToBytesEncoder {

    private final PipelineManager manager;

    public ApduToBytesSlaveChannelEncoder(PipelineManager manager) {
        this.manager = manager;
    }

}
