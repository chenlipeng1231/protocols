package com.clp.protocol.iec104.client.pipeline.state;

import com.clp.protocol.iec104.client.async.MasterPromise;

public interface MasterPromiseRegister {

    /**
     * 注册一个promise，如果注册失败，则返回false；
     * @param promise
     * @param <V>
     */
    <V> void register(MasterPromise<V> promise);

}
