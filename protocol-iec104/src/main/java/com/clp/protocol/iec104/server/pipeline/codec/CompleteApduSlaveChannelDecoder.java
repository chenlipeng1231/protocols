package com.clp.protocol.iec104.server.pipeline.codec;

import com.clp.protocol.iec104.common.pipeline.AbstractCompleteApduDecoder;
import com.clp.protocol.iec104.server.pipeline.PipelineManager;

public class CompleteApduSlaveChannelDecoder extends AbstractCompleteApduDecoder {
    private final PipelineManager manager;

    public CompleteApduSlaveChannelDecoder(PipelineManager manager) {
        this.manager = manager;
    }
}
