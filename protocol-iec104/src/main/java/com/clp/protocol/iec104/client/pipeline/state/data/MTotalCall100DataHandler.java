package com.clp.protocol.iec104.client.pipeline.state.data;

import com.clp.protocol.iec104.apdu.asdu.IAsdu;
import com.clp.protocol.iec104.client.async.MasterPromise;
import com.clp.protocol.iec104.client.async.sendapdu.SendTotalCall100Res;
import com.clp.protocol.iec104.client.config.MasterDataConfig;
import com.clp.protocol.iec104.client.pipeline.MPipelineManager;
import com.clp.protocol.iec104.client.pipeline.state.MasterPromiseRegister;
import com.clp.protocol.core.pdu.nbytepdu.FailedToSendFrameException;
import com.clp.protocol.core.utils.AssertUtil;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nullable;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

@Slf4j
public class MTotalCall100DataHandler extends AbstractMDataStateHandler implements MasterPromiseRegister {

    private enum State {
        TOTAL_CALL_100,
        TOTAL_CALL_100_ACK,
        TOTAL_CALL_100_END;
    }

    @Getter
    private final int periodSeconds;

    private volatile State state;
    private volatile long lastTotalCall100Time;
    private volatile long lastTotalCall100AckTime;
    private volatile long lastTotalCall100EndTime;

    private final Vector<MasterPromise<SendTotalCall100Res>> promises = new Vector<>();

    public MTotalCall100DataHandler(MPipelineManager manager, MasterDataConfig cfg) {
        super(manager);
        this.periodSeconds = cfg.getTotalCall100PeriodSeconds();
    }

    @Override
    protected void resetState() {
        this.state = State.TOTAL_CALL_100_END;
        this.lastTotalCall100Time = System.currentTimeMillis();
        this.lastTotalCall100AckTime = System.currentTimeMillis();
        this.lastTotalCall100EndTime = System.currentTimeMillis();
        this.promises.clear();
    }

    @Override
    protected void afterResetState() {

    }

    @Override
    protected IAsdu updateStateBySend(IAsdu iAsdu) throws Exception {
        switch (iAsdu.getTypeTag()) {
            case C_IC_NA_1: // 总召唤100
                switch (iAsdu.getCot().getCause()) {
                    case COT_ACT: // 激活
                        if (state == State.TOTAL_CALL_100_END) {
                            state = State.TOTAL_CALL_100;
                            promises.forEach(promise -> promise.getRes().setSendSuccess(true));
                            lastTotalCall100Time = System.currentTimeMillis();
                        } else {
                            promises.forEach(promise -> {
                                promise.getRes().setSendSuccess(false).setRecvTotalCall100Ack(false).setRecvTotalCall100End(false);
                                promise.setFailure(new FailedToSendFrameException(iAsdu));
                            });
                            promises.clear();
                            log.warn("已经在进行总召唤，放弃本次总召唤命令！");
                            return null;
                        }
                        break;
                }
                break;
        }

        return iAsdu;
    }

    @Override
    protected IAsdu updateStateByRecv(IAsdu iAsdu) throws Exception {
        switch (iAsdu.getTypeTag()) {
            case C_IC_NA_1: // 总召唤100
                switch (iAsdu.getCot().getCause()) {
                    case COT_ACTCON: // 确认激活
                        if (state == State.TOTAL_CALL_100) {
                            promises.forEach(promise -> promise.getRes().setRecvTotalCall100Ack(true));
                            state = State.TOTAL_CALL_100_ACK;
                            lastTotalCall100AckTime = System.currentTimeMillis();
                        } else {
                            log.warn("接收到无效的总召唤确认报文！");
                        }
                        break;
                    case COT_ACTTERM: // 激活终止
                        if (state == State.TOTAL_CALL_100_ACK) {
                            promises.forEach(promise -> {
                                promise.getRes().setRecvTotalCall100End(true);
                                promise.setSuccess();
                            });
                            promises.clear();
                            state = State.TOTAL_CALL_100_END;
                            lastTotalCall100EndTime = System.currentTimeMillis();
                        } else {
                            log.warn("接收到无效的总召唤确认终止报文！");
                        }
                        break;
                }
                break;
        }

        return iAsdu;
    }

    @Override
    @Nullable
    protected ScheduledTask getScheduledTask() {
        return new ScheduledTask(0, 1, TimeUnit.SECONDS) {
            @Override
            public void run() {
                if (!inMaster().controlInfo().isInitCompleted()) {
                    return;
                }

                long nowTime = System.currentTimeMillis();
                switch (state) {
                    case TOTAL_CALL_100:
                        if (nowTime - lastTotalCall100Time > 1000 * 3) {
                            promises.forEach(promise -> {
                                promise.getRes().setRecvTotalCall100Ack(false).setRecvTotalCall100End(false);
                                promise.setFailure(new RuntimeException("发送总召唤100的3秒内没有收到确认报文，放弃本次总召唤！"));
                            });
                            promises.clear();
                            state = State.TOTAL_CALL_100_END;
                            lastTotalCall100EndTime = System.currentTimeMillis();
                            log.warn("发送总召唤100的3秒内没有收到确认报文，放弃本次总召唤！");
                        }
                        break;
                    case TOTAL_CALL_100_ACK:
                        if (nowTime - lastTotalCall100AckTime > 1000 * 3) {
                            promises.forEach(promise -> {
                                promise.getRes().setRecvTotalCall100End(false);
                                promise.setSuccess(); // TODO 默认为成功了
                            });
                            promises.clear();
                            state = State.TOTAL_CALL_100_END;
                            lastTotalCall100EndTime = System.currentTimeMillis();
                            log.warn("接收到总召唤100确认的3秒内没有收到召唤终止报文，默认本次召唤结束！");
                        }
                        break;
                    case TOTAL_CALL_100_END:
                        if (nowTime - lastTotalCall100EndTime > 1000L * periodSeconds) {
                            inMaster().getIAsduSender().sendTotalCall100();
                            log.info("周期性发送总召唤100");
                        }
                        break;
                }
            }
        };
    }

    @Override
    @SuppressWarnings("unchecked")
    public <V> void register(MasterPromise<V> sendPromise) {
        V res = sendPromise.getRes();
        AssertUtil.notNull(res);

        if (sendPromise.getRes() instanceof SendTotalCall100Res) {
            promises.add(((MasterPromise<SendTotalCall100Res>) sendPromise));
            return;
        }

        throw new IllegalArgumentException();
    }

    public boolean isDoingTotalCall100() {
        return state != State.TOTAL_CALL_100_END;
    }
}
