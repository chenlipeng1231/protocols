package com.clp.protocol.iec104.apdu.asdu.infoobj.qua;

import com.clp.protocol.iec104.definition.Tm;
import com.clp.protocol.iec104.definition.TypeTag;
import lombok.NoArgsConstructor;

/**
 * 测量值，标度化值
 */
public class M_ME_NB_1_Qua extends TmQua {

    public M_ME_NB_1_Qua() {
        this(null);
    }

    public M_ME_NB_1_Qua(Tm.Valid valid) {
        super(TypeTag.M_ME_NB_1, valid);
    }
}
