package com.clp.protocol.iec104.client.pipeline.state.data;

import com.clp.protocol.iec104.apdu.asdu.IAsdu;
import com.clp.protocol.iec104.client.pipeline.MPipelineManager;

import javax.annotation.Nullable;

public class MIAsduCallbackHandler extends AbstractMDataStateHandler {
    public MIAsduCallbackHandler(MPipelineManager manager) {
        super(manager);
    }

    @Override
    protected void resetState() {

    }

    @Override
    protected void afterResetState() {

    }

    @Override
    protected IAsdu updateStateByRecv(IAsdu iAsdu) throws Exception {
        inMaster().getIAsduRecver().handleRecvCallbacks(iAsdu);
        return iAsdu;
    }

    @Override
    protected IAsdu updateStateBySend(IAsdu iAsdu) throws Exception {
        return iAsdu;
    }

    @Nullable
    @Override
    protected ScheduledTask getScheduledTask() {
        return null;
    }
}
