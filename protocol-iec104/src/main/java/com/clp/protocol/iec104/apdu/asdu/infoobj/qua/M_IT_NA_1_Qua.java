package com.clp.protocol.iec104.apdu.asdu.infoobj.qua;

import com.clp.protocol.iec104.definition.Tp;
import com.clp.protocol.iec104.definition.TypeTag;

/**
 * 累积量（遥脉）
 */
public class M_IT_NA_1_Qua extends TpQua {

    public M_IT_NA_1_Qua() {
        this(null, null, null, -1);
    }

    public M_IT_NA_1_Qua(boolean isValid, boolean isAdjusted, boolean isOverflowed, int seq) {
        super(isValid, isAdjusted, isOverflowed, seq);
    }

    public M_IT_NA_1_Qua(Tp.Iv iv, Tp.Ca ca, Tp.Cy cy, int seq) {
        super(TypeTag.M_IT_NA_1, iv, ca, cy, seq);
    }
}
