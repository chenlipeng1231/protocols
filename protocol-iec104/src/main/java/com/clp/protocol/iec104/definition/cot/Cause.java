package com.clp.protocol.iec104.definition.cot;

import com.clp.protocol.core.utils.EnumElemDoesNotExistException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 传输原因值的定义
 */
@Getter
@AllArgsConstructor
public enum Cause {
    /**
     * 未使用
     */
    NOT_USED(0, "未使用"),
    /**
     * 周期、循环
     */
    COT_PER_CYC(1, "周期、循环"),
    /**
     * 背景扫描
     */
    COT_BACK(2, "背景扫描"),
    /**
     * 自发、突发
     */
    COT_SPONT(3, "自发、突发"),
    /**
     * 初始化
     */
    COT_INIT(4, "初始化"),
    /**
     * 请求或者被请求
     */
    COT_REQ(5, "请求或者被请求"),
    /**
     * 激活
     */
    COT_ACT(6, "激活"),
    /**
     * 确认激活
     */
    COT_ACTCON(7, "确认激活"),
    /**
     * 停止激活
     */
    COT_DEACT(8, "停止激活"),
    /**
     * 停止激活确认
     */
    COT_DEACTCON(9, "停止激活确认"),
    /**
     * 激活终止
     */
    COT_ACTTERM(10, "激活终止"),
    /**
     * 远方命令引起的返送信息
     */
    COT_RETREM(11, "远方命令引起的返送信息"),
    /**
     * 当地命令引起的返送信息
     */
    COT_RETLOG(12, "当地命令引起的返送信息"),
    /**
     * 文件传输
     */
    COT_FILE(13, "文件传输"),
    /**
     * 响应站召唤
     */
    COT_INRO(20, "响应站召唤"),
    /**
     * 响应第1组召唤
     */
    COT_INRO1(21, "响应第1组召唤"),
    /**
     * 响应第2组召唤
     */
    COT_INRO2(22, "响应第2组召唤"),
    /**
     * 响应第3组召唤
     */
    COT_INRO3(23, "响应第3组召唤"),
    /**
     * 响应第4组召唤
     */
    COT_INRO4(24, "响应第4组召唤"),
    /**
     * 响应第5组召唤
     */
    COT_INRO5(25, "响应第5组召唤"),
    /**
     * 响应第6组召唤
     */
    COT_INRO6(26, "响应第6组召唤"),
    /**
     * 响应第7组召唤
     */
    COT_INRO7(27, "响应第7组召唤"),
    /**
     * 响应第8组召唤
     */
    COT_INRO8(28, "响应第8组召唤"),
    /**
     * 响应第9组召唤
     */
    COT_INRO9(29, "响应第9组召唤"),
    /**
     * 响应第10组召唤
     */
    COT_INRO10(30, "响应第10组召唤"),
    /**
     * 响应第11组召唤
     */
    COT_INRO11(31, "响应第11组召唤"),
    /**
     * 响应第12组召唤
     */
    COT_INRO12(32, "响应第12组召唤"),
    /**
     * 响应第13组召唤
     */
    COT_INRO13(33, "响应第13组召唤"),
    /**
     * 响应第14组召唤
     */
    COT_INRO14(34, "响应第14组召唤"),
    /**
     * 响应第15组召唤
     */
    COT_INRO15(35, "响应第15组召唤"),
    /**
     * 响应第16组召唤
     */
    COT_INRO16(36, "响应第16组召唤"),
    /**
     * 响应计数量召唤
     */
    COT_REQCOGEN(37, "响应计数量召唤"),
    /**
     * 响应第1组计数量召唤
     */
    COT_REQCO1(38, "响应第1组计数量召唤"),
    /**
     * 响应第2组计数量召唤
     */
    COT_REQCO2(39, "响应第2组计数量召唤"),
    /**
     * 响应第3组计数量召唤
     */
    COT_REQCO3(40, "响应第3组计数量召唤"),
    /**
     * 响应第4组计数量召唤
     */
    COT_REQCO4(41, "响应第4组计数量召唤"),
    /**
     * 未知的类型标识
     */
    COT_UNKNOW_TYPE(44, "未知的类型标识"),
    /**
     * 未知的传送原因
     */
    COT_UNKNOW_COT(45, "未知的传送原因"),
    /**
     * 未知的应用服务数据单元公共地址
     */
    COT_UNKNOW_ADDR(46, "未知的应用服务数据单元公共地址"),
    /**
     * 未知的信息体对象地址
     */
    COT_UNKNOW_INF(47, "未知的信息体对象地址");

    private final int val;
    private final String desc;

    /**
     * 根据值返回对应的类型
     *
     * @param val
     * @return
     */
    public static Cause gain(int val) {
        for (Cause cause : Cause.values()) {
            if (cause.getVal() == val) {
                return cause;
            }
        }
        throw new EnumElemDoesNotExistException(Cause.class);
    }
}
