package com.clp.protocol.iec104.apdu;

import com.clp.protocol.iec104.definition.ApduType;
import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePdu;

public abstract class Apdu extends BaseNBytePdu<Apdu> {

    /**
     * 该Apdu的类型
     * @return
     */
    public abstract ApduType apduType();

    /**
     * 是否为U帧
     * @return
     */
    public boolean isUType() {
        return apduType() == ApduType.UType;
    }

    /**
     * 是否为S帧
     * @return
     */
    public boolean isSType() {
        return apduType() == ApduType.SType;
    }

    /**
     * 是否为I帧
     * @return
     */
    public boolean isIType() {
        return apduType() == ApduType.IType;
    }

    public UApdu castToUType() {
        if (!this.isUType()) throw new RuntimeException("类型转换失败！");
        return (UApdu) this;
    }

    public SApdu castToSType() {
        if (!this.isSType()) throw new RuntimeException("类型转换失败！");
        return (SApdu) this;
    }

    public IApdu castToIType() {
        if (!this.isIType()) throw new RuntimeException("类型转换失败！");
        return (IApdu) this;
    }
}
