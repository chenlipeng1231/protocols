package com.clp.protocol.iec104.apdu.asdu.infoobj.infoelem;

import com.clp.protocol.iec104.definition.Tc;

/**
 * 有遥控信息
 */
public interface HasTcInfo {

    Tc.Type type();

    /**
     * 命令类型：选择还是执行
     * @return
     */
    Tc.CmdType getCmdType();

    /**
     * 是否为 on 命令
     * @return
     */
    boolean isSwitchOn();
}
