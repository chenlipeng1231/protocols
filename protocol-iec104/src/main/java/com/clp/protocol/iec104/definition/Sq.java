package com.clp.protocol.iec104.definition;

import com.clp.protocol.core.utils.EnumElemDoesNotExistException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 可变结构限定词的连续性
 */
@Getter
@AllArgsConstructor
public enum Sq {
    /**
     * 1为地址连续
     */
    CONTINUE(1, "地址连续"),
    /**
     * 0为地址不连续
     */
    NOT_CONTINUE(0, "地址不连续");

    private final int val;
    private final String desc;

    public static Sq gain(int value) {
        for (Sq sq : Sq.values()) {
            if (sq.getVal() == value) {
                return sq;
            }
        }
        throw new EnumElemDoesNotExistException(Sq.class);
    }
}
