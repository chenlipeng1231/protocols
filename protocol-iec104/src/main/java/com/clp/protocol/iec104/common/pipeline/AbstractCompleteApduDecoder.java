package com.clp.protocol.iec104.common.pipeline;

import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

import java.nio.ByteOrder;

public abstract class AbstractCompleteApduDecoder extends LengthFieldBasedFrameDecoder {
    private static final ByteOrder byteOrder = ByteOrder.LITTLE_ENDIAN;
    private static final int maxFrameLength = 255;
    private static final int lengthFieldOffset = 1;
    private static final int lengthFieldLength = 1;
    private static final int lengthAdjustment = 0;
    private static final int initialBytesToStrip = 0;
    private static final boolean failFast = true;

    public AbstractCompleteApduDecoder() {
        super(byteOrder, maxFrameLength, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip, failFast);
    }
}
