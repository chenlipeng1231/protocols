package com.clp.protocol.iec104.apdu.asdu.infoobj.infoelem.filetransfer.dtfile;

import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePduClip;
import com.clp.protocol.core.pdu.ByteToStringFormat;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * 传输文件的属性（备用）
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DTFileProperty extends BaseNBytePduClip<DTFileProperty> {
    private byte propertyByte;

    @Override
    public DTFileProperty refreshFrom(ByteBuf buf) {
        this.propertyByte = buf.readByte();;
        return this;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void writeBytesTo(ByteBuf buf) {
        buf.writeByte(propertyByte);
    }

    @Override
    public void writeFormattedByteStringsTo(StringBuilder sb, String frameClipBytesSeparator, String byteSeparator, ByteToStringFormat byteFormat) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void writeSimpleDescriptionTo(StringBuilder sb) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void writeDetailDescriptionTo(StringBuilder sb) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        // TODO 暂时这样
        return null;
    }

    @Override
    protected void forEachOneLevelChild(Consumer<BaseNBytePduClip<?>> consumer) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DTFileProperty that = (DTFileProperty) o;
        return propertyByte == that.propertyByte;
    }

    @Override
    public int hashCode() {
        return Objects.hash(propertyByte);
    }
}
