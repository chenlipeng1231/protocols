package com.clp.protocol.iec104.apdu.asdu.infoobj.infoelem;

import com.clp.protocol.iec104.definition.Tm;
import com.clp.protocol.iec104.definition.TypeTag;
import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePduClip;
import com.clp.protocol.core.pdu.ByteToStringFormat;
import io.netty.buffer.ByteBuf;

import java.util.function.Consumer;

/**
 * 设定值命令，标度化值
 */
public class C_SE_NB_1_InfoElem extends TaInfoElem {

    public C_SE_NB_1_InfoElem() {
        super(TypeTag.C_SE_NB_1);
    }

    @Override
    public InfoElem refreshFrom(ByteBuf buf) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isValid() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void writeBytesTo(ByteBuf buf) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void writeFormattedByteStringsTo(StringBuilder sb, String frameClipBytesSeparator, String byteSeparator, ByteToStringFormat byteFormat) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void writeSimpleDescriptionTo(StringBuilder sb) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void writeDetailDescriptionTo(StringBuilder sb) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Tm.Type getTmType() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Number getNumberVal() {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void forEachOneLevelChild(Consumer<BaseNBytePduClip<?>> consumer) {
        throw new UnsupportedOperationException();
    }
}
