package com.clp.protocol.iec104.client.pipeline.state.control;

import com.clp.protocol.iec104.apdu.Apdu;
import com.clp.protocol.iec104.apdu.IApdu;
import com.clp.protocol.iec104.apdu.SApdu;
import com.clp.protocol.iec104.apdu.UApdu;
import com.clp.protocol.iec104.apdu.asdu.IAsdu;
import com.clp.protocol.iec104.apdu.asdu.infoobj.InfoObj;
import com.clp.protocol.iec104.client.config.MasterControlConfig;
import com.clp.protocol.iec104.client.pipeline.MPipelineManager;
import com.clp.protocol.iec104.definition.TypeTag;
import com.clp.protocol.core.pdu.nbytepdu.NBytePduClip;
import com.clp.protocol.core.utils.ByteUtil;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nullable;

@Slf4j
public class MApduLogControlStateHandler extends AbstractMControlStateHandler {
    private final NBytePduClip.ToStringFormat toStringFormat;

    public MApduLogControlStateHandler(MPipelineManager manager, MasterControlConfig cfg) {
        super(manager);
        this.toStringFormat = cfg.getApduToStringFormat();
    }

    @Override
    protected void resetState() {

    }

    @Override
    protected void afterResetState() {

    }

    private void logAgain(Apdu apdu) {
        log.debug("{}", ByteUtil.bytesToHexString(apdu.toBytes()));
    }

    @Override
    protected UApdu updateStateByRecv(UApdu uApdu) {
        uApdu.setToStringFormat(toStringFormat);
        log.debug("[Control] 接收U帧\n{}", uApdu);
        logAgain(uApdu);
        return uApdu;
    }

    @Override
    protected SApdu updateStateByRecv(SApdu sApdu) {
        sApdu.setToStringFormat(toStringFormat);
        log.debug("[Control] 接收S帧\n{}", sApdu);
        logAgain(sApdu);
        return sApdu;
    }

    @Override
    protected IApdu updateStateByRecv(IApdu iApdu) {
        IAsdu iAsdu = iApdu.getIAsdu();
        TypeTag typeTag = iAsdu.getTypeTag();
        if (typeTag == TypeTag.M_SP_NA_1) {
            for (InfoObj infoObj : iAsdu.getInfoObjs()) {
                int addr = infoObj.getAddr();
                if (addr == 1443) {
                    System.out.println(1);
                }
            }
        } else if (typeTag == TypeTag.M_ME_NC_1) {
            for (InfoObj infoObj : iAsdu.getInfoObjs()) {
                int addr = infoObj.getAddr();
                if (addr == 20676) {
                    System.out.println(1);
                }
            }
        }


        iApdu.setToStringFormat(toStringFormat);
        log.debug("[Control] 接收I帧\n{}", iApdu);
        logAgain(iApdu);
        return iApdu;
    }

    @Override
    protected UApdu updateStateBySend(UApdu uApdu) {
        uApdu.setToStringFormat(toStringFormat);
        log.debug("[Control] 发送U帧\n{}", uApdu);
        logAgain(uApdu);
        return uApdu;
    }

    @Override
    protected SApdu updateStateBySend(SApdu sApdu) {
        sApdu.setToStringFormat(toStringFormat);
        log.debug("[Control] 发送S帧\n{}", sApdu);
        logAgain(sApdu);
        return sApdu;
    }

    @Override
    protected IApdu updateStateBySend(IApdu iApdu) {
        iApdu.setToStringFormat(toStringFormat);
        log.debug("[Control] 发送I帧\n{}", iApdu);
        logAgain(iApdu);
        return iApdu;
    }

    @Nullable
    @Override
    protected ScheduledTask getScheduledTask() {
        return null;
    }
}
