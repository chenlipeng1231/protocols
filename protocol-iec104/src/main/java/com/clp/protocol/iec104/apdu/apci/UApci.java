package com.clp.protocol.iec104.apdu.apci;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UApci extends Apci<UApci, UCtrlArea> {
    public UApci(byte head, int length, UCtrlArea uControlArea) {
        super(head, length, uControlArea);
    }
}
