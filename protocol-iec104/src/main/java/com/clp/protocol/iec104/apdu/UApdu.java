package com.clp.protocol.iec104.apdu;

import com.clp.protocol.iec104.apdu.apci.UApci;
import com.clp.protocol.iec104.definition.ApduType;
import com.clp.protocol.iec104.definition.ConstVal;
import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePduClip;
import com.clp.protocol.core.pdu.ByteToStringFormat;
import io.netty.buffer.ByteBuf;
import lombok.*;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * U帧
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UApdu extends Apdu {
    private UApci uApci;

    @Override
    public ApduType apduType() {
        return ApduType.UType;
    }

    @Override
    public UApdu refreshFrom(ByteBuf buf) {
        this.uApci = new UApci().refreshFrom(buf);
        return this;
    }

    @Override
    public boolean isValid() {
        return uApci != null && uApci.isValid();
    }

    @Override
    public void writeBytesTo(ByteBuf buf) {
        uApci.writeBytesTo(buf);
        // 注意：最后要设置长度
        buf.setByte(buf.readerIndex() + 1, buf.readableBytes() - ConstVal.HEAD_LEN - ConstVal.LENGTH_LEN);
    }

    @Override
    public void writeFormattedByteStringsTo(StringBuilder sb, String frameClipBytesSeparator, String byteSeparator, ByteToStringFormat byteFormat) {
        uApci.writeFormattedByteStringsTo(sb, frameClipBytesSeparator, byteSeparator, byteFormat);
    }

    @Override
    public void writeSimpleDescriptionTo(StringBuilder sb) {
        sb.append("     <").append(apduType().getDesc()).append("> ");
        uApci.writeSimpleDescriptionTo(sb);
    }

    @Override
    public void writeDetailDescriptionTo(StringBuilder sb) {
        sb.append("     <帧类型：").append(apduType().getDesc()).append("> ");
        uApci.writeDetailDescriptionTo(sb);
    }

    @Override
    protected void forEachOneLevelChild(Consumer<BaseNBytePduClip<?>> consumer) {
        consumer.accept(uApci);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UApdu uApdu = (UApdu) o;
        return Objects.equals(uApci, uApdu.uApci);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uApci);
    }
}
