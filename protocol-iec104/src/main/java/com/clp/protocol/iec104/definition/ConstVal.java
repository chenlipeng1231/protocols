package com.clp.protocol.iec104.definition;

/**
 * 104协议相关值
 */
public class ConstVal {
    /* 104规约帧各个字段占用的字节数 */
    /**
     * 启动字符：1字节
     */
    public static final int HEAD_LEN = 1;
    /**
     * APDU长度：1字节
     */
    public static final int LENGTH_LEN = 1;
    /**
     * 控制域：4字节
     */
    public static final int CTRL_AREA_LEN = 4;
    /**
     * 类型标识：1字节
     */
    public static final int TYPE_TAG_LEN = 1;
    /**
     * 可变结构限定词：1字节
     */
    public static final int VSQ_LEN = 1;
    /**
     * 传输原因：2字节
     */
    public static final int COT_LEN = 2;
    /**
     * 应用服务数据单元公共地址：2字节
     */
    public static final int RTU_ADDRESS_LEN = 2;
    /**
     * 信息元素地址：3字节
     */
    public static final int INFO_ELEM_ADDRESS_LEN = 3;
    /**
     * 限定词长度：1字节
     */
    public static final int QUALIFIER_LEN = 1;
    /**
     * 时标，4字节
     */
    public static final int CP32TIME2A_LEN = 4;
    /**
     * 时标：7字节
     */
    public static final int CP56TIME2A_LEN = 7;

    /* 常用的值 */
    /**
     * 启动字符值 为固定值
     */
    public static final byte HEAD_VAL = 0x68;
    /**
     * APCI长度
     */
    public static final byte APCI_LEN = 6;
    /**
     * 发送 / 接收 序列号的最大值
     */
    public static final int MAX_SEQ = 32768;

    /* 文件传输 */
    /**
     * 附加数据包类型长度
     */
    public static final int ATTACH_PACK_TYPE_LEN = 1;
    /**
     * 操作标识长度
     */
    public static final int OPERATION_TAG_LEN = 1;
    /**
     * 目录id长度
     */
    public static final int DIRE_ID_LEN = 4;
    /**
     * 目录名长度的长度
     */
    public static final int DIRE_NAME_BYTES_LEN_LEN = 1;
    /**
     * 召唤标志长度
     */
    public static final int CALL_FLAG_LEN = 1;
    /**
     * 结果描述字长度
     */
    public static final int RESULT_DESCRIPTOR_LEN = 1;
    /**
     * 后续标志长度
     */
    public static final int FOLLOW_UP_FLAG_LEN = 1;
    /**
     * 文件数量长度
     */
    public static final int FILE_NUM_LEN = 1;
    /**
     * 文件名称长度的长度
     */
    public static final int FILE_NAME_BYTES_LEN_LEN = 1;
    /**
     * 文件id长度
     */
    public static final int FILE_ID_LEN = 4;
    /**
     * 文件属性长度
     */
    public static final int DT_FILE_PROPERTY_LEN = 1;
    /**
     * 文件大小长度
     */
    public static final int FILE_SIZE_LEN = 4;
    /**
     * 数据短号长度
     */
    public static final int DATA_SEGMENT_LEN = 4;
    /**
     * 校验码长度
     */
    public static final int CHECK_CODE_LEN = 1;

    /**
     * 一个apdu的不变字节大小
     */
    public static final int APDU_CONSTANT_SIZE = APCI_LEN + TYPE_TAG_LEN + VSQ_LEN + COT_LEN + RTU_ADDRESS_LEN;
    /**
     * 一个apdu可变字节大小
     */
    public static final int APDU_VARIABLE_SIZE = ((1 << (LENGTH_LEN * 8)) - 1) - APDU_CONSTANT_SIZE;
}
