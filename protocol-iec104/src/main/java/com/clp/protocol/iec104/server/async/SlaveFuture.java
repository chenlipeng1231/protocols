package com.clp.protocol.iec104.server.async;

import com.clp.protocol.iec104.server.Slave;
import com.clp.protocol.core.async.IFuture;

/**
 * 主站Future，跟ChannelFuture一样，ChannelFuture会绑定对应的Channel，MasterFuture会绑定对应的Master
 * @param <V>
 */
public interface SlaveFuture<V> extends IFuture<V, SlaveFutureListener<V>> {
    /**
     * 返回与此Future相关的主站
     * @return
     */
    Slave slave();

    @Override
    SlaveFuture<V> sync();

    @Override
    SlaveFuture<V> sync(int timeoutMs);

    @Override
    SlaveFuture<V> addListener(SlaveFutureListener<V> listener);
}
