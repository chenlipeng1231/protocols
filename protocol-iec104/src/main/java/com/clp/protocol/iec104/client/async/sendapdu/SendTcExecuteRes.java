package com.clp.protocol.iec104.client.async.sendapdu;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 发送遥控执行 的结果
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class SendTcExecuteRes {
    private volatile boolean isSendExecuteSuccess; // 是否成功发送遥控执行

    private volatile boolean isRecvExecuteAckYes; // 是否接收遥控执行肯定确认
    private volatile boolean isRecvExecuteAckNo; // 是否接收遥控执行否定确认
    private volatile boolean isRecvEnd; // 是否接收遥控执行激活终止

    private String failDesc; // 失败描述
}
