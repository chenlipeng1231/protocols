package com.clp.protocol.iec104.apdu.asdu.infoobj.qua;

import com.clp.protocol.iec104.definition.Tm;
import com.clp.protocol.iec104.definition.TypeTag;
import lombok.NoArgsConstructor;

/**
 * 测量值，归一化值 的限定词
 */
public class M_ME_NA_1_Qua extends TmQua {

    public M_ME_NA_1_Qua() {
        this(null);
    }

    public M_ME_NA_1_Qua(Tm.Valid valid) {
        super(TypeTag.M_ME_NA_1, valid);
    }
}
