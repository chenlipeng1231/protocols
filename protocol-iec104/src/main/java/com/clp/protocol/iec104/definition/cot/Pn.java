package com.clp.protocol.iec104.definition.cot;

import com.clp.protocol.core.utils.EnumElemDoesNotExistException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 传输原因的 肯定/否定确认定义
 */
@Getter
@AllArgsConstructor
public enum Pn {
    /**
     * 0为肯定确认
     */
    PN_YES(0, "肯定确认"),
    /**
     * 1为否定确认
     */
    PN_NO(1, "否定确认");

    private final int val;
    private final String desc;

    public static Pn gain(int value) {
        for (Pn PN : Pn.values()) {
            if (PN.getVal() == value) {
                return PN;
            }
        }
        throw new EnumElemDoesNotExistException(Pn.class);
    }
}
