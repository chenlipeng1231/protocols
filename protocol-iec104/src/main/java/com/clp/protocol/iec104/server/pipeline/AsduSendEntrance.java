package com.clp.protocol.iec104.server.pipeline;

import com.clp.protocol.iec104.apdu.asdu.IAsdu;
import io.netty.channel.ChannelPromise;

/**
 * asdu发送入口
 */
public interface AsduSendEntrance {

    /**
     * 返回false，代表进入等待队列；返回true，代表发送成功
     * @param iAsdu
     * @param promise 最终发送成功后进行通知
     * @return
     */
    void tryChannelSend(IAsdu iAsdu, ChannelPromise promise);

}
