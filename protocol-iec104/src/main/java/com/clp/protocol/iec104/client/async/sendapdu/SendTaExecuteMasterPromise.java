package com.clp.protocol.iec104.client.async.sendapdu;

import com.clp.protocol.iec104.client.Master;
import com.clp.protocol.iec104.client.async.GenericMasterPromise;
import lombok.Getter;

/**
 * 发送遥调执行 的promise
 */
@Getter
public class SendTaExecuteMasterPromise extends GenericMasterPromise<SendTaExecuteMasterPromise, SendTaExecuteRes> {
    /**
     * 遥调点所在的公共地址
     */
    private final int rtuAddr;
    /**
     * 遥调点信息体地址
     */
    private final int addr;

    public SendTaExecuteMasterPromise(Master master, SendTaExecuteRes res, int rtuAddr, int addr) {
        super(master);
        this.res = res;
        this.rtuAddr = rtuAddr;
        this.addr = addr;
    }
}
