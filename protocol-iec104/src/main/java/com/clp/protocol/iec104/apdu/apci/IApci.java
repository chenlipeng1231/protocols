package com.clp.protocol.iec104.apdu.apci;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class IApci extends Apci<IApci, ICtrlArea> {
    public IApci(byte head, int length, ICtrlArea iControlArea) {
        super(head, length, iControlArea);
    }
}
