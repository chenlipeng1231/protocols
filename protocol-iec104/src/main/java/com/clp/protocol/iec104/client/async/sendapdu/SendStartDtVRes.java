package com.clp.protocol.iec104.client.async.sendapdu;

import com.clp.protocol.iec104.common.res.BaseRes;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 发送启动传输激活 的结果
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class SendStartDtVRes extends BaseRes<SendStartDtVRes> {
    private volatile boolean isRecvAck; // 是否成功接收启动传输激活确认
}
