package com.clp.protocol.iec104.apdu.asdu.infoobj.qua;

import com.clp.protocol.iec104.definition.Tm;
import com.clp.protocol.iec104.definition.TypeTag;
import lombok.NoArgsConstructor;

/**
 * 带时标的测量值，归一化值
 */
public class M_ME_TA_1_Qua extends TmQua {

    public M_ME_TA_1_Qua() {
        this(null);
    }

    public M_ME_TA_1_Qua(Tm.Valid valid) {
        super(TypeTag.M_ME_TA_1, valid);
    }
}
