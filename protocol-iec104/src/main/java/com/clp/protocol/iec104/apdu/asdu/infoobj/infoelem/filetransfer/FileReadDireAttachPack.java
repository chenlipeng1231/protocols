package com.clp.protocol.iec104.apdu.asdu.infoobj.infoelem.filetransfer;

import com.clp.protocol.iec104.definition.ConstVal;
import com.clp.protocol.iec104.definition.Ft;
import com.clp.protocol.core.pdu.nbytepdu.time2a.CP56Time2a;
import com.clp.protocol.core.utils.ByteUtil;
import com.clp.protocol.core.utils.time.DateUtil;
import com.clp.protocol.core.utils.StringUtil;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 文件目录召唤 附加数据包
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class FileReadDireAttachPack extends AttachPack {
    @Override
    public Ft.OperationTag operationTag() {
        return Ft.OperationTag.FILE_READ_DIRE;
    }

    /**
     * 目录id
     */
    private long direId;

    /**
     * 目录名长度
     */
    private int direNameBytesLen;

    /**
     * 目录名
     */
    private String direName;

    /**
     * 召唤标志
     */
    private Ft.CallFlag callFlag;

    /**
     * 查询起始时间
     */
    private Date queryStartTime;

    /**
     * 查询终止时间
     */
    private Date queryStopTime;

    @Override
    public FileReadDireAttachPack refreshFrom(ByteBuf buf) {
        checkOperationTag(buf.readByte());
        // 目录id
        byte[] direIdBytes = new byte[ConstVal.DIRE_ID_LEN]; buf.readBytes(direIdBytes);
        this.direId = ByteUtil.bytes4ToLongLE(direIdBytes);
        // 目录名长度
        this.direNameBytesLen = buf.readByte() & 0xFF;
        // 目录名
        byte[] direNameBytes = new byte[direNameBytesLen]; buf.readBytes(direNameBytes);
        this.direName = StringUtil.decode(direNameBytes, "utf-8");
        // 召唤标志
        this.callFlag = Ft.CallFlag.gain(buf.readByte() & 0xFF);
        // 查询起始时间
        byte[] queryStartTimeBytes = new byte[CP56Time2a.bytesLen]; buf.readBytes(queryStartTimeBytes);
        this.queryStartTime = DateUtil.CP56Time2aBytes7ToDate(queryStartTimeBytes);
        // 查询结束时间
        byte[] queryStopTimeBytes = new byte[CP56Time2a.bytesLen]; buf.readBytes(queryStopTimeBytes);
        this.queryStopTime = DateUtil.CP56Time2aBytes7ToDate(queryStopTimeBytes);
        return this;
    }

    @Override
    public boolean isValid() {
        if (direId < 0 || direId > 0xFFFFFFFFL) return false;
        if (direNameBytesLen < 0 || direNameBytesLen > 0xFF) return false;
        if (direName == null || direName.equals("")) return false;
        if (callFlag == null) return false;
        if (queryStartTime == null) return false;
        return queryStopTime != null;
    }

    @Override
    public void writeBytesTo(ByteBuf buf) {
        // 填写操作标识
        buf.writeByte(operationTag().getVal());
        // 目录id
        buf.writeBytes(ByteUtil.longToBytes4LE(direId));
        // 目录名长度
        byte[] direNameBytes = StringUtil.encode(direName, "utf-8");
        buf.writeByte(direNameBytes.length);
        // 目录名
        buf.writeBytes(direNameBytes);
        // 召唤标志
        buf.writeByte(callFlag.getVal());
        // 查询起始时间
        buf.writeBytes(DateUtil.dateToCP56Time2aBytes7(queryStartTime));
        // 查询终止时间
        buf.writeBytes(DateUtil.dateToCP56Time2aBytes7(queryStopTime));
    }

    @Override
    public String toString() {
        // TODO 暂时这样
        return null;
    }
}
