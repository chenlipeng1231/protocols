package com.clp.protocol.iec104.client.pipeline.state.control;

/**
 * 控制信息
 */
public interface MControlInfo {

    /**
     * 是否激活了启动传输
     * @return
     */
    boolean isStartedDt();

    /**
     * 是否初始化结束
     * @return
     */
    boolean isInitCompleted();

    boolean isTestingFr();

    int k();

    int w();

    int t0();

    int t1();

    int t2();

    int t3();

    int sendSeq();

    int recvSeq();

    boolean isDoingTotalCall100();

    boolean isDoingTotalCall101();

    boolean isDoingTcSelect(int infoObjAddr);

    boolean isDoingTcExecute(int infoObjAddr);

    boolean isDoingTaSelect(int infoObjAddr);

    boolean isDoingTaExecute(int infoObjAddr);

    boolean isAutoStartDtV();
}
