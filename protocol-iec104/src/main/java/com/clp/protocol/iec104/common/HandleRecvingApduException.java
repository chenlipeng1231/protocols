package com.clp.protocol.iec104.common;

public class HandleRecvingApduException extends Exception {
    public HandleRecvingApduException(String msg) {
        super(msg);
    }
}
