package com.clp.protocol.iec104.apdu.asdu.infoobj.infoelem;

import com.clp.protocol.iec104.definition.TypeTag;

/**
 * 遥控信息元素类型
 */
public abstract class TcInfoElem extends InfoElem implements HasTcInfo {
    protected TcInfoElem(TypeTag typeTag) {
        super(typeTag);
    }
}
