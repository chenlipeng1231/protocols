package com.clp.protocol.iec104.server.pipeline.state.data;

public interface TsData extends PointData {

    /**
     * 是否为 on
     * @return
     */
    boolean isSwitchOn();

    /**
     * 是否有效
     * @return
     */
    boolean isValid();

    /**
     * 是否为当前值
     * @return
     */
    boolean isCurrVal();

    /**
     * 是否被取代
     * @return
     */
    boolean isReplaced();

    /**
     * 是否被闭锁
     * @return
     */
    boolean isLocked();

    class Comparator implements java.util.Comparator<TsData> {
        public static final Comparator INSTANCE = new Comparator();

        @Override
        public int compare(TsData o1, TsData o2) {
            return o1.getAddress() - o2.getAddress();
        }
    }

}
