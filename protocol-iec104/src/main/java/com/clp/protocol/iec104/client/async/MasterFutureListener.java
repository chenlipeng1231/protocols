package com.clp.protocol.iec104.client.async;

import com.clp.protocol.core.async.IFutureListener;

public interface MasterFutureListener<V> extends IFutureListener<MasterFuture<V>> {
}
