package com.clp.protocol.iec104.server;

public class PortAlreadyBindException extends RuntimeException {

    public PortAlreadyBindException(String host, int port) {
        super("host=" + host + " already bind port=" + port);
    }

}
