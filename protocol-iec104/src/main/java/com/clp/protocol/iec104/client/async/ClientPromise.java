package com.clp.protocol.iec104.client.async;

import com.clp.protocol.core.async.IFutureListener;
import com.clp.protocol.core.async.IPromise;

/**
 * 可能弃用
 * @param <V>
 */
public interface ClientPromise<V> extends ClientFuture<V>, IPromise<V, IFutureListener<ClientFuture<V>>> {

    @Override
    ClientPromise<V> setRes(V val);

    @Override
    ClientPromise<V> setSuccess();

    @Override
    ClientPromise<V> setFailure(Throwable cause);

    @Override
    ClientPromise<V> sync();

    @Override
    ClientPromise<V> addListener(IFutureListener<ClientFuture<V>> listener);

}
