package com.clp.protocol.iec104.apdu.asdu.infoobj.qua;

import com.clp.protocol.iec104.definition.TypeTag;
import com.clp.protocol.iec104.definition.quatype.TaQuaType;
import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePduClip;
import com.clp.protocol.core.pdu.ByteToStringFormat;
import com.clp.protocol.core.utils.ByteUtil;
import io.netty.buffer.ByteBuf;
import lombok.Getter;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * 遥调的限定词
 */
@Getter
public abstract class TaQua extends Qua {
    private TaQuaType type;

    protected TaQua(TypeTag typeTag, TaQuaType type) {
        super(typeTag);
        this.type = type;
    }

    @Override
    public TaQua refreshFrom(ByteBuf buf) {
        this.type = TaQuaType.gain(buf.readByte());
        return this;
    }

    @Override
    public boolean isValid() {
        return type != null;
    }

    @Override
    public void writeBytesTo(ByteBuf buf) {
        buf.writeByte(type.getVal());
    }

    @Override
    public void writeFormattedByteStringsTo(StringBuilder sb, String frameClipBytesSeparator, String byteSeparator, ByteToStringFormat byteFormat) {
        sb.append(byteFormat.format(type.getVal()));
    }

    @Override
    public void writeSimpleDescriptionTo(StringBuilder sb) {
        sb.append(ByteUtil.byteToHexStr(type.getVal(), true));
    }

    @Override
    public void writeDetailDescriptionTo(StringBuilder sb) {
        sb.append(type.getDesc()).append("(").append(ByteUtil.byteToHexStr(type.getVal(), true)).append(")");
    }

    @Override
    protected void forEachOneLevelChild(Consumer<BaseNBytePduClip<?>> consumer) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaQua taQua = (TaQua) o;
        return type == taQua.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }
}
