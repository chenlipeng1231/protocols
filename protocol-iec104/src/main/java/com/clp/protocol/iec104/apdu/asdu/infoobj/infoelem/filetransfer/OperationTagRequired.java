package com.clp.protocol.iec104.apdu.asdu.infoobj.infoelem.filetransfer;

import com.clp.protocol.iec104.definition.Ft;

public interface OperationTagRequired {

    Ft.OperationTag operationTag();

    default void checkOperationTag(byte otBy) {
        if (operationTag().getVal() != (otBy & 0xFF)) {
            throw new RuntimeException("Wrong: " + Ft.OperationTag.class.getSimpleName());
        }
    }
}
