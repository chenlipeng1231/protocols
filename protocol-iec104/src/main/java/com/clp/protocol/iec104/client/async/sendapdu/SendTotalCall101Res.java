package com.clp.protocol.iec104.client.async.sendapdu;

import com.clp.protocol.iec104.common.res.BaseRes;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 发送总召唤101（电度量召唤） 的结果
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class SendTotalCall101Res extends BaseRes<SendTotalCall101Res> {
    private volatile boolean isRecvTotalCall101Ack; // 是否接收到总召唤101（电度量召唤）确认
    private volatile boolean isRecvTotalCall101End; // 是否接收到总召唤101（电度量召唤）终止
}
