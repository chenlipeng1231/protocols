package com.clp.protocol.iec104.client.async;

import com.clp.protocol.core.async.IPromise;

/**
 * 主站Promise
 * @param <V> 值
 */
public interface MasterPromise<V> extends MasterFuture<V>, IPromise<V, MasterFutureListener<V>> {

    @Override
    MasterPromise<V> setRes(V val);

    @Override
    MasterPromise<V> setSuccess();

    @Override
    MasterPromise<V> setFailure(Throwable cause);

    @Override
    MasterPromise<V> sync();

    @Override
    MasterPromise<V> sync(int timeoutMs);

    @Override
    MasterPromise<V> addListener(MasterFutureListener<V> listener);

}
