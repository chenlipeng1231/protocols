package com.clp.protocol.iec104.server.pipeline.state.data;

import com.clp.protocol.iec104.definition.Ta;
import com.clp.protocol.iec104.server.SlaveChannel;
import lombok.Getter;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

@Getter
public abstract class TaDataReactor extends DataReactor {
    private final Ta.Type taType;

    public TaDataReactor(Executor executor, Ta.Type taType) {
        super(executor);
        this.taType = taType;
    }

    /**
     * 是否接受遥调选择
     * @param slaveChannel
     * @return
     */
    public abstract CompletableFuture<Boolean> acceptSelect(SlaveChannel slaveChannel, int infoObjAddr);

    /**
     * 是否接受遥调执行
     * @param slaveChannel
     * @return
     */
    public abstract CompletableFuture<Boolean> acceptExecute(SlaveChannel slaveChannel, int infoObjAddr);
}
