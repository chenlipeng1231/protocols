package com.clp.protocol.iec104.apdu.asdu.infoobj;

import com.clp.protocol.iec104.definition.TypeTag;

/**
 * 需要类型标识
 */
public interface TypeTagRequired {
    TypeTag typeTag();

    default void checkTypeTag() {
        if (typeTag() == null) {
            throw new NullPointerException("TypeTag字段不能为空！");
        }
    }

    default boolean hasTypeTag() {
        return typeTag() != null;
    }
}
