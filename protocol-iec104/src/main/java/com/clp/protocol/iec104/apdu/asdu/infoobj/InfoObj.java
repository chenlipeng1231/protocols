package com.clp.protocol.iec104.apdu.asdu.infoobj;

import com.clp.protocol.iec104.apdu.asdu.infoobj.infoelem.InfoElem;
import com.clp.protocol.iec104.apdu.asdu.infoobj.qua.Qua;
import com.clp.protocol.iec104.definition.TypeTag;
import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePduClip;
import com.clp.protocol.core.pdu.nbytepdu.time2a.Time2a;

import java.util.function.Consumer;

public abstract class InfoObj extends BaseNBytePduClip<InfoObj> implements TypeTagRequired {

    private final TypeTag typeTag;

    protected InfoObj(TypeTag typeTag) {
        this.typeTag = typeTag;
    }

    @Override
    public TypeTag typeTag() {
        return typeTag;
    }

    /**
     * 获取地址
     * @return
     */
    public abstract int getAddr();

    /**
     * 获取信息元素
     * @return
     */
    public abstract InfoElem getInfoElem();

    /**
     * 获取限定词
     * @return
     */
    public abstract Qua getQua();

    /**
     * 获取时标
     * @return
     */
    public abstract Time2a getTime2a();

    @Override
    protected void forEachOneLevelChild(Consumer<BaseNBytePduClip<?>> consumer) {
        checkTypeTag();
        TypeTag typeTag = typeTag();
        if (typeTag.hasInfoElem()) {
            consumer.accept(getInfoElem());
        }
        if (typeTag.hasQua()) {
            consumer.accept(getQua());
        }
        if (typeTag.hasTime2a()) {
            consumer.accept(getTime2a());
        }
    }
}
