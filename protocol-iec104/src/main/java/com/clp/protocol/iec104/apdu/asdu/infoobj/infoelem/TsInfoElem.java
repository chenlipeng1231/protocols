package com.clp.protocol.iec104.apdu.asdu.infoobj.infoelem;

import com.clp.protocol.iec104.definition.TypeTag;

/**
 * 遥信信息元素类型
 */
public abstract class TsInfoElem extends InfoElem implements HasTsInfo {
    protected TsInfoElem(TypeTag typeTag) {
        super(typeTag);
    }
}
