package com.clp.protocol.iec104.client.async;

import com.clp.protocol.core.async.IFuture;
import com.clp.protocol.core.async.IFutureListener;
import com.clp.protocol.iec104.client.Iec104MasterManager;

/**
 * 可能弃用
 * @param <V>
 */
public interface ClientFuture<V> extends IFuture<V, IFutureListener<ClientFuture<V>>> {
    /**
     * 返回与此Future相关的主站
     * @return
     */
    Iec104MasterManager client();

    @Override
    ClientFuture<V> sync();

    @Override
    ClientFuture<V> addListener(IFutureListener<ClientFuture<V>> listener);

}
