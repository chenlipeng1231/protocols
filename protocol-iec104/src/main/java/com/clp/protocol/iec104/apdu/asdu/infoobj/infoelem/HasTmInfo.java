package com.clp.protocol.iec104.apdu.asdu.infoobj.infoelem;

import com.clp.protocol.iec104.definition.Tm;

/**
 * 有遥测信息
 */
public interface HasTmInfo {
    /**
     * 值类型
     * @return
     */
    Tm.Type getTmType();

    /**
     * 获取值（包装类）
     */
    Number getNumberVal();
}
