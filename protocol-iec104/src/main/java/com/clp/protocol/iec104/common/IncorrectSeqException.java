package com.clp.protocol.iec104.common;

public class IncorrectSeqException extends HandleRecvingApduException {

    public IncorrectSeqException(String msg) {
        super(msg);
    }
}
