package com.clp.protocol.iec104.client.event;

import com.clp.protocol.iec104.client.Master;
import com.clp.protocol.core.event.Event;
import com.clp.protocol.core.utils.AssertUtil;
import lombok.Getter;

@Getter
public final class MasterConnectEvent extends Event<Master> {
    private final Type type;

    public MasterConnectEvent(Master source, Type type, Throwable cause) {
        super(source, cause);
        AssertUtil.notNull(type);
        this.type = type;
    }

    public MasterConnectEvent(Master source, Type type) {
        this(source, type, null);
    }

    public enum Type {
        /**
         * 开始新连接前
         */
        CONNECT_BEFORE,

        /**
         * 新连接 成功
         */
        CONNECT_SUCCESS,

        /**
         * 新连接失败
         */
        CONNECT_FAILED,

        AUTO_RECONNECT_BEFORE,
        AUTO_RECONNECT_SUCCESS,
        AUTO_RECONNECT_FAILED,

        /**
         * 重连前
         */
        RECONNECT_BEFORE,

        /**
         * 重连成功
         */
        RECONNECT_SUCCESS,

        /**
         * 重连失败
         */
        RECONNECT_FAILED,

        /**
         * 自动断开连接前
         */
        AUTO_DISCONNECT_BEFORE,
        /**
         * 自动断开连接成功
         */
        AUTO_DISCONNECT_SUCCESS,
        /**
         * 自动断开连接失败
         */
        AUTO_DISCONNECT_FAILED,

        /**
         * 断开连接前
         */
        DISCONNECT_BEFORE,

        /**
         * 断开连接成功
         */
        DISCONNECT_SUCCESS,

        /**
         * 断开连接失败
         */
        DISCONNECT_FAILED;
    }
}
