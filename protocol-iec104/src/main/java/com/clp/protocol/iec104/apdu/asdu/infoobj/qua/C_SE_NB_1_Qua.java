package com.clp.protocol.iec104.apdu.asdu.infoobj.qua;

import com.clp.protocol.iec104.definition.TypeTag;
import com.clp.protocol.iec104.definition.quatype.TaQuaType;
import lombok.Getter;

/**
 * 设定值命令，标度化值
 */
@Getter
public class C_SE_NB_1_Qua extends TaQua {

    public C_SE_NB_1_Qua() {
        this(null);
    }

    public C_SE_NB_1_Qua(TaQuaType type) {
        super(TypeTag.C_SE_NB_1, type);
    }
}
