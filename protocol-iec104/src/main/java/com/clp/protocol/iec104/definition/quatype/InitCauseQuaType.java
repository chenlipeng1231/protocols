package com.clp.protocol.iec104.definition.quatype;

import com.clp.protocol.core.utils.EnumElemDoesNotExistException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 初始化结束限定词类型 - 初始化原因
 */
@Getter
@AllArgsConstructor
public enum InitCauseQuaType {
    INIT_CAUSE_0((byte)0x00, "初始化结束：0，当地电源合上"),
    INIT_CAUSE_1((byte)0x01, "初始化结束：1，当地手动复位"),
    INIT_CAUSE_2((byte)0x02, "初始化结束：2，远方复位");

    private final byte val;
    private final String desc;

    public static InitCauseQuaType gain(byte val) {
        for (InitCauseQuaType quaType : InitCauseQuaType.values()) {
            if (quaType.val == val) {
                return quaType;
            }
        }
        throw new EnumElemDoesNotExistException(InitCauseQuaType.class);
    }
}
