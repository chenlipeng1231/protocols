package com.clp.protocol.iec104.server;

import com.clp.protocol.iec104.server.async.DefaultSlavePromise;
import com.clp.protocol.iec104.server.async.SlavePromise;

import java.util.concurrent.Executor;
import java.util.function.Consumer;

public interface Slave {

    Executor executor();

    /**
     * 绑定的本地ip
     * @return
     */
    String localHost();

    /**
     * 绑定的本地端口号
     * @return
     */
    int localPort();

    /**
     * 子站的地址
     * @return
     */
    int rtuAddr();

    /**
     * 子站是否已经开启
     * @return
     */
    boolean isOpen();

    default <V> SlavePromise<V> newPromise(Class<V> clazz) {
        return new DefaultSlavePromise<>(this);
    }

    void forEachSlaveChannel(Consumer<SlaveChannel> consumer);
}
