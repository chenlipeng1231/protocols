package com.clp.protocol.iec104.definition.quatype;

import com.clp.protocol.core.utils.EnumElemDoesNotExistException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 遥调的限定词类型
 */
@Getter
@AllArgsConstructor
public enum TaQuaType {
    SELECT((byte)0x80, "设置命令限定词(遥调），选择预置参数"),
    EXECUTE((byte)0x00, "设置命令限定词（遥调），执行激活参数");

    private final byte val;
    private final String desc;

    public static TaQuaType gain(byte val) {
        for (TaQuaType type : TaQuaType.values()) {
            if (type.val == val) {
                return type;
            }
        }
        throw new EnumElemDoesNotExistException(TaQuaType.class);
    }
}
