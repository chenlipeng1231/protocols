package com.clp.protocol.iec104.client;

import com.clp.protocol.iec104.apdu.asdu.IAsdu;
import com.clp.protocol.core.pdu.PduRecver;

/**
 * 主站Apdu接收器
 */
public interface MasterIAsduRecver extends PduRecver<IAsdu> {

    Master master();

}
