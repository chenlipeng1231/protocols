package com.clp.protocol.iec104.common.res;

/**
 * 基础结果
 * @param <R>
 */
public abstract class BaseRes<R extends BaseRes<R>> {
    private volatile boolean isSendSuccess; // 是否成功发送

    @SuppressWarnings("unchecked")
    protected R self() {
        return ((R) this);
    }

    public boolean getSendSuccess() {
        return isSendSuccess;
    }

    public R setSendSuccess(boolean isSendSuccess) {
        this.isSendSuccess = isSendSuccess;
        return self();
    }
}
