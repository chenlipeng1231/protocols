package com.clp.protocol.iec104.client.pipeline.state.data;

import java.util.List;

public interface MDataInfo {
    List<MTcDataHandler.PtState> listAllDoingTcPtStates();

    List<MTaDataHandler.PtState> listAllDoingTaPtStates();

    int totalCall100PeriodSeconds();

    int totalCall101PeriodSeconds();
}
