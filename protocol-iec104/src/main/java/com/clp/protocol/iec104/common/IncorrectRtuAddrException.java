package com.clp.protocol.iec104.common;

public class IncorrectRtuAddrException extends HandleRecvingApduException {
    public IncorrectRtuAddrException(int expectRtuAddr, int rtuAddr) {
        super("Expect rtu address is " + expectRtuAddr + ", but received " + rtuAddr);
    }
}
