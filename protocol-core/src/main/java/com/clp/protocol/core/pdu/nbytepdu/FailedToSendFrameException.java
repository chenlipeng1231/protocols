package com.clp.protocol.core.pdu.nbytepdu;

public class FailedToSendFrameException extends RuntimeException {
    public FailedToSendFrameException(NBytePdu<?> frame) {
        super("Failed to send frame: " + frame);
    }
}
