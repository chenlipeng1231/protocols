package com.clp.protocol.core.utils;

import java.math.BigDecimal;

public class NumberUtil {

    public static boolean isInteger(Number val) {
        AssertUtil.notNull(val, "val");
        if (val instanceof Float) {
            return isInteger(((Float) val));
        }
        if (val instanceof Double) {
            return isInteger(((Double) val));
        }
        if (val instanceof BigDecimal) {
            return isInteger(((BigDecimal) val));
        }
        return true;
    }

    /**
     * 是否为整数
     * @param val
     * @return
     */
    public static boolean isInteger(float val) {
        return val == Math.round(val);
    }

    public static boolean isInteger(Float val) {
        AssertUtil.notNull(val, "val");
        return isInteger(val.floatValue());
    }

    public static boolean isInteger(double val) {
        return val == Math.round(val);
    }

    public static boolean isInteger(Double val) {
        AssertUtil.notNull(val, "val");
        return isInteger(val.doubleValue());
    }

    public static boolean isInteger(BigDecimal val) {
        AssertUtil.notNull(val, "val");
        return val.stripTrailingZeros().scale() == 0;
    }

}
