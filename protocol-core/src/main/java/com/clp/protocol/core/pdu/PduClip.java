package com.clp.protocol.core.pdu;

/**
 * 协议数据单元 片
 */
public interface PduClip {

    /**
     * 是否是有效的帧片段
     * @return
     */
    boolean isValid();

    /**
     * 检查帧片段是否有效
     */
    default void checkValid() {
        if (!isValid()) {
            throw new InvalidPduClipException(this);
        }
    }
}
