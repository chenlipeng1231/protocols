package com.clp.protocol.core.utils;

/**
 * 十六进制但字符转整型异常
 */
public class ParseHexCharToIntException extends RuntimeException {
    public ParseHexCharToIntException(String msg) {
        super(msg);
    }
}
