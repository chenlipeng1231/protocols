package com.clp.protocol.core.event;

import com.clp.protocol.core.utils.AssertUtil;
import lombok.Getter;

/**
 * 容器事件
 */
@Getter
public final class ListenableContainerEvent<E> extends Event<ListenableContainer<E>> {
    private final Type type;
    private final E element;

    public ListenableContainerEvent(ListenableContainer<E> source, Type type, E element) {
        this(source, type, element, null);
    }

    public ListenableContainerEvent(ListenableContainer<E> source, Type type, E element, Throwable cause) {
        super(source, cause);
        AssertUtil.notNull(type);
        this.type = type;
        this.element = element;
    }

    public enum Type {
        ELEMENT_ADDED,
        ELEMENT_REMOVED
    }
}
