package com.clp.protocol.core.pdu;

import com.clp.protocol.core.utils.ByteUtil;

/**
 * 字节打印格式，给定一个 byte 类型的数，转换成一个字符串的规则
 */
public enum ByteToStringFormat {
    /**
     * 二进制
     */
    BINARY() {
        @Override
        public String format(byte by) {
            return ByteUtil.byteToBinString(by);
        }
    },

    /**
     * 有符号十进制
     */
    SIGNED_DECIMAL() {
        @Override
        public String format(byte by) {
            return "" + by;
        }
    },

    /**
     * 无符号十进制
     */
    UNSIGNED_DECIMAL() {
        @Override
        public String format(byte by) {
            return "" + (by & 0xFF);
        }
    },

    /**
     * 十六进制
     */
    UPPERCASE_HEXADECIMAL() {
        @Override
        public String format(byte by) {
            return ByteUtil.byteToHexStr(by, true);
        }
    },

    SMALL_HEXADECIMAL() {
        @Override
        public String format(byte by) {
            return ByteUtil.byteToHexStr(by, false);
        }
    };

    public abstract String format(byte by);
}
