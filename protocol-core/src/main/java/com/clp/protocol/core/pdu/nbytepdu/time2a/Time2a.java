package com.clp.protocol.core.pdu.nbytepdu.time2a;

import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePduClip;

import java.util.Date;

/**
 * 时标
 */
public abstract class Time2a extends BaseNBytePduClip<Time2a> {

    public static CP32Time2a newInvalidCP32Time2a() {
        return new CP32Time2a();
    }

    public static CP56Time2a newInvalidCP56Time2a() {
        return new CP56Time2a();
    }

    public abstract Date toDate();
}
