package com.clp.protocol.core.async;

@SuppressWarnings("rawtypes")
public interface IPromise<V, IFL extends IFutureListener> extends IFuture<V, IFL> {
//public interface IPromise<V, IFL extends IFutureListener<? extends IFuture<V, IFL>>> extends IFuture<V, IFL> {

    IPromise<V, IFL> setRes(V val);

    IPromise<V, IFL> setSuccess();

    IPromise<V, IFL> setFailure(Throwable cause);

    @Override
    IPromise<V, IFL> sync(int timeoutMs);

    @Override
    IPromise<V, IFL> sync();

    @Override
    IPromise<V, IFL> addListener(IFL listener);
}
