package com.clp.protocol.core.client;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

/**
 * 基于 netty 的连接器
 */
public interface NettyConnector {

    ChannelFuture connect(ChannelInitializer<SocketChannel> aInitializer, String remoteHost, int remotePort);

    ChannelFuture connect(ChannelInitializer<SocketChannel> aInitializer, String localHost, int localPort, String remoteHost, int remotePort);

}
