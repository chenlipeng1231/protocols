package com.clp.protocol.core.client;

import lombok.Getter;

/**
 * 连接已存在异常
 */
@Getter
public class ConnectionAlreadyExistException extends FailedToConnectException {
    private final String remoteHost1;
    private final int remotePort1;
    private final String remoteHost2;
    private final int remotePort2;

    public ConnectionAlreadyExistException(String remoteHost, int remotePort) {
        super("连接失败：已经存在连接 " + remoteHost + ":" + remotePort + " 的主站！");
        this.remoteHost1 = remoteHost;
        this.remotePort1 = remotePort;
        this.remoteHost2 = null;
        this.remotePort2 = 0;
    }

    public ConnectionAlreadyExistException(String remoteHost1, int remotePort1, String remoteHost2, int remotePort2) {
        super("连接失败：已经存在连接 " + remoteHost1 + ":" + remotePort1 + " 和 " + remoteHost2 + ":" + remotePort2 + " 的主站！");
        this.remoteHost1 = remoteHost1;
        this.remotePort1 = remotePort1;
        this.remoteHost2 = remoteHost2;
        this.remotePort2 = remotePort2;
    }
}
