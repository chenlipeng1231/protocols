package com.clp.protocol.core.pdu;

public interface JsonPdu extends Pdu {

    /**
     * 将json字符串写入 StringBuilder
     * @param sb
     */
    void writeJsonTo(StringBuilder sb);

    default String toJsonString() {
        StringBuilder sb = new StringBuilder();
        writeJsonTo(sb);
        return sb.toString();
    }
}
