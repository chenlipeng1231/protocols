package com.clp.protocol.core.event;

import com.clp.protocol.core.utils.ModifierUtil;
import lombok.Getter;

import javax.annotation.Nullable;
import java.util.Objects;

/**
 * 事件
 */
@Getter
public abstract class Event<S> {
    protected S source; // 事件源
    @Nullable
    protected Throwable cause; // 原因

    protected Event(S source) {
        this(source, null);
    }

    protected Event(S source, @Nullable Throwable cause) {
        if (!ModifierUtil.hasModifier(this.getClass(), ModifierUtil.ModifierType.FINAL)) {
            throw new IllegalArgumentException("事件类必须为final！");
        }
        Objects.requireNonNull(source, "Client event source can not be null!");
        this.source = source;
        this.cause = cause;
    }
}
