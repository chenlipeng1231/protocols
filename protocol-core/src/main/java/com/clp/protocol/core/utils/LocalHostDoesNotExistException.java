package com.clp.protocol.core.utils;

import lombok.Getter;

/**
 * 不存在该本地ip异常
 */
@Getter
public class LocalHostDoesNotExistException extends Exception {
    /** 不存在的host */
    private final String notExistLocalHost;

    public LocalHostDoesNotExistException(String localHost) {
        super("本机不存在该ip:" + localHost);
        this.notExistLocalHost = localHost;
    }
}
