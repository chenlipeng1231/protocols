package com.clp.protocol.core.utils;

public class ParseCharToIntException extends Exception{
    public ParseCharToIntException(String message) {
        super(message);
    }
}
