package com.clp.protocol.core.async;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface SuccessPolicy {
    SuccessPolicy ALL_IS_SUCCESS = AllIsSuccessPolicy.INSTANCE;
    SuccessPolicy ANY_IS_SUCCESS = AnyIsSuccessPolicy.INSTANCE;

    default <P extends IPromise<?, ?>, F extends IFuture<?, ?>> void applyIPromise(P promise, F... futures) {
        applyIPromise(promise, Arrays.asList(futures));
    }

    @SuppressWarnings("rawtypes")
    <P extends IPromise, F extends IFuture> void applyIPromise(P promise, List<F> futures);

    // ----------------------------------------------------------------------------------------------
    CompletableFuture<Boolean> applyCompletableFuture(CompletableFuture<Boolean>... futures);

    @SuppressWarnings("unchecked")
    default CompletableFuture<Boolean> applyCompletableFuture(List<CompletableFuture<Boolean>> futures) {
        return applyCompletableFuture(futures.toArray(new CompletableFuture[0]));
    }

    // ----------------------------------------------------------------------------------------------

    default boolean applyBoolean(Boolean... isTrues) {
        return applyBoolean(Arrays.asList(isTrues));
    }

    default boolean applyBoolean(boolean... isTrues) {
        List<Boolean> isTrueList = new ArrayList<>();
        for (boolean isTrue : isTrues) {
            isTrueList.add(isTrue);
        }
        return applyBoolean(isTrueList);
    }

    boolean applyBoolean(List<Boolean> isTrues);
}
