package com.clp.protocol.core.pdu;

@FunctionalInterface
public interface PduSendCallback<P extends Pdu> {

    void beforeSend(P pdu);

    default void afterSendSuccess(P pdu) {}

    default void afterSendFailed(P pdu, Throwable cause) {}

}
