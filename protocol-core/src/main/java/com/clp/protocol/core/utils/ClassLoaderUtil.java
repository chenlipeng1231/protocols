package com.clp.protocol.core.utils;

public class ClassLoaderUtil {
    /**
     * 获取顺序如下：
     *  1、获取当前线程的ContextClassLoader
     *  2、获取当前类对应的ClassLoader
     *  3、获取系统ClassLoader
     * @return
     */
    public static ClassLoader getClassLoader() {
        ClassLoader classLoader = getContextClassLoader();
        if (classLoader == null) {
            classLoader = ClassLoaderUtil.class.getClassLoader();
            if (null == classLoader) {
                classLoader = ClassLoader.getSystemClassLoader();
            }
        }
        return classLoader;
    }

    public static ClassLoader getContextClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }
}
