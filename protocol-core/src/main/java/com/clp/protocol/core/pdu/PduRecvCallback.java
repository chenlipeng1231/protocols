package com.clp.protocol.core.pdu;

@FunctionalInterface
public interface PduRecvCallback<P extends Pdu> {

    boolean whenRecv(P frame);
}
