package com.clp.protocol.core.pdu.nbytepdu.time2a;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Week {
    NONE(0, "无效星期"),
    MONDAY(1, "星期一"),
    TUESDAY(2, "星期二"),
    WEDNESDAY(3, "星期三"),
    THURSDAY(4, "星期四"),
    FRIDAY(5, "星期五"),
    SATURDAY(6, "星期六"),
    SUNDAY(7, "星期日");

    private int val;
    private String desc;

    public static Week gain(int val) {
        for (Week week : Week.values()) {
            if (week.val == val) {
                return week;
            }
        }
        return NONE;
    }
}
