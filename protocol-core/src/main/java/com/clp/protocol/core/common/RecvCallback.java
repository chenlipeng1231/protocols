package com.clp.protocol.core.common;

public interface RecvCallback<T> {

    void whenRecv(T t);

}
