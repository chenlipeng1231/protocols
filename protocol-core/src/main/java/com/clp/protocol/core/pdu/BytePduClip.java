package com.clp.protocol.core.pdu;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface BytePduClip<BPC extends BytePduClip<BPC>> extends PduClip {
    BPC refreshFrom(InputStream inputStream) throws IOException;

    void writeBytesTo(OutputStream outputStream) throws IOException;

    byte[] toBytes();

    /**
     * 格式化字节数组并生成字符串表示
     *  每个方法开头不加 byteSeparator，每次遇到子片段先添加  frameClipBytesSeparator，子片段执行后如果还有内容，再添加 FrameClipBytesSeparator
     * @param sb 要写入的字符串
     * @param byteFormat 字节转换字符串格式
     * @param byteSeparator 字节分隔符
     * @param frameClipBytesSeparator 帧片段分隔符
     */
    void writeFormattedByteStringsTo(StringBuilder sb, String frameClipBytesSeparator, String byteSeparator, ByteToStringFormat byteFormat);

    void writeSimpleDescriptionTo(StringBuilder sb);

    void writeDetailDescriptionTo(StringBuilder sb);
}
