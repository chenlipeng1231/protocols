package com.clp.protocol.core.pdu;

import java.util.Collection;

public interface PduSender<P extends Pdu> {

    void addSendCallback(PduSendCallback<P> callback);

    void addSendCallbacks(Collection<PduSendCallback<P>> callbacks);

    void removeSendCallback(PduSendCallback<P> callback);

    void removeSendCallbacks(Collection<PduSendCallback<P>> callbacks);

}
