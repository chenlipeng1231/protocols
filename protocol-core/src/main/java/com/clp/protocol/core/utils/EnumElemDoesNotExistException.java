package com.clp.protocol.core.utils;

/**
 * 枚举元素不存在 异常
 */
public class EnumElemDoesNotExistException extends RuntimeException {
    public EnumElemDoesNotExistException(Class<? extends Enum<?>> enumClass) {
        super("Could not find the elem from enum " + enumClass.getSimpleName());
    }
}
