package com.clp.protocol.core.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MethodUtil {

    @SuppressWarnings("unchecked")
    public static <R> R invoke(Object obj, Method method, Object... params) {
        if (obj == null || method == null) {
            throw new IllegalArgumentException();
        }
        try {
            method.setAccessible(true);
            return ((R) method.invoke(obj, params));
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
