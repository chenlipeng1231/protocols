package com.clp.protocol.core.pdu.nbytepdu;

import com.clp.protocol.core.pdu.BytePdu;

public interface NBytePdu<P extends NBytePdu<P>> extends NBytePduClip<P>, BytePdu<P> {

}
