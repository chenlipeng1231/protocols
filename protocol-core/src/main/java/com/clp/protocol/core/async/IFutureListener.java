package com.clp.protocol.core.async;

import java.lang.reflect.Method;

/**
 * 当IFuture的结果已经获取到时会调用监听器的这个方法
 * @param <IF>
 */
@FunctionalInterface
@SuppressWarnings("rawtypes")
public interface IFutureListener<IF extends IFuture> {
    //public interface IFutureListener<IF extends IFuture<?, ? extends IFutureListener<IF>>> {
    void operationComplete(IF future);

    static Method getOperationCompleteMethod() {
        try {
            return IFutureListener.class.getMethod("operationComplete", IFuture.class);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }
}
