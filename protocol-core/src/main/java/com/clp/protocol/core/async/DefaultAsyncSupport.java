package com.clp.protocol.core.async;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class DefaultAsyncSupport implements AsyncSupport, Closeable {
    private final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(8);

    @Override
    public ScheduledExecutorService scheduledExecutorService() {
        return scheduledExecutorService;
    }

    @Override
    public void close() throws IOException {
        scheduledExecutorService.shutdown();
    }
}
