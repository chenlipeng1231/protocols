package com.clp.protocol.core.utils;

import java.util.Collection;

public class AssertUtil {

    public static void isTrue(boolean flag, String msg) {
        if (!flag) {
            throw new IllegalArgumentException(msg);
        }
    }

    public static void isTrue(boolean flag) {
        isTrue(flag, "Expect true but actually false");
    }

    public static void notNull(Object obj, String msg) {
        if (obj == null) {
            throw new IllegalArgumentException(msg);
        }
    }

    public static void notNull(Object obj) {
        notNull(obj, "Expect a not null object but actually null");
    }

    public static void nonNullIncludingElements(Collection<?> collection, String msg) {
        AssertUtil.notNull(collection, msg);
        for (Object elem : collection) {
            if (elem == null) {
                throw new IllegalArgumentException(msg);
            }
        }
    }

    public static void nonNullIncludingElements(Collection<?> collection) {
        nonNullIncludingElements(collection, "Expect a not null collection that has not null elements bu actually not fits");
    }

    public static void notEmpty(Collection<?> collection, String msg) {
        AssertUtil.notNull(collection, msg);
        if (collection.isEmpty()) {
            throw new IllegalArgumentException(msg);
        }
    }

    public static void notEmpty(Collection<?> collection) {
        notEmpty(collection, "Expect a not empty collection but actually empty");
    }

}
