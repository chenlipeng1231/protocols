package com.clp.protocol.core.common;

/**
 * 可以激活的
 * @param <R>
 */
public interface Activatable<R> {

    /**
     * 激活
     * @return
     */
    R activate();

    /**
     * 是否处于激活状态
     * @return
     */
    boolean isActivated();
}
