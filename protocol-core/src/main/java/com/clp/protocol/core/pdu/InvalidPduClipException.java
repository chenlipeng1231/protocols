package com.clp.protocol.core.pdu;

/**
 * 非法帧异常
 */
public class InvalidPduClipException extends RuntimeException {
    public InvalidPduClipException(PduClip pduClip) {
        super("Invalid pduClip: " + pduClip);
    }
}
