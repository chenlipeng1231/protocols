package com.clp.protocol.core.client;

public class CouldNotAutoReconnectException extends Exception {

    public CouldNotAutoReconnectException(String msg) {
        super(msg);
    }

    public CouldNotAutoReconnectException(Throwable cause) {
        super(cause);
    }

}
