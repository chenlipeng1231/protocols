package com.clp.protocol.core.event;

import com.clp.protocol.core.utils.ModifierUtil;
import com.clp.protocol.core.utils.TypeResolver;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 客户端事件 监听器
 * @param <E> 监听的事件
 *              只会监听本事件，而不会监听该事件的子事件
 */
public interface EventListener<E extends Event<?>> {
    /**
     * 缓存键值对
     */
    @SuppressWarnings("rawtypes")
    Map<Class<? extends EventListener>, Class<? extends Event>> CACHE_MAP = new ConcurrentHashMap<>(64);
    /**
     * 最大缓存容量
     */
    int MAX_CACHE_MAP_SIZE = 1024;

    @SuppressWarnings({"unchecked", "rawtypes"})
    default Class<E> eventClass() {
        final Class<? extends EventListener> thisClass = this.getClass();

        Class<? extends Event> eventClass = CACHE_MAP.get(thisClass);
        if (eventClass != null) {
            return (Class<E>) eventClass;
        }
        eventClass = (Class<E>) TypeResolver.resolveRawArguments(EventListener.class, thisClass)[0];
//        eventClass = (Class<E>) GenericTypeResolver.resolveTypeArgument(this.getClass(), EventListener.class);
        if (!ModifierUtil.hasModifier(eventClass, ModifierUtil.ModifierType.FINAL)) {
            throw new IllegalArgumentException("事件类必须为final！");
        }

        if (CACHE_MAP.size() <= MAX_CACHE_MAP_SIZE) {
            CACHE_MAP.put(thisClass, eventClass);
        }

        return (Class<E>) eventClass;
    }

    void onEvent(E event);

}
