package com.clp.protocol.core.utils;

import cn.hutool.core.bean.BeanUtil;

import java.util.Map;

public class ObjectUtil {

    public static boolean isNull(Object obj) {
        return obj == null;
    }

    public static <T> T mapToObject(Map<?, ?> map, Class<T> clazz) {
        return mapToObject(map, ClassUtil.newInstance(clazz));
    }

    public static <T> T mapToObject(Map<?, ?> map, T obj) {
        return BeanUtil.fillBeanWithMap(map, obj, false);
    }

}
