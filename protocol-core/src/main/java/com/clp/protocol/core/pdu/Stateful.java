package com.clp.protocol.core.pdu;

public interface Stateful<SP extends Pdu, RP extends Pdu> {
    /**
     * 因为写出了Apdu，所以要调整状态信息
     *
     * @param pdu
     */
    SP updateStateBySending(SP pdu);

    /**
     * 因为读入了Apdu，所以要调整状态信息
     *
     * @param pdu
     */
    RP updateStateByRecving(RP pdu);

}
