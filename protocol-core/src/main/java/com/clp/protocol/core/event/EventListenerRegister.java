package com.clp.protocol.core.event;

import java.util.Collection;

/**
 * 客户端事件监听器 注册器
 */
public interface EventListenerRegister<S> {

    /**
     * 添加事件监听器
     * @param listener
     * @param <E>
     */
    <E extends Event<S>> void addEventListener(EventListener<E> listener);

    <E extends Event<S>> void addEventListeners(Collection<EventListener<E>> listeners);

    /**
     * 移除事件监听器
     * @param listener
     * @param <E>
     */
    <E extends Event<S>> void removeEventListener(EventListener<E> listener);

    <E extends Event<S>> void removeEventListeners(Collection<EventListener<E>> listeners);

}
