package com.clp.protocol.core.common;

public interface Checkable {

    /**
     * 检查配置，如果有异常或错误，抛出
     * @throws Throwable 检查出现的异常或错误
     */
    void check() throws Throwable;

}
