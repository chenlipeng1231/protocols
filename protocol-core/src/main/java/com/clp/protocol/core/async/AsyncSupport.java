package com.clp.protocol.core.async;

import java.util.concurrent.ScheduledExecutorService;

/**
 * 异步支持
 */
public interface AsyncSupport {
    ScheduledExecutorService scheduledExecutorService();
}
