package com.clp.protocol.core.async;

import io.netty.util.concurrent.Promise;

public interface NettyAsyncSupport extends AsyncSupport {

    <T> Promise<T> createPromise(Class<T> clazz);
}
