package com.clp.protocol.core.utils;

/**
 * 非期望的数字 异常
 */
public class UnexpectNumberException extends RuntimeException{

    public UnexpectNumberException(Number expectNum, Number actualNum) {
        super("Expect number is " + expectNum + ", but actually " + actualNum + ".");
    }

}
