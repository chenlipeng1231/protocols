package com.clp.protocol.core.async;

import com.clp.protocol.core.utils.AssertUtil;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;

public class AllIsSuccessPolicy implements SuccessPolicy {
    public static final AllIsSuccessPolicy INSTANCE = new AllIsSuccessPolicy();

    private AllIsSuccessPolicy() {
    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public <P extends IPromise, F extends IFuture> void applyIPromise(P promise, List<F> futures) {
        int futuresSize = futures.size();

        if (futuresSize == 0) throw new IllegalArgumentException();
        if (promise.isDone()) return;

        final AtomicBoolean[] isFinisheds = new AtomicBoolean[futuresSize];
        for (int i = 0; i < isFinisheds.length; i++) {
            isFinisheds[i] = new AtomicBoolean(false);
        }
        final AtomicBoolean[] isFaileds = new AtomicBoolean[futuresSize];
        for (int i = 0; i < isFaileds.length; i++) {
            isFaileds[i] = new AtomicBoolean(false);
        }

        Object lock = new Object();

        Class futureListenerClass = futures.get(0).getListenerClass();
        for (int index = 0; index < futuresSize; index++) {
            final int i = index;
            IFutureListener listener = (IFutureListener) Proxy.newProxyInstance(futureListenerClass.getClassLoader(), new Class[]{futureListenerClass}, new InvocationHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    if (method.getName().equals(IFutureListener.getOperationCompleteMethod().getName())) {
                        synchronized (lock) {
                            if (futures.get(i).isSuccess()) {
                                isFaileds[i].set(false);

                                // 检查是否是最后一个
                                boolean isLastFinished = true;
                                // 轮询检查所有其他结束的是否都成功
                                boolean isAllFinishedSuccess = true;
                                for (int j = 0; j < futuresSize; j++) {
                                    if (j == i) continue;
                                    if (isFinisheds[j].get()) {
                                        if (isFaileds[j].get()) {
                                            isAllFinishedSuccess = false;
                                        }
                                    } else {
                                        isLastFinished = false;
                                    }
                                }

                                // 设置结果
                                if (isLastFinished) {
                                    if (isAllFinishedSuccess) {
                                        promise.setSuccess();
                                    } else {
                                        promise.setFailure(new RuntimeException("Future列表未能全部执行成功！"));
                                    }
                                }
                            } else {
                                isFaileds[i].set(true);

                                // 检查是否是最后一个
                                boolean isLastFinished = true;
                                // 轮询检查是否全部执行失败
                                boolean isAllFinishedFailed = true;
                                for (int j = 0; j < futuresSize; j++) {
                                    if (j == i) continue;
                                    if (isFinisheds[j].get()) {
                                        if (!isFaileds[j].get()) {
                                            isAllFinishedFailed = false;
                                        }
                                    } else {
                                        isLastFinished = false;
                                    }
                                }

                                // 设置结果
                                if (isLastFinished) {
                                    if (isAllFinishedFailed) {
                                        promise.setFailure(new RuntimeException("Future列表全部执行失败！"));
                                    } else {
                                        promise.setFailure(new RuntimeException("Future列表未能全部执行成功！"));
                                    }
                                }
                            }
                            isFinisheds[i].set(true);
                        }
                        return null;
                    } else {
                        return method.invoke(args);
                    }
                }
            });
            futures.get(i).addListener(listener);
        }
    }

    @Override
    public CompletableFuture<Boolean> applyCompletableFuture(CompletableFuture<Boolean>... futures) {
        AssertUtil.notNull(futures, "futures");
        int futuresLength = futures.length;
        if (futuresLength == 0) throw new IllegalArgumentException();

        return CompletableFuture.allOf(futures).thenApplyAsync(new Function<Void, Boolean>() {
            @Override
            public Boolean apply(Void unused) {
                try {
                    for (CompletableFuture<Boolean> future : futures) {
                        if (!future.get()) {
                            return false;
                        }
                    }
                    return true;
                } catch (InterruptedException | ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    @Override
    public boolean applyBoolean(List<Boolean> isTrues) {
        for (boolean isTrue : isTrues) {
            if (!isTrue) return false;
        }
        return true;
    }
}
