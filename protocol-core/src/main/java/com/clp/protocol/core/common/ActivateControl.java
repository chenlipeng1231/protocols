package com.clp.protocol.core.common;

/**
 * 激活控制：表示一个对象具有激活-重新激活；停止激活这一周期
 */
public interface ActivateControl<F> extends Activatable<F> {

    /**
     * 重新激活
     * @return
     */
    F reactivate();

    /**
     * 取消激活
     * @return
     */
    F inactivate();

}
