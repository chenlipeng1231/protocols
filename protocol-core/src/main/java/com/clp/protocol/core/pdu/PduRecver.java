package com.clp.protocol.core.pdu;

import java.util.Collection;

public interface PduRecver<P extends Pdu> {
    void addRecvCallback(PduRecvCallback<P> callback);

    void addRecvCallbacks(Collection<PduRecvCallback<P>> callbacks);

    void removeRecvCallback(PduRecvCallback<P> callback);

    void removeRecvCallbacks(Collection<PduRecvCallback<P>> callbacks);
}
