package com.clp.protocol.core.pdu;

public interface BytePdu<P extends BytePdu<P>> extends BytePduClip<P>, Pdu {
}
