package com.clp.protocol.core.client;

public class FailedToConnectException extends RuntimeException {
    public FailedToConnectException(String remoteHost1, int remotePort1, String remoteHost2, int remotePort2) {
        super("连接 " + remoteHost1 + ":" + remotePort1 + " 和 " + remoteHost2 + ":" + remotePort2 + " 失败！");
    }

    public FailedToConnectException(String remoteHost, int remotePort) {
        super("连接 " + remoteHost + ":" + remotePort + " 失败！");
    }

    public FailedToConnectException(String msg) {
        super(msg);
    }
}
