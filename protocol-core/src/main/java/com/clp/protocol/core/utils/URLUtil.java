package com.clp.protocol.core.utils;

import java.net.MalformedURLException;
import java.net.URL;

public class URLUtil {
    private URLUtil() {}

    public static URL getClasspathUrl() {
        return getClasspathUrl(ClassLoaderUtil.getClassLoader());
    }

    public static URL getClasspathUrl(ClassLoader classLoader) {
        return getClasspathUrl(classLoader, "");
    }

    public static URL getClasspathUrl(String filePath) {
        return getClasspathUrl(ClassLoaderUtil.getClassLoader(), filePath);
    }

    public static URL getClasspathUrl(ClassLoader classLoader, String filePath) {
        AssertUtil.notNull(classLoader, "ClassLoader");
        AssertUtil.notNull(filePath, "FilePath");
        URL url = classLoader.getResource(filePath);
        AssertUtil.notNull(url, "URL");
        return url;
    }

    public static URL getUrl(String filePath) {
        AssertUtil.notNull(filePath, "FilePath");
        try {
            return new URL(filePath);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
