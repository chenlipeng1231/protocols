package com.clp.protocol.core.pdu.nbytepdu.time2a;

import com.clp.protocol.core.pdu.nbytepdu.BaseNBytePduClip;
import com.clp.protocol.core.pdu.ByteToStringFormat;
import com.clp.protocol.core.utils.ByteUtil;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Objects;
import java.util.function.Consumer;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CP32Time2a extends Time2a {
    public static int bytesLen = 4;
    /**
     * 时
     */
    private int hour;

    /**
     * 分
     */
    private int minute;

    /**
     * 秒
     */
    private int second;

    /**
     * 毫秒
     */
    private int millisecond;

    @Override
    public CP32Time2a refreshFrom(ByteBuf buf) {
        byte[] bytes = new byte[4];
        buf.readBytes(bytes);
        this.hour = bytes[3] & 0x1F;
        this.minute = bytes[2] & 0x3F;
        int allMs = ((bytes[1] & 0xFF) << 8) + (bytes[0] & 0xFF);
        this.second = allMs / 1000;
        this.millisecond = allMs % 1000;
        return this;
    }

    @Override
    public boolean isValid() {
//        if (hour < 0 || hour >= 24) return false;
//        if (minute < 0 || minute >= 60) return false;
//        if (second < 0 || second >= 60) return false;
//        return millisecond >= 0 && millisecond < 1000;
        return true;
    }

    @Override
    public void writeBytesTo(ByteBuf buf) {
        // 计算总毫秒数
        int allMs = second * 1000 + millisecond;
        // 写入
        buf.writeShortLE(allMs);
        buf.writeByte(minute & 0x3F);
        buf.writeByte(hour & 0x1F);
    }

    @Override
    public void writeFormattedByteStringsTo(StringBuilder sb, String frameClipBytesSeparator, String byteSeparator, ByteToStringFormat byteFormat) {
        // 计算总毫秒数
        int allMs = second * 1000 + millisecond;
        byte[] msBytes = ByteUtil.intToBytes2LE(allMs);
        sb.append(byteFormat.format(msBytes[0]));
        for (int i = 1; i < msBytes.length; i++) {
            sb.append(byteSeparator).append(byteFormat.format(msBytes[i]));
        }
        sb.append(byteSeparator).append(byteFormat.format((byte) (minute & 0x3F)));
        sb.append(byteSeparator).append(byteFormat.format((byte) (hour & 0x1F)));
    }

    @Override
    public void writeSimpleDescriptionTo(StringBuilder sb) {
        sb.append("CP32Time2a<").append(hour).append(":").append(minute).append(":").append(second).append(":").append(millisecond).append(">");
    }

    @Override
    public void writeDetailDescriptionTo(StringBuilder sb) {
        sb.append("CP32Time2a<").append(hour).append(":").append(minute).append(":").append(second).append(":").append(millisecond).append(">");
    }

    @Override
    protected void forEachOneLevelChild(Consumer<BaseNBytePduClip<?>> consumer) {
    }

    @Override
    public Date toDate() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CP32Time2a that = (CP32Time2a) o;
        return hour == that.hour && minute == that.minute && second == that.second && millisecond == that.millisecond;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hour, minute, second, millisecond);
    }
}
