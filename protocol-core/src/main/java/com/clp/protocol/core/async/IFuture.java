package com.clp.protocol.core.async;

import com.clp.protocol.core.utils.TypeResolver;

/**
 * 不会实际使用的接口，IFuture类似于Future，不过除了可以获取结果（V）之外，可以判断成功和失败
 * @param <V> 结果泛型
 * @param <IFL> 添加监听器，在结束之后会调用添加的监听器
 */
@SuppressWarnings("rawtypes")
public interface IFuture<V, IFL extends IFutureListener> {
//public interface IFuture<V, IFL extends IFutureListener<? extends IFuture<V, IFL>>> {
    /**
     * 是否完成
     * @return
     */
    boolean isDone();

    /**
     * 是否执行成功
     * @return
     */
    boolean isSuccess();

    /**
     * 如果执行失败，获取失败原因
     * @return
     */
    Throwable cause();

    /**
     * 如果有执行结果，获取执行结果
     * @return
     */
    V getRes();

    /**
     * 同步等待完成
     * @return
     */
    IFuture<V, IFL> sync();

    /**
     * 同步等待完成，等待有限时间
     * @param timeoutMs 超时时间毫秒数
     * @return
     */
    IFuture<V, IFL> sync(int timeoutMs);

    /**
     * 添加监听器
     * @param listener 监听器
     * @return
     */
    IFuture<V, IFL> addListener(IFL listener);

    @SuppressWarnings("unchecked")
    default Class<IFL> getListenerClass() {
        return (Class<IFL>) TypeResolver.resolveRawArguments(IFuture.class, this.getClass())[1];
    }
}
