package com.clp.protocol.core.utils;

/**
 * 非法数值 异常
 */
public class IllegalNumberException extends RuntimeException {

    public IllegalNumberException(byte num) {
        super("Illegal number: " + num);
    }
    public IllegalNumberException(short num) {
        super("Illegal number: " + num);
    }
    public IllegalNumberException(int num) {
        super("Illegal number: " + num);
    }
    public IllegalNumberException(long num) {
        super("Illegal number: " + num);
    }
    public IllegalNumberException(float num) {
        super("Illegal number: " + num);
    }
    public IllegalNumberException(double num) {
        super("Illegal number: " + num);
    }
}
