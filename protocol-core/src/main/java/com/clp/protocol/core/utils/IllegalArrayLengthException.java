package com.clp.protocol.core.utils;

/**
 * 非法数组长度异常
 */
public class IllegalArrayLengthException extends RuntimeException {

    public IllegalArrayLengthException(int expectLen, int actualLen) {
        super("Expected length: " + expectLen + ", actual length: " + actualLen);
    }

}
