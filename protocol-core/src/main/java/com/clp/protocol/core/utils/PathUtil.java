package com.clp.protocol.core.utils;

import lombok.extern.slf4j.Slf4j;

import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
public class PathUtil {
    private PathUtil() {}

    public static Path ofUserCurrentPath() {
        return Paths.get("");
    }
}
